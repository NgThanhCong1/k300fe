import React from 'react';

import './app.css';
import { Route, Switch } from 'react-router-dom';
import Footer from './components/Commons/Footer';
import Navbar from './components/Commons/Navbar';
import Sidebar from './components/Commons/Sidebar';
import Login from './components/PageContents/Auth/login';
import HomeProduct from './components/PageContents/Product/home/index';
import DetailProduct from './components/PageContents/Product/detail/index';
import UpdateProduct from './components/PageContents/Product/update';
import AddProduct from './components/PageContents/Product/add';
import AddSubProduct from './components/PageContents/SubProduct/add';
import HomeSubProduct from './components/PageContents/SubProduct/home';
import UpdateSubProduct from './components/PageContents/SubProduct/update';
import AddInInvoice from './components/PageContents/ImportProduct/add';
import HomeInInvoice from './components/PageContents/ImportProduct/home';
import InInvoiceDetail from './components/PageContents/ImportProduct/detail';
import HomeOutInvoice from './components/PageContents/OutInvoice/home';
import DetailOutInvoice from './components/PageContents/OutInvoice/detail';
import UpdateOutInvoice from './components/PageContents/OutInvoice/update';
import PrivateRoute from './components/PageContents/Auth/PrivateRoute';
import HomeAdmin from './components/PageContents/Home';

import HomeBrand from './components/PageContents/Brand/home';
import AddBrand from './components/PageContents/Brand/add';
import UpdateBrand from './components/PageContents/Brand/update';

import HomeCategory from './components/PageContents/Category/home';
import AddCategory from './components/PageContents/Category/add';
import UpdateCategory from './components/PageContents/Category/update';

import HomeDiscount from './components/PageContents/Discount/home';
import AddDiscount from './components/PageContents/Discount/add';
import UpdateDiscount from './components/PageContents/Discount/update';

import HomeDistributor from './components/PageContents/Distributor/home';
import AddDistributor from './components/PageContents/Distributor/add';
import UpdateDistributor from './components/PageContents/Distributor/update';

import HomeUser from './components/PageContents/User/home';
import AddUser from './components/PageContents/User/add';

import HomeRole from './components/PageContents/Role/home';
import AddRole from './components/PageContents/Role/add';
import UpdateRole from './components/PageContents/Role/update';
import AssignRole from './components/PageContents/Role/assign';
import Keyword from './components/PageContents/User/keyword';

import HomeAccount from './components/PageContents/Account/home';

import HomeStore from './components/PageContents/Store/home';
import AddStore from './components/PageContents/Store/add';
import UpdateStore from './components/PageContents/Store/update';

import { AbilityContext } from './components/PageContents/Auth/CASL/can';
import defineAbility from './components/PageContents/Auth/CASL/defineAbility';

function App() {
  return (
    <AbilityContext.Provider value={defineAbility}>
      <>
        <Navbar />
        <Sidebar />
        <div id="app">
          <div className="main-wrapper">
            <div className="navbar-bg"></div>
            <div className="main-content">
              <section className="section">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="card">
                      <Switch>
                        <PrivateRoute path="/product/home" exact component={HomeProduct} />
                        <PrivateRoute path="/product/detail/:id" exact component={DetailProduct} />
                        <PrivateRoute path="/product/add" exact component={AddProduct} />
                        <PrivateRoute path="/product/update/:id" exact component={UpdateProduct} />
                        <PrivateRoute path="/sub-product/add/:id/:name" exact component={AddSubProduct} />
                        <PrivateRoute path="/sub-product/home" exact component={HomeSubProduct} />
                        <PrivateRoute path="/sub-product/update/:productCode" exact component={UpdateSubProduct} />

                        <PrivateRoute path="/import-product/add" exact component={AddInInvoice} />
                        <PrivateRoute path="/import-product/home" exact component={HomeInInvoice} />
                        <PrivateRoute path="/import-product/viewdetail/:id" exact component={InInvoiceDetail} />

                        <PrivateRoute path="/outInvoice/home" component={HomeOutInvoice} />
                        <PrivateRoute path="/outInvoice/detail/:id" exact component={DetailOutInvoice} />
                        <PrivateRoute path="/outInvoice/update/:id" exact component={UpdateOutInvoice} />



                        <PrivateRoute path="/brand/home" exact component={HomeBrand} />
                        <PrivateRoute path="/brand/add" exact component={AddBrand} />
                        <PrivateRoute path="/brand/update/:id" exact component={UpdateBrand} />

                        <PrivateRoute path="/category/home" exact component={HomeCategory} />
                        <PrivateRoute path="/category/add" exact component={AddCategory} />
                        <PrivateRoute path="/category/update/:id" exact component={UpdateCategory} />

                        <PrivateRoute path="/discount/home" exact component={HomeDiscount} />
                        <PrivateRoute path="/discount/add" exact component={AddDiscount} />
                        <PrivateRoute path="/discount/update/:id" exact component={UpdateDiscount} />

                        <PrivateRoute path="/distributor/home" exact component={HomeDistributor} />
                        <PrivateRoute path="/distributor/add" exact component={AddDistributor} />
                        <PrivateRoute path="/distributor/update/:id" exact component={UpdateDistributor} />

                        <PrivateRoute path="/user/home/:role" exact component={HomeUser} />
                        <PrivateRoute path="/user/add/:role" exact component={AddUser} />
                        <PrivateRoute path="/role/home" exact component={HomeRole} />
                        <PrivateRoute path="/keyword/home" exact component={Keyword} />
                        <PrivateRoute path="/role/add" exact component={AddRole} />
                        <PrivateRoute path="/role/update/:role" exact component={UpdateRole} />
                        <PrivateRoute path="/role/assign/:userName" exact component={AssignRole} />

                        <PrivateRoute path="/account/home" exact component={HomeAccount} />
                        <PrivateRoute path="/" exact component={HomeAdmin} />

                        <PrivateRoute path="/store/home" exact component={HomeStore} />
                        <PrivateRoute path="/store/add" exact component={AddStore} />
                        <PrivateRoute path="/store/update/:id" exact component={UpdateStore} />
                      </Switch>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
        <Footer />
      </>
      <Route path="/login" exact component={Login} />
    </AbilityContext.Provider>
  );
}

export default App;
