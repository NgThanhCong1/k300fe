export const forceUpdate = () => {
    return {
        type: 'FORCE_UPDATE'
    };
};

export const userLogin = (staffInfor, listPermissions) => {
    return {
        type: 'USER_LOGIN',
        data: { staffInfor, listPermissions }
    };
};

export const userLogout = () => {
    return {
        type: 'USER_LOGOUT'
    };
};

export const updateUser = (userInfor) => {
    return {
        type: 'UPDATE_USER',
        data: userInfor
    };
};