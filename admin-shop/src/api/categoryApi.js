import axiosClient from "./axiosClient";

const categoryApi = {
    addCategory: category => {
        const url = "category/add-cate";
        return axiosClient.post(url, category);
    },
    deleteCategory: category => {
        const url = "Category/delete-cate";
        return axiosClient.post(url, category);
    },
    updateCategory: category => {
        const url = "category/update-cate";
        return axiosClient.post(url, category);
    },
    getAllCategories: params => {
        const url = "category/get-all-cate";
        return axiosClient.get(url, { params });
    },
    getDataForUpdateCategory: id => {
        const url = `category/get-data-for-update-category?id=${id}`;
        return axiosClient.get(url);
    },
    getParentCategories: () => {
        const url = "category/get-categories-by-parentCategory";
        return axiosClient.get(url);
    }
}

export default categoryApi;