import axiosClient from "./axiosClient";

const productApi = {
    getDataForAddProduct: () => {
        const url = "Product/get-data-for-add-product";
        return axiosClient.get(url);
    },
    addProduct: product => {
        const url = "Product/add-product";
        return axiosClient.post(url, product);
    },
    addSubProduct: subProduct => {
        const url = "Product/add-subProduct";
        return axiosClient.post(url, subProduct);
    },
    getDataForUpdateProduct: id => {
        const url = `Product/get-data-for-update-product?id=${id}`;
        return axiosClient.get(url);
    },
    getAllProducts: params => {
        const url = "Product/get-all-product";
        return axiosClient.get(url, { params });
    },
    deleteProduct: id => {
        const url = "Product/delete-product";
        return axiosClient.post(url, { id });
    },
    updateProduct: product => {
        const url = "Product/update-product";
        return axiosClient.post(url, product);
    },
    getDataForUpdateSubProduct: productCode => {
        const url = `Product/get-data-for-update-subProduct?productCode=${productCode}`;
        return axiosClient.get(url);
    },
    updateSubProduct: subProduct => {
        const url = "Product/update-subProduct";
        return axiosClient.post(url, subProduct);
    },
}

export default productApi;