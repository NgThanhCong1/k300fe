import axiosClient from "./axiosClient";

const brandApi = {
    addBrand: brand => {
        const url = "brand/add-brand";
        return axiosClient.post(url, brand);
    },
    deleteBrand: brand => {
        const url = "brand/delete-brand";
        return axiosClient.post(url, brand);
    },
    updateBrand: brand => {
        const url = "brand/update-brand";
        return axiosClient.post(url, brand);
    },
    getAllBrands: params => {
        const url = "brand/get-all-brand";
        return axiosClient.get(url, { params });
    },
    getDataForUpdateBrand: id => {
        const url = `brand/get-data-for-update-brand?id=${id}`;
        return axiosClient.get(url);
    }
}

export default brandApi;