import axiosClient from "./axiosClient";

const importProductApi = {
    getDataForAddInInvoice: () => {
        const url = "importProduct/get-data-for-add-ininvoice";
        return axiosClient.get(url);
    },
    addInInvoice: ininvoice => {
        const url = "ImportProduct/add-ininvoice";
        return axiosClient.post(url, ininvoice);
    },
    getInInvoiceDetails: id => {
        const url = `ImportProduct/get-ininvoice-detail?inInvoiceId=${id}`;
        return axiosClient.get(url);
    },
    getAllIninvoices: params => {
        const url = "ImportProduct/get-all-ininvoice";
        return axiosClient.get(url, { params });
    },
}

export default importProductApi;