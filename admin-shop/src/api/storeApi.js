import axiosClient from "./axiosClient";

const storeApi = {
    addStore: store => {
        const url = "Store/add-store";
        return axiosClient.post(url, store);
    },
    getAllStores: params => {
        const url = "Store/get-all-store";
        return axiosClient.get(url, { params });
    },
    deleteStore: store => {
        const url = "Store/delete-store";
        return axiosClient.post(url, store);
    },
    getDataForUpdateStore: id => {
        const url = `Store/get-data-for-update-store?id=${id}`;
        return axiosClient.get(url);
    },
    updateStore: store => {
        const url = "Store/update-store";
        return axiosClient.post(url, store);
    },
}

export default storeApi;