import axiosClient from "./axiosClient";

const distributorApi = {
    addDistributor: distributor => {
        const url = "distributor/add-distributor";
        return axiosClient.post(url, distributor);
    },
    deleteDistributor: distributor => {
        const url = "distributor/delete-distributor";
        return axiosClient.post(url, distributor);
    },
    updateDistributor: distributor => {
        const url = "Distributor/update-distributor";
        return axiosClient.post(url, distributor);
    },
    getAllDistributors: params => {
        const url = "distributor/get-all-distributor";
        return axiosClient.get(url, { params });
    },
    getDataForUpdateDistributor: id => {
        const url = `Distributor/get-data-for-update-distributor?id=${id}`;
        return axiosClient.get(url);
    },
}

export default distributorApi;