import axiosClient from "./axiosClient";

const authApi = {
    login: userInfor => {
        const url = "/User/user-login";
        return axiosClient.post(url, userInfor);
    },
    getRefreshToken: token => {
        const url = `User/get-refresh-token`;
        return axiosClient.post(url, { token });
    },
}

export default authApi;