import axiosClient from "./axiosClient";

const outInvoiceApi = {
    getOutInvoiceDetail: id => {
        const url = `Outport/get-outinvoice-detail-by-outInvoiceId?id=${id}`;
        return axiosClient.get(url);
    },
    updateOutInvoice: (type, item) => {
        const url = `Outport/update-outinvoice?type=${type}`;
        return axiosClient.post(url, item);
    },
    getDataForUpadteOutInvoice: id => {
        const url = `Outport/get-data-for-update-outInvoice?id=${id}`;
        return axiosClient.get(url);
    },
    updateOutInvoiceData: outInvoice => {
        const url = `Outport/update-outInvoice-data`;
        return axiosClient.post(url, outInvoice);
    },
    getAllOutInvoices: params => {
        const url = `Outport/get-all-invoices`;
        return axiosClient.get(url, { params });
    },
}

export default outInvoiceApi;