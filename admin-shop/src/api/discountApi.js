import axiosClient from "./axiosClient";

const discountApi = {
    addDiscount: discount => {
        const url = "discount/add-discount";
        return axiosClient.post(url, discount);
    },
    deleteDiscount: discount => {
        const url = "discount/delete-discount";
        return axiosClient.post(url, discount);
    },
    updateDiscount: discount => {
        const url = "discount/update-discount";
        return axiosClient.post(url, discount);
    },
    getAllDiscounts: params => {
        const url = "Discount/get-all-discount";
        return axiosClient.get(url, { params });
    },
    getDataForUpdateDiscount: id => {
        const url = `discount/get-data-for-update-discount?id=${id}`;
        return axiosClient.get(url);
    },
}

export default discountApi;