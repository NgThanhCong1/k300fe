import axiosClient from "./axiosClient";

const generalApi = {
    addChat: chat => {
        const url = "general/add-direct-chat";
        return axiosClient.post(url, chat);
    },
    getDataForDashboard: () => {
        const url = "general/get-data-for-dashboard";
        return axiosClient.get(url);
    },
    get2MonthKeywords: params => {
        const url = "General/get-2months-recent-keywords";
        return axiosClient.get(url, { params });
    },
}

export default generalApi;