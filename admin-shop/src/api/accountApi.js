import axiosClient from "./axiosClient";

const accountApi = {
    getStaff: id => {
        const url = `/user/get-user-by-id?id=${id}`;
        return axiosClient.get(url);
    },
    updateStaff: staff => {
        const url = "User/update-user";
        return axiosClient.post(url, staff);
    }
}

export default accountApi;