import axiosClient from "./axiosClient";

const notifyApi = {
    addNotify: notify => {
        const url = "notify/add-notify";
        return axiosClient.post(url, notify);
    },
}

export default notifyApi;