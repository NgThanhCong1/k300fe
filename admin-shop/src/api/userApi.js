import axiosClient from "./axiosClient";

const userApi = {
    getAllCustomers: () => {
        const url = "user/get-users-by-role?role=Customer";
        return axiosClient.get(url);
    },
    addRole: role => {
        const url = "User/add-role";
        return axiosClient.post(url, role);
    },
    getDataForAssignRole: userName => {
        const url = `User/get-data-for-assign-role?userName=${userName}`;
        return axiosClient.get(url);
    },
    removeAllRolesOfUser: listAssignRoles => {
        const url = `User/remove-all-roles-of-user`;
        return axiosClient.post(url, listAssignRoles);
    },
    getAllRoles: params => {
        const url = "User/get-all-roles";
        return axiosClient.get(url, { params });
    },
    deleteRole: name => {
        const url = "User/delete-role";
        return axiosClient.post(url, name);
    },
    getAllPermissionsOfRole: role => {
        const url = `User/get-permissions-by-role?roleName=${role}`;
        return axiosClient.get(url);
    },
    updatePermissionsOfRole: permissionOfRole => {
        const url = "User/update-permissions-of-role";
        return axiosClient.post(url, permissionOfRole);
    },
    addUser: (role, user) => {
        const url = `User/create-user?role=${role}`;
        return axiosClient.post(url, user);
    },
    getUserByRole: params => {
        const url = "User/get-users-by-role";
        return axiosClient.get(url, { params });
    },
    deleteUser: (user) => {
        const url = "User/delete-user";
        return axiosClient.post(url, user);
    },
}

export default userApi;

/*
try {
                const response = await productApi.getDataForUpdateProduct(id);
                if (response.code === 1) {
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
*/