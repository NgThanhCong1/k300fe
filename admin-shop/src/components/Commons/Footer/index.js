import React from 'react'
import { userName } from '../../Constant';

const Footer = function (params) {
  return (
    <footer className="main-footer">
      <div className="footer-left">
        Copyright &copy; 2018 <div className="bullet"></div> Active by {userName}
      </div>
      <div className="footer-right">
        2.3.0
      </div>
    </footer>
  );
}

export default Footer;