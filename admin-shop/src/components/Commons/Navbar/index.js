import { Link } from 'react-router-dom';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { userLogout } from 'actions';
import { rootPathImg } from 'components/Constant';

// const LogoutButton = withRouter(({ history }) => (
//   localStorage.getItem("staffToken") ? (
//     <>

//     </>
//   ) : (
//     <>you not login</>
//   )
// ))
function Navbar() {
  const userInfor = useSelector(state => state.userReducer);
  const dispatch = useDispatch();

  return (
    <nav className="navbar navbar-expand-lg main-navbar">
      <form className="form-inline mr-auto">
        <ul className="navbar-nav mr-3">
          <li><a href="/" data-toggle="sidebar" className="nav-link nav-link-lg"><i className="fas fa-bars"></i></a></li>
          <li><a href="/" data-toggle="search" className="nav-link nav-link-lg d-sm-none"><i className="fas fa-search"></i></a></li>
        </ul>
        <div className="search-element">
          <input className="form-control" type="search" placeholder="Search" aria-label="Search" data-width="250" />
          <button className="btn" type="submit"><i className="fas fa-search"></i></button>
        </div>
      </form>
      <ul className="navbar-nav navbar-right">
        <li className="dropdown dropdown-list-toggle"><a href="/" data-toggle="dropdown" className="nav-link nav-link-lg message-toggle beep"><i className="far fa-envelope"></i></a>
        </li>
        <li className="dropdown dropdown-list-toggle"><a href="/" data-toggle="dropdown" className="nav-link notification-toggle nav-link-lg beep"><i className="far fa-bell"></i></a>
        </li>
        <li className="dropdown"><a href="/" data-toggle="dropdown" className="nav-link dropdown-toggle nav-link-lg nav-link-user">
          <img
            alt="alt here"
            src={
              userInfor
                ? `${rootPathImg}${userInfor.staffInfor.avatar}`
                : `${rootPathImg}/Images/Users/user.png`
            }
            className="rounded-circle mr-1"
          />
          <div className="d-sm-none d-lg-inline-block">
            Hi, {userInfor && userInfor.staffInfor.userName}
          </div>
        </a>
          <div className="dropdown-menu dropdown-menu-right">
            <Link to="/account/home">
              <a
                href="/"
                className="dropdown-item has-icon"
              >
                <i className="far fa-user"></i> Profile
              </a>
            </Link>
            <div className="dropdown-divider"></div>
            <a
              href="/"
              className="dropdown-item has-icon"
            >
              {
                userInfor
                  ? <span
                    className="dropdown-item has-icon"
                    onClick={() => {
                      localStorage.removeItem("staffToken");
                      localStorage.removeItem("listPermissions");
                      dispatch(userLogout());
                      // history.push('/login');
                    }}>
                    <i className="fas fa-sign-out-alt"></i> Sign out
                  </span>
                  : <>you not login</>
              }
            </a>
          </div>
        </li>
      </ul>
    </nav>
  );
}

export default Navbar;
