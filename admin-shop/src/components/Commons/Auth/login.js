import React, { useState } from 'react';
import './login.css'
import { useForm } from "react-hook-form";

Login.propTypes = {

};

function Login(props) {
    // const [usename, setUsername] = useState('')
    // const [password, setPassword] = useState('')
    const { onLoginSubmit, onRegisterBtnClick } = props;
    const { register, handleSubmit, formState: { errors } } = useForm({
        mode: 'onBlur',
    });

    function handleRegisterBtnClick() {
        if (!onRegisterBtnClick) return
        onRegisterBtnClick()
    }

    const onSubmit = data => {
        if (!onLoginSubmit) return
        onLoginSubmit({
            username: usename,
            password: password,
        })
    }

    return (
        <>
            <div class="overlay" onclick="hide()"></div>
            <div class="formwrap">
                <div class="close-btn">

                </div>
                <div class="form-left">
                    <div class="form-left__content">
                        <h3>The K300</h3>
                        <p>Upgrade your fashion now</p>
                    </div>
                    <img src="https://i.pinimg.com/originals/6b/a6/e2/6ba6e2c31411a00988f952a556509c70.jpg" alt="" />
                </div>
                <div class="form-right">
                    <form action="" class="login-form" id="login-form" onSubmit={handleSubmit(onSubmit)}>
                        <div class="form-group">
                            <label for="Username">USERNAME</label>
                            <input
                                type="text"
                                name="Username"
                                id="Username"
                                {...register('username', { required: true })}
                            // onChange={(e) => setUsername(e.target.value)}
                            />
                            {errors.username && <span style={{ color: "red" }}>This field is required</span>}
                        </div>
                        <div class="form-group">
                            <label for="Password">PASSWORD</label>
                            <input
                                type="password"
                                name="Password"
                                id="Password"
                                {...register('password', { minLength: 10 })}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                            {errors.password && <span style={{ color: "red" }}>10 character atleast</span>}

                        </div>
                        <div class="form-submit">
                            <button type="submit" class="form-btn">LOGIN NOW</button>
                            <p onClick={() => handleRegisterBtnClick()}>SIGN UP HERE</p>
                        </div>
                    </form>
                </div>
            </div>
        </>
    );
}

export default Login;