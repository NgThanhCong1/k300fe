import React from 'react';
import { Link } from 'react-router-dom';
import { Can } from 'components/PageContents/Auth/CASL/can';

function Invoice(props) {
    const { listInInvoices, listSearchStaticDashBoards } = props
    return (
        <div className="row">
            <div className="col-md-8">
                <div className="card">
                    <div className="card-header">
                        <h4>Import products</h4>
                        <div className="card-header-action">
                            <Can I="read" a="ImportProduct">
                                <Link
                                    className="btn btn-danger"
                                    to={`/import-product/home`}
                                >
                                    View More <i className="fas fa-chevron-right" />
                                </Link>
                            </Can>
                        </div>
                    </div>
                    <div className="card-body p-0">
                        <div className="table-responsive table-invoice">
                            <table className="table table-striped">
                                <tbody
                                    style={{ maxHeight: "250px", overflowY: "auto", display: "block" }}
                                >
                                    <tr>
                                        <th>Import Id</th>
                                        <th>Total Money</th>
                                        <th>Distributor</th>
                                        <th>Staff Import</th>
                                        <th>Action</th>
                                    </tr>
                                    {
                                        listInInvoices.map(item => (
                                            <tr>
                                                <td>{item.id}</td>
                                                <td className="font-weight-600">{item.totalMoney}</td>
                                                <td><div className="badge badge-success">{item.distributorName}</div></td>
                                                <td>{item.createdBy}</td>
                                                <td>

                                                    <Can I="read" a="ImportProduct">
                                                        <Link
                                                            className="btn btn-primary"
                                                            to={`/import-product/viewdetail/${item.id}`}
                                                        >
                                                            Detail
                                                        </Link>
                                                    </Can>
                                                </td>

                                            </tr>
                                        ))
                                    }
                                </tbody></table>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-md-4">
                <div className="card">
                    <div className="card-header">
                        <h4>Popular Keywords</h4>
                        <div className="card-header-action">
                            <Can I="read" a="Keyword">
                                <Link
                                    className="btn btn-danger"
                                    to={`/keyword/home`}
                                >
                                    View More <i className="fas fa-chevron-right" />
                                </Link>
                            </Can>
                        </div>
                    </div>
                    <div className="card-body p-0">
                        <div className="table-responsive table-invoice">
                            <table className="table table-striped">
                                <tbody
                                    style={{ maxHeight: "250px", overflowY: "auto", display: "block" }}
                                >
                                    <tr>
                                        <th>Keyword</th>
                                        <th>Search volume</th>
                                    </tr>
                                    {
                                        listSearchStaticDashBoards.map(item => (
                                            <tr>
                                                <td>{item.keyword}</td>
                                                <td className="font-weight-600">{item.keywordCount}</td>
                                            </tr>
                                        ))
                                    }
                                </tbody></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Invoice;