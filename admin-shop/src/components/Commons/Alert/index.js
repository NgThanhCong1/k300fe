export default function CustomAlert(props) {
    const { type, message } = props;
    return (
        <>
            {/* <div class="alert alert-primary" role="alert">
                {message}
            </div>
            <div class="alert alert-secondary" role="alert">
                {message}
            </div>
            <div class="alert alert-success" role="alert">
                {message}
            </div>
            <div class="alert alert-danger" role="alert">
                {message}
            </div>
            <div class="alert alert-warning" role="alert">
                {message}
            </div>
            <div class="alert alert-info" role="alert">
                {message}
            </div>
            <div class="alert alert-light" role="alert">
                {message}
            </div>
            <div class="alert alert-dark" role="alert">
                {message}
            </div> */}
            {
                type === 1
                    ? <div
                        class="alert alert-success"
                        role="alert"
                    >
                        {message}
                    </div>
                    : type === -1
                        ? <div
                            class="alert alert-dark"
                            role="alert"
                        >
                            !!! {message}
                        </div>
                        : ''
            }
        </>
    );
}