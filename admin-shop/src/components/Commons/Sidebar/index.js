import React from 'react';
import { Link } from 'react-router-dom';
import { Can } from '../../PageContents/Auth/CASL/can';

const Sidebar = function (params) {
  return (
    <div className="main-sidebar">
      <aside id="sidebar-wrapper">
        <Link to="/">
          <div className="sidebar-brand">
            <a href="/">K300 Admin</a>
          </div>
        </Link>
        <div className="sidebar-brand sidebar-brand-sm">
          <a href="index.html">St</a>
        </div>
        <ul class="sidebar-menu">
          <li class="menu-header">Dashboard</li>
          <li class="nav-item dropdown active">
            <a href="/" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            <ul class="dropdown-menu">
              <li class="active"><a class="nav-link" href="index.html">Ecommerce Dashboard</a></li>
            </ul>
          </li>
          <li class="menu-header">Starter</li>
          <li class="nav-item dropdown">
            <a href="/" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i>
              <span>Product</span>
            </a>
            <ul class="dropdown-menu">
              <Can I="read" a="Product">
                <li>
                  <Link to="/product/home">
                    Product
                  </Link>
                </li>
              </Can>
              <Can I="read" a="Brand">
                <li>
                  <Link to="/brand/home">
                    Brand
                  </Link>
                </li>
              </Can>
              <Can I="read" a="Category">
                <li>
                  <Link to="/category/home">
                    Category
                  </Link>
                </li>
              </Can>
              <Can I="read" a="Discount">
                <li>
                  <Link to="/discount/home">
                    Discount
                  </Link>
                </li>
              </Can>
              <Can I="read" a="Distributor">
                <li>
                  <Link to="/distributor/home">
                    Distributor
                  </Link>
                </li>
              </Can>

            </ul>
          </li>
          <Can I="read" a="ImportProduct">
            <li class="nav-item dropdown">
              <Link to="/import-product/home">
                <i class="fas fa-columns"></i>Import product
              </Link>
            </li>
          </Can>
          <Can I="read" a="OutportProduct">
            <li class="nav-item dropdown">
              <Link to="/outInvoice/home">
                <i class="fas fa-columns"></i>Outinvoice
              </Link>
            </li>
          </Can>
          <li class="nav-item dropdown">
            <a href="/" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i>
              <span>Users</span>
            </a>
            <ul class="dropdown-menu">
              <Can I="read" a="User">
                <li>
                  <Link to="/user/home/all">
                    All Users
                  </Link>
                </li>
              </Can>
              <li>
                <Link to="/user/home/SellStaff">
                  SellStaff
                </Link>
              </li>
              <li>
                <Link to="/user/home/RepoStaff">
                  RepoStaff
                </Link>
              </li>
              <Can I="read" a="Role">
                <li>
                  <Link to="/user/home/Customer">
                    Customer
                  </Link>
                </li>
                <li>
                  <Link to="/role/home">
                    Roles
                  </Link>
                </li>
              </Can>
            </ul>
          </li>
          <Can I="read" a="Store">
            <li class="nav-item dropdown">
              <Link to="/store/home">
                <i class="fas fa-columns"></i>Stores
              </Link>
            </li>
          </Can>
        </ul>
        <div className="mt-4 mb-4 p-3 hide-sidebar-mini">
          <a href="https://getstisla.com/docs" className="btn btn-primary btn-lg btn-block btn-icon-split">
            <i className="fas fa-rocket"></i> Documentation
          </a>
        </div>
      </aside>
    </div>
  );
}

export default Sidebar;