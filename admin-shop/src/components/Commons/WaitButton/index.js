import { Spinner } from "reactstrap";

export default function WaitButton(props) {
    const { type, className, btnText } = props;
    const isOff = true;
    return (
        <button
            type={type}
            className={className}
            disabled={isOff}
        >
            {isOff ? <Spinner></Spinner> : btnText}
        </button>
    );
}