import { rootPathImg } from 'components/Constant';
import React from 'react';
import { PieChart, Pie, Cell, ResponsiveContainer } from 'recharts';

function BestProduct(props) {
    const { listTop10PopularProducts, listCategoryStatics } = props
    let data = [];
    let total = 0
    listCategoryStatics.forEach(item => {
        total += item.buyQuantityOfCategoryInMonth
    })
    listCategoryStatics.forEach(item => {
        data.push({
            name: item, value: (item.buyQuantityOfCategoryInMonth / total) * 100
        })
    })
    const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
    const RADIAN = Math.PI / 180;
    const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index, payload }) => {
        const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
        const x = cx + radius * Math.cos(-midAngle * RADIAN);
        const y = cy + radius * Math.sin(-midAngle * RADIAN);
        return (
            <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
                {`${payload.name.name}.${(percent * 100).toFixed(0)}%`}
            </text>
        );
    };

    return (
        <div className="row">
            <div className="col-md-6">
                <div className="card gradient-bottom">
                    <div className="card-header">
                        <h4>Top Categories this month</h4>
                    </div>
                </div>
            </div>
            <div className="col-md-6">
                <div className="card gradient-bottom">
                    <div className="card-header">
                        <h4>Top 10 Products this month</h4>
                    </div>
                </div>
            </div>
            <div className="col-md-6">
                <ResponsiveContainer width="100%" height="100%">
                    <PieChart width={400} height={400}>
                        <Pie
                            data={data}
                            cx="50%"
                            cy="50%"
                            labelLine={false}
                            label={renderCustomizedLabel}
                            outerRadius={200}
                            fill="#8884d8"
                            dataKey="value"
                        >
                            {data.map((entry, index) => (
                                <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                            ))}
                        </Pie>
                    </PieChart>
                </ResponsiveContainer>
            </div>
            <div className="col-md-6">
                <div className="card gradient-bottom">
                    {/* <div className="card-header">
                        <h4>Top 10 Products this month</h4>
                        <div className="card-header-action dropdown">
                            <a href="#" data-toggle="dropdown" className="btn btn-danger dropdown-toggle">Month</a>
                            <ul className="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                <li className="dropdown-title">Select Period</li>
                                <li><a href="#" className="dropdown-item">Today</a></li>
                                <li><a href="#" className="dropdown-item">Week</a></li>
                                <li><a href="#" className="dropdown-item active">Month</a></li>
                                <li><a href="#" className="dropdown-item">This Year</a></li>
                            </ul>
                        </div>
                    </div> */}
                    <div className="card-body" id="top-5-scroll">
                        <ul
                            className="list-unstyled list-unstyled-border"
                            style={{ maxHeight: "400px", overflowY: "auto" }}
                        >
                            {
                                listTop10PopularProducts.map((item) => (
                                    <li className="media">
                                        <img
                                            className="mr-3 rounded"
                                            width={55}
                                            src={`${rootPathImg}${item.listImagesLink[0]}`}
                                            alt="product"
                                        />
                                        <div className="media-body">
                                            <div className="float-right">
                                                <div className="font-weight-600 text-muted text-small">
                                                    {item.quantitySellInMonth} Sales
                                                </div>
                                            </div>
                                            <div className="media-title">
                                                {item.productCode}
                                            </div>
                                            <div className="mt-1">
                                                <div className="budget-price">
                                                    <div className="budget-price-square bg-primary" data-width="64%" />
                                                    <div className="budget-price-label">
                                                        {item.outPrice}

                                                    </div>
                                                </div>
                                                <div className="budget-price">
                                                    <div className="budget-price-square bg-danger" data-width="43%" />
                                                    <div className="budget-price-label">
                                                        {item.inPrice}

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                ))
                            }
                        </ul>
                    </div>
                    <div className="card-footer pt-3 d-flex justify-content-center">
                        <div className="budget-price justify-content-center">
                            <div className="budget-price-square bg-primary" data-width={20} />
                            <div className="budget-price-label">Selling Price</div>
                        </div>
                        <div className="budget-price justify-content-center">
                            <div className="budget-price-square bg-danger" data-width={20} />
                            <div className="budget-price-label">Budget Price</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    );
}

export default BestProduct;