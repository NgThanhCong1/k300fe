import { useState } from "react";
import { Button, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'
import { Link } from 'react-router-dom';

function CusOutInvoicePropDown(props) {
    const { item, handleBtnEditClick, handleBtnCancleClick } = props;
    const [isOpen, setIsOpen] = useState(false)
    const toggle = () => {
        setIsOpen(!isOpen)
    }
    return (
        <Dropdown isOpen={isOpen} toggle={toggle}>
            <DropdownToggle className="bg-info" caret>
                Dropdown
            </DropdownToggle>
            <DropdownMenu right>
                <DropdownItem>
                    <Button
                        onClick={() => handleBtnEditClick(item)}
                        color="warning"
                        style={{ width: "100%" }}
                    >
                        {
                            item.status === 1 ? "Approve"
                                : item.status === 2 ? "Shipping"
                                    : item.status === 3 ? "Recieved"
                                        : item.status === 0 ? "Done"
                                            : item.status === 4 ? "Done"
                                                : ""}
                    </Button>
                </DropdownItem>
                <DropdownItem>
                    <Button
                        onClick={() => handleBtnCancleClick(item)}
                        color="danger"
                        style={{ width: "100%" }}
                    >
                        <i class="far fa-times-circle"></i>
                    </Button>

                </DropdownItem>
                <DropdownItem>
                    <Link
                        to={`/outInvoice/detail/${item.id}`}
                    >
                        <Button
                            color="primary"
                            style={{ width: "100%" }}
                        >
                            Detail
                        </Button>
                    </Link>
                </DropdownItem>
            </DropdownMenu>
        </Dropdown>
    )
}

export default CusOutInvoicePropDown;