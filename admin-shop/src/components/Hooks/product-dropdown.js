import { useState } from "react";
import { Button, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'
import { Link } from 'react-router-dom';
import { Can } from '../PageContents/Auth/CASL/can';

function CusPropDown(props) {
    const { item, handleDelete } = props;
    const [isOpen, setIsOpen] = useState(false)
    const toggle = () => {
        setIsOpen(!isOpen)
    }
    return (
        <Dropdown isOpen={isOpen} toggle={toggle}>
            <DropdownToggle className="bg-info" caret>
                Dropdown
            </DropdownToggle>
            <DropdownMenu right>
                <DropdownItem>
                    <Link
                        to={`/product/detail/${item.id}`}
                    >
                        <Button
                            color="primary"
                            style={{ width: "100%" }}
                        >
                            <i class="fas fa-info-circle"></i>
                        </Button>
                    </Link>
                </DropdownItem>
                <Can I="update" a="Product">
                    <DropdownItem>
                        <Link
                            to={`/product/update/${item.id}`}
                        >
                            <Button
                                color="warning"
                                style={{ width: "100%" }}
                            >
                                <i className="fas fa-edit"></i>
                            </Button>
                        </Link>
                    </DropdownItem>
                    <DropdownItem>
                        <Button
                            onClick={() => handleDelete(item.id, item.name)}
                            style={{ width: "100%" }}
                            color="danger"
                        >
                            <i className="fas fa-trash"></i>
                        </Button>
                    </DropdownItem>
                </Can>
            </DropdownMenu>
        </Dropdown>
    )
}

export default CusPropDown;