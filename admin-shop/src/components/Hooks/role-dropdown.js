import { useState } from "react";
import { Button, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'
import { Link } from 'react-router-dom';

function CusPropDown(props) {
    const { item, handleDelete } = props;
    const [isOpen, setIsOpen] = useState(false)
    const toggle = () => {
        setIsOpen(!isOpen)
    }
    return (
        <Dropdown isOpen={isOpen} toggle={toggle}>
            <DropdownToggle className="bg-info" caret>
                Dropdown
            </DropdownToggle>
            <DropdownMenu right>
                <DropdownItem>
                    <Link
                        to={`/role/update/${item.name}`}
                    >
                        <Button
                            color="warning"
                            style={{ marginRight: "16px" }}
                        >
                            <i className="fas fa-edit"></i>
                        </Button>
                    </Link>
                </DropdownItem>
                <DropdownItem>
                    <Button
                        onClick={() => handleDelete(item.name)}
                        color="danger"
                    >
                        <i className="fas fa-trash"></i>
                    </Button>
                </DropdownItem>
            </DropdownMenu>
        </Dropdown>
    )
}

export default CusPropDown;