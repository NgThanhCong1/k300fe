import { useState } from "react";
import { Button, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'
import { Link } from 'react-router-dom';

function CusPropDown(props) {
    const { item, handleDelete } = props;
    console.log(item);
    const [isOpen, setIsOpen] = useState(false)
    const toggle = () => {
        setIsOpen(!isOpen)
    }
    return (
        <Dropdown isOpen={isOpen} toggle={toggle}>
            <DropdownToggle className="bg-info" caret>
                Dropdown
            </DropdownToggle>
            <DropdownMenu right>
                <DropdownItem>
                    <Link
                        to={`/role/assign/${item.userName}`}
                    >
                        <Button
                            color="warning"
                            style={{ marginRight: "16px" }}
                        >
                            Assgin role
                        </Button>
                    </Link>
                </DropdownItem>
                <DropdownItem>
                    <Button
                        onClick={() => handleDelete(item.id, item.userName)}
                        color="danger"
                    >
                        <i className="fas fa-trash"></i>
                    </Button>
                </DropdownItem>
            </DropdownMenu>
        </Dropdown>
    )
}

export default CusPropDown;