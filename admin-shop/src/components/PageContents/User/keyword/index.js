import React, { useState, useEffect } from 'react';
import BoxKeyword from './keyword';
import Loading from 'components/Commons/Loading';
import generalApi from 'api/generalApi';

//handle data
function Keyword() {
    const [listKeywords, setListKeywords] = useState([])
    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [filter, setFilter] = useState({
        _limit: 10,
        _page: 1,
        keyword: 'null',
    })
    const [pagination, setPagination] = useState({
        _page: 1,
        _limit: 10,
        _totalRows: 1,
    })
    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await generalApi.get2MonthKeywords(filter);
                if (response.code === 1) {
                    setListKeywords(response.data.listKeywords)
                    setPagination(response.data.pagination)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, filter]);

    function handleSearch(newFilter) {
        if (!newFilter) return
        setFilter({
            ...filter,
            _page: 1,
            keyword: newFilter.searchTerm ? newFilter.searchTerm : 'null',
        })
    }

    function handlePageChange(newPage) {
        if (!newPage) return
        setFilter({
            ...filter,
            _page: newPage,
        })
    }

    return (
        isFetchComplete ?
            <BoxKeyword
                listKeywords={listKeywords}
                pagination={pagination}
                handleSearch={handleSearch}
                handlePageChange={handlePageChange}
            />
            :
            <Loading />
    );
}

export default Keyword;