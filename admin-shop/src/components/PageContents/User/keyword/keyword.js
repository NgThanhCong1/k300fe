import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import Pagination from "react-js-pagination";
import { Link } from 'react-router-dom';
import { Table } from 'reactstrap';

BoxKeyword.propTypes = {
    listProducts: PropTypes.array,
    handleSearch: PropTypes.func,
    pagination: PropTypes.object,
    handlePageChange: PropTypes.func,
}

BoxKeyword.defaultProps = {
    listProducts: [],
    handleSearch: null,
    pagination: null,
    handlePageChange: null,
}

function BoxKeyword(props) {
    const { listKeywords, handleSearch, pagination, handlePageChange } = props
    const { _page, _limit, _totalRows } = pagination

    const totalPage = Math.ceil(_totalRows / _limit)
    const [searchTerm, setSearchTerm] = useState('')
    const [activePage, setActivePage] = useState(_page)
    const typingTimeOutRef = useRef(null)

    function onPageChange(pageNumber) {
        if (!handlePageChange) return
        setActivePage(pageNumber)
        handlePageChange(pageNumber)
    }
    function handleSearchInput(e) {
        setSearchTerm(e.target.value)
        const searchValue = e.target.value
        if (!handleSearch) return
        if (typingTimeOutRef.current) clearTimeout(typingTimeOutRef.current)
        typingTimeOutRef.current = setTimeout(() => {
            handleSearch({
                searchTerm: searchValue
            })
        }, 300)
    }

    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h4>All 2 month keywords | TotalPages: {totalPage} | You are now at: {_page}</h4>

                
                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
            <Link
                        to="/"
                    >
                        <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                    </Link>
                    </div>
            <div className="card-body">
                <div style={{ display: "flex", marginBottom: "4px" }}>
                    <div className="search-container" >
                        <form action="#">
                            <input
                                type="text"
                                placeholder="search.."
                                name="searchData"
                                value={searchTerm}
                                onChange={(e) => handleSearchInput(e)}
                            />
                        </form>
                    </div>
                </div>
                <div>
                    <Table responsive hover>
                        <thead>
                            <tr>
                                <th>Keyword</th>
                                <th>CustomerId</th>
                                <th>CreatedAt</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                listKeywords.map((item, key) => (
                                    <tr key={key} style={{ margin: "10px 0" }}>
                                        <td>{item.keyword}</td>
                                        <td>{item.customerId}</td>
                                        <td>{item.createdAt}</td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                    <div className="pagination">
                        <Pagination
                            itemClass="page-item"
                            linkClass="page-link"
                            activePage={activePage}
                            itemsCountPerPage={_limit}
                            totalItemsCount={_totalRows}
                            pageRangeDisplayed={5}
                            onChange={onPageChange}
                        />
                    </div>
                </div>
            </div>
        </>
    );
}

export default BoxKeyword;