import { useForm } from "react-hook-form";
import { Link } from 'react-router-dom';

function BoxAdd(props) {
    const { handleAddUser } = props
    const { register, handleSubmit, formState: { errors } } = useForm();

    const onSubmit = data => {
        handleAddUser({
            firstName: data.firstName,
            lastName: data.lastName,
            username: data.username,
            password: data.password,
        })
    }
    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h6 class="mb-2 text-primary">Add user</h6>
                   
                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
            <Link
                        to="/user/home"
                    >
                        <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                    </Link>
                    </div>
            <div className="card-body">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div class="row">

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="firstName">firstName</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="firstName"
                                    placeholder="Enter firstName"
                                    {...register('firstName', { required: true })}
                                />
                                {errors.firstName && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="lastName">lastName</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="lastName"
                                    placeholder="Enter lastName"
                                    {...register('lastName', { required: true })}
                                />
                                {errors.lastName && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="username">Email</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="username"
                                    placeholder="Enter Email"
                                    {...register('username', { required: true })}
                                />
                                {errors.username && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="password">password</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="password"
                                    placeholder="Enter password"
                                    {...register('password', { required: true, minLength: 10 })}
                                />
                                {errors.password && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <button className="btn btn-primary" type="submit">Add</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>
    );
}

export default BoxAdd;