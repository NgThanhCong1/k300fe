import React from 'react';
import BoxAdd from './add';
import { useParams } from 'react-router-dom';
import userApi from 'api/userApi';

function AddUser() {
    let { role } = useParams();

    async function handleAddUser(addUser) {
        if (!addUser) return;
        try {
            const response = await userApi.addUser(role, addUser);
            if (response.code === 1) {
                alert(response.message)
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    return (
        <BoxAdd
            handleAddUser={handleAddUser}
        />
    );
}

export default AddUser;