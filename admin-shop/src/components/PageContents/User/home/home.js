import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import Pagination from "react-js-pagination";
import { Link } from 'react-router-dom';
import { Button, Table } from 'reactstrap'
import { Can } from '../../Auth/CASL/can';

BoxHome.propTypes = {
    listProducts: PropTypes.array,
    handleSearch: PropTypes.func,
    pagination: PropTypes.object,
    handlePageChange: PropTypes.func,
}

BoxHome.defaultProps = {
    listProducts: [],
    handleSearch: null,
    pagination: null,
    handlePageChange: null,
}

function BoxHome(props) {

    const { role, listUsers, handleSearch, handleDelete, pagination, handlePageChange } = props
    const { _page, _limit, _totalRows } = pagination

    const totalPage = Math.ceil(_totalRows / _limit)
    const [searchTerm, setSearchTerm] = useState('')
    const [activePage, setActivePage] = useState(_page)
    const typingTimeOutRef = useRef(null)

    function onPageChange(pageNumber) {
        if (!handlePageChange) return
        setActivePage(pageNumber)
        handlePageChange(pageNumber)
    }

    function handleSearchInput(e) {
        setSearchTerm(e.target.value)
        const searchValue = e.target.value
        if (!handleSearch) return
        if (typingTimeOutRef.current) clearTimeout(typingTimeOutRef.current)
        typingTimeOutRef.current = setTimeout(() => {
            handleSearch({
                searchTerm: searchValue
            })
        }, 300)
    }

    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h4>All users | Total pages: {totalPage} | You are now at: {_page}</h4>


                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
                <Link
                    to="/"
                >
                    <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                </Link>
            </div>
            <div className="card-body">
                <div style={{ display: "flex", marginBottom: "4px" }}>
                    {
                        role.includes("Staff")
                        && <Can I="create" a="User">
                            <Link
                                to={`/user/add/${role}`}
                                style={{ flex: "0.15", marginRight: "4px" }}
                            >
                                <Button
                                    color="primary"
                                    style={{ width: "100%" }}
                                >
                                    <i className="fas fa-plus-square"></i>
                                </Button>
                            </Link>
                        </Can>
                    }

                    <div className="search-container" >
                        <form action="#">
                            <input
                                type="text"
                                placeholder="search.."
                                name="searchData"
                                value={searchTerm}
                                onChange={(e) => handleSearchInput(e)}
                            />
                        </form>
                    </div>
                </div>
                <div>
                    <Table hover>
                        <thead>
                            <tr>
                                <th>Avatar</th>
                                <th>Fullname</th>
                                <th>Address</th>
                                <th>Email</th>
                                <th>Phone number</th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                listUsers.map((item) => (
                                    <tr key={item.userName}>
                                        <td>
                                            {
                                                item.avatar ?
                                                    <img
                                                        src={`${process.env.REACT_APP_IMAGE_URL}${item.avatar}`}
                                                        style={{ maxWidth: "50px" }}
                                                        alt="user avatar here  "
                                                    />
                                                    :
                                                    <img
                                                        src={`${process.env.REACT_APP_IMAGE_URL}/Images/Users/user.png`}
                                                        style={{ maxWidth: "50px" }}
                                                        alt="user avatar here  "
                                                    />
                                            }
                                        </td>
                                        <td>{item.firstName} {item.lastName}</td>
                                        <td
                                            style={{
                                                minWidth: "250px"
                                            }}
                                        >
                                            {item.address}
                                        </td>
                                        <td>{item.email}</td>
                                        <td>{item.phoneNumber}</td>
                                        <td>
                                            <Can I="delete" a="User">
                                                <div
                                                    style={{
                                                        display: "flex"
                                                    }}
                                                >
                                                    <Link
                                                        to={`/role/assign/${item.userName}`}
                                                    >
                                                        <Button
                                                            color="warning"
                                                        >
                                                            Assign
                                                        </Button>
                                                    </Link>
                                                    <Button
                                                        onClick={() => handleDelete(item.id, item.userName)}
                                                        color="danger"
                                                    >
                                                        <i className="fas fa-trash"></i>
                                                    </Button>
                                                </div>
                                            </Can>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                    <div className="pagination">
                        <Pagination
                            itemClass="page-item"
                            linkClass="page-link"
                            activePage={activePage}
                            itemsCountPerPage={_limit}
                            totalItemsCount={_totalRows}
                            pageRangeDisplayed={5}
                            onChange={onPageChange}
                        />
                    </div>
                </div>
            </div>
        </>
    );
}

export default BoxHome;