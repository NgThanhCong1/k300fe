import React, { useState, useEffect } from 'react';
import BoxHome from './home';
import Loading from 'components/Commons/Loading';
import { useParams } from 'react-router-dom';
import userApi from 'api/userApi';

//handle data
function HomeUser() {
    let { role } = useParams();
    const [listUsers, setListUsers] = useState([])
    const [isFetchComplete, setIsFetchComplete] = useState(false)

    const [filter, setFilter] = useState({
        _limit: 10,
        _page: 1,
        userName: 'null',
    })
    const [pagination, setPagination] = useState({
        _page: 1,
        _limit: 10,
        _totalRows: 1,
    })

    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await userApi.getUserByRole({ ...filter, role: role });
                if (response.code === 1) {
                    setListUsers(response.data.listApplicationUsers)
                    setPagination(response.data.pagination)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, filter, role]);

    function handleSearch(newFilter) {
        if (!newFilter) return
        setFilter({
            ...filter,
            _page: 1,
            userName: newFilter.searchTerm ? newFilter.searchTerm : 'null',
        })
    }

    function handlePageChange(newPage) {
        if (!newPage) return
        setFilter({
            ...filter,
            _page: newPage,
        })
    }

    async function deleteUser(id) {
        if (!id) return
        try {
            const response = await userApi.deleteUser({
                id: id
            });
            if (response.code === 1) {
                setIsFetchComplete(false);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }
    function handleDelete(id, name) {
        if (window.confirm("delete ?" + name)) {
            deleteUser(id);
        } else {
            console.log("no delete")
        }
    }
    return (
        isFetchComplete ?

            <BoxHome
                role={role}
                listUsers={listUsers}
                pagination={pagination}
                handleDelete={handleDelete}
                handleSearch={handleSearch}
                handlePageChange={handlePageChange}
            />
            :
            <Loading />
    );
}

export default HomeUser;