import React, { useState, useEffect } from 'react';
import BoxHome from './home';
import { connectionNotifyHub } from 'components/Constant';
import Loading from 'components/Commons/Loading';
import discountApi from 'api/discountApi';
import userApi from 'api/userApi';
import notifyApi from 'api/notifyApi';

function HomeDiscount() {
    const [listDiscounts, setListDiscounts] = useState([])
    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [filter, setFilter] = useState({
        _limit: 10,
        _page: 1,
        nameLike: 'null',
    })
    const [pagination, setPagination] = useState({
        _page: 1,
        _limit: 10,
        _totalRows: 1,
    })
    useEffect(() => {

        async function getDataFromApi() {
            try {
                const response = await discountApi.getAllDiscounts(filter);
                if (response.code === 1) {
                    setListDiscounts(response.data.listDiscount)
                    setPagination(response.data.pagination)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api add discount", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, filter]);

    function handleSearch(newFilter) {
        if (!newFilter) return
        setFilter({
            ...filter,
            _page: 1,
            nameLike: newFilter.searchTerm ? newFilter.searchTerm : 'null',
        })
    }

    function handlePageChange(newPage) {
        if (!newPage) return
        setFilter({
            ...filter,
            _page: newPage,
        })
    }

    async function getAllCustomersToSendDiscount(discount) {

        try {
            const response = await userApi.getAllCustomers();
            if (response.code === 1) {
                response.data.listApplicationUsers.forEach(customer =>
                    addDiscountNotify(discount, customer.id)
                )
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api add discount", error);
        }
    }

    async function addDiscountNotify(discount, customerId) {

        try {
            const response = await notifyApi.addNotify({
                content: discount.discountCode,
                customerId: customerId,
                type: 5
            });
            if (response.code === 1) {
                if (connectionNotifyHub)
                    connectionNotifyHub.send("UpdateListNotify", customerId);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api add discount", error);
        }
    }

    async function deleteDiscount(id) {

        if (!id) return
        try {
            const response = await discountApi.deleteDiscount({ id });
            if (response.code === 1) {
                setIsFetchComplete(false);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api add discount", error);
        }
    }

    function handleDelete(id, name) {
        if (window.confirm("delete ?" + name)) {
            deleteDiscount(id);
        } else {
            console.log("no delete")
        }
    }
    return (
        isFetchComplete ?
            <BoxHome
                listDiscounts={listDiscounts}
                pagination={pagination}
                handleDelete={handleDelete}
                handleSearch={handleSearch}
                handlePageChange={handlePageChange}
                getAllCustomersToSendDiscount={getAllCustomersToSendDiscount}
            />
            :
            <Loading />
    );
}

export default HomeDiscount;