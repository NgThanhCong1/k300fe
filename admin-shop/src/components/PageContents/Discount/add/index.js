import React from 'react';
import BoxAdd from './add';
import discountApi from 'api/discountApi';

function AddDiscount() {

    async function handleAddDiscount(addDiscount) {
        try {
            const response = await discountApi.addDiscount(addDiscount);
            if (response.code === 1) {
                alert(response.message)
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api add discount", error);
        }
    }

    return (
        <BoxAdd
            handleAddDiscount={handleAddDiscount}
        />
    );
}

export default AddDiscount;