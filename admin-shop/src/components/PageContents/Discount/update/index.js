import BoxUpdate from './update';
import { useParams } from 'react-router';
import { useState, useEffect } from 'react';
import Loading from 'components/Commons/Loading';
import discountApi from 'api/discountApi';

function UpdateDiscount() {

    const [isFetchComplete, setIsFetchComplete] = useState(false);
    const [discount, setDiscount] = useState(null);
    let { id } = useParams();

    useEffect(() => {
        async function getDataFromApi() {

            try {
                const response = await discountApi.getDataForUpdateDiscount(id);
                if (response.code === 1) {
                    setDiscount(response.data)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api add discount", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, id]);

    async function handleUpdateDiscount(updateDiscount) {

        if (!updateDiscount) return;
        try {
            const response = await discountApi.updateDiscount({ ...updateDiscount, id: id });
            if (response.code === 1) {
                alert(response.message)
                setIsFetchComplete(false)
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api add discount", error);
        }
    }
    return (
        isFetchComplete ?
            <BoxUpdate
                discount={discount}
                handleUpdateDiscount={handleUpdateDiscount}
            />
            :
            <Loading />
    )
}

export default UpdateDiscount;