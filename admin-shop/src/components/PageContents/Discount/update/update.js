import { useForm } from "react-hook-form";
import { Link } from 'react-router-dom';

function BoxUpdate(props) {
    const { discount, handleUpdateDiscount } = props
    const { register, handleSubmit, formState: { errors } } = useForm();

    const onSubmit = data => {
        handleUpdateDiscount({
            discountCode: data.discountCode,
            quantity: data.quantity,
            price: data.price,
        })
    }
    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h4 class="mb-2 text-primary">Update discount</h4>
                   
                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
            <Link
                        to="/discount/home"
                    >
                        <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                    </Link>
                    </div>
            <div className="card-body">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div class="row">

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="discountCode">Category name</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="discountCode"
                                    placeholder="Enter code"
                                    {...register('discountCode', { required: true })}
                                    defaultValue={discount.discountCode}
                                />
                                {errors.discountCode && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="quantity">Quantity</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="quantity"
                                    placeholder="Entern quantity"
                                    {...register('quantity', { required: true })}
                                    defaultValue={discount.quantity}
                                />
                                {errors.quantity && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="price">Price</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="price"
                                    placeholder="Enter price"
                                    {...register('price', { required: true })}
                                    defaultValue={discount.price}
                                />
                                {errors.price && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>

                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <button className="btn btn-primary" type="submit">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>

    );
}

export default BoxUpdate;