import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Pagination from "react-js-pagination";
import { Link } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { Can } from '../../Auth/CASL/can';

BoxHome.propTypes = {
    listProducts: PropTypes.array,
    handleSearch: PropTypes.func,
    pagination: PropTypes.object,
    handlePageChange: PropTypes.func,
}

BoxHome.defaultProps = {
    listProducts: [],
    handleSearch: null,
    pagination: null,
    handlePageChange: null,
}

function BoxHome(props) {
    const { listStores, handleDelete, pagination, handlePageChange } = props
    const { _page, _limit, _totalRows } = pagination

    const totalPage = Math.ceil(_totalRows / _limit)
    const [activePage, setActivePage] = useState(_page)

    function onPageChange(pageNumber) {
        if (!handlePageChange) return
        setActivePage(pageNumber)
        handlePageChange(pageNumber)
    }

    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h4>All stores | Total pages: {totalPage} | You are now at: {_page}</h4>


                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
                <Link
                    to="/"
                >
                    <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                </Link>
            </div>
            <div className="card-body">
                <div style={{ display: "flex", marginBottom: "4px" }}>
                    <Can I="create" a="Store">
                        <Link
                            to="/store/add"
                            style={{ flex: "0.15", marginRight: "4px" }}
                        >
                            <Button
                                color="primary"
                                style={{ width: "100%" }}
                            >
                                <i className="fas fa-plus-square"></i>
                            </Button>
                        </Link>
                    </Can>

                </div>
                <div>
                    <Table hover>
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <Can I="update" a="Store">
                                    <th>Action</th>
                                </Can>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                listStores.map((item) => (
                                    <tr key={item.id}>
                                        <td>
                                            <img
                                                src={`${process.env.REACT_APP_IMAGE_URL}${item.image}`}
                                                style={{ maxWidth: "100px" }}
                                                alt="store here  "
                                            />
                                        </td>
                                        <td>{item.address}</td>
                                        <td>{item.phone}</td>
                                        <Can I="update" a="Store">
                                            <td>
                                                <Link
                                                    to={`/store/update/${item.id}`}
                                                >
                                                    <Button
                                                        color="warning"
                                                    >
                                                        <i className="fas fa-edit"></i>
                                                    </Button>
                                                </Link>
                                                <Button
                                                    onClick={() => handleDelete(item.id, item.address)}
                                                    color="danger"
                                                >
                                                    <i className="fas fa-trash"></i>
                                                </Button>
                                            </td>
                                        </Can>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                    <div className="pagination">
                        <Pagination
                            itemClass="page-item"
                            linkClass="page-link"
                            activePage={activePage}
                            itemsCountPerPage={_limit}
                            totalItemsCount={_totalRows}
                            pageRangeDisplayed={5}
                            onChange={onPageChange}
                        />
                    </div>
                </div>
            </div>
        </>
    );
}

export default BoxHome;