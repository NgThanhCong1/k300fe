import React, { useState, useEffect } from 'react';
import BoxHome from './home';
import Loading from 'components/Commons/Loading';
import storeApi from 'api/storeApi';

//handle data
function HomeStore() {

    const [listStores, setListStores] = useState([])
    const [isFetchComplete, setIsFetchComplete] = useState(false)

    const [filter, setFilter] = useState({
        _limit: 10,
        _page: 1,
    })

    const [pagination, setPagination] = useState({
        _page: 1,
        _limit: 10,
        _totalRows: 1,
    })

    useEffect(() => {

        async function getDataFromApi() {
            try {
                const response = await storeApi.getAllStores(filter);
                if (response.code === 1) {
                    setListStores(response.data.listStores)
                    setPagination(response.data.pagination)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, filter]);

    function handlePageChange(newPage) {
        if (!newPage) return
        setFilter({
            ...filter,
            _page: newPage,
        })
    }

    async function deleteStore(id) {
        if (!id) return
        try {
            const response = await storeApi.deleteStore({ id: id });
            if (response.code === 1) {
                setIsFetchComplete(false);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    function handleDelete(id, name) {
        if (window.confirm("delete ?" + name)) {
            deleteStore(id);
        } else {
            console.log("no delete")
        }
    }

    return (
        isFetchComplete ?
            <BoxHome
                listStores={listStores}
                pagination={pagination}
                handleDelete={handleDelete}
                handlePageChange={handlePageChange}
            />
            :
            <Loading />
    );
}

export default HomeStore;