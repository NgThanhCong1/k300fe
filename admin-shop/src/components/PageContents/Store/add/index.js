import React from 'react';
import BoxAdd from './add';
import storeApi from 'api/storeApi';

function AddStore() {

    async function handleAddStore(addStore) {
        let formData = new FormData()
        for (var index = 0; index < addStore.images.length; ++index) {
            formData.append('imageFiles', addStore.images[index])
        }
        formData.append('address', addStore.address);
        formData.append('phone', addStore.phone);

        try {
            const response = await storeApi.addStore(formData);
            if (response.code === 1) {
                alert(response.message)
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    return (
        <BoxAdd
            handleAddStore={handleAddStore}
        />
    );
}

export default AddStore;