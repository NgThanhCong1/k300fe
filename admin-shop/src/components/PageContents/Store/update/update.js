import { useState } from "react";
import { useForm } from "react-hook-form";
import { Link } from 'react-router-dom';

function BoxUpdate(props) {

    const { store, handleUpdateStore } = props
    const [listFiles, setListFiles] = useState([])
    const [listFilesObj, setListFilesObj] = useState([])
    const { register, handleSubmit, formState: { errors } } = useForm();
    const fileArray = []
    const fileObj = []

    const onSubmit = data => {
        handleUpdateStore({
            address: data.address,
            phone: data.phone,
            images: listFilesObj,
        })
    }

    function handleUploadFiles(e) {
        fileObj.push(e.target.files)
        for (let i = 0; i < fileObj[0].length; i++) {
            fileArray.push(URL.createObjectURL(fileObj[0][i]))
        }
        setListFiles(fileArray)
        setListFilesObj(e.target.files)
    }

    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h6 class="mb-2 text-primary">Update store</h6>
                   
                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
            <Link
                        to="/store/home"
                    >
                        <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                    </Link>
                    </div>
            <div className="card-body">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div class="row">

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="address">Store address</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="address"
                                    placeholder="Enter Store address"
                                    {...register('address', { required: true })}
                                    defaultValue={store.address}
                                />
                                {errors.address && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="phone">Store phone</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="phone"
                                    placeholder="Enter Store phone"
                                    {...register('phone', { required: true })}
                                    defaultValue={store.phone}
                                />
                                {errors.phone && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <label htmlFor="listFiles">Pictures of store:</label>
                            <input
                                type="file"
                                className="form-control"
                                id="listFiles"
                                onChange={(e) => handleUploadFiles(e)}
                            />
                            <div>
                                {listFiles.length === 0 ?
                                    <img style={{ maxWidth: "100px" }} src={`${process.env.REACT_APP_IMAGE_URL}${store.image}`} alt="..." />
                                    :

                                    (listFiles).map(url => (
                                        <img style={{ maxWidth: "100px" }} src={url} alt="..." />
                                    ))
                                }
                            </div>
                        </div>

                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <button className="btn btn-primary" type="submit">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>

    );
}

export default BoxUpdate;