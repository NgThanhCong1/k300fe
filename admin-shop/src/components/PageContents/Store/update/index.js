import BoxUpdate from './update';
import { useParams } from 'react-router';
import { useState, useEffect } from 'react';
import Loading from 'components/Commons/Loading';
import storeApi from 'api/storeApi';

function UpdateStore() {

    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [store, setStore] = useState(null)
    let { id } = useParams()

    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await storeApi.getDataForUpdateStore(id);
                if (response.code === 1) {
                    setStore(response.data)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, id]);

    async function handleUpdateStore(updateStore) {
        let formData = new FormData()
        for (var index = 0; index < updateStore.images.length; ++index) {
            formData.append('imageFiles', updateStore.images[index])
        }
        formData.append('address', updateStore.address);
        formData.append('phone', updateStore.phone);
        formData.append('id', id);

        try {
            const response = await storeApi.updateStore(formData);
            if (response.code === 1) {
                alert(response.message)
                setIsFetchComplete(false)
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }
    return (
        isFetchComplete ?
            <BoxUpdate
                store={store}
                handleUpdateStore={handleUpdateStore}
            />
            :
            <Loading />
    )
}

export default UpdateStore;