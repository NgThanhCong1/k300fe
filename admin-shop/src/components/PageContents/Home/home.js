import BestProduct from "components/Commons/BestProduct";
import Invoice from "components/Commons/Invoice";
import { rootPathImg } from 'components/Constant';
import { Link } from "react-router-dom";
import { useState, useRef, useEffect } from "react";
import { Can } from '../Auth/CASL/can';
import { useSelector } from "react-redux";

export default function BoxHome(props) {

    const { dashboard, addChat, listChats } = props;

    const [listTop10PopularProducts,] = useState(dashboard.listSubProductStatics);
    const [listCategoryStatics,] = useState(dashboard.listCategoryStatics);
    const [listInInvoices,] = useState(dashboard.listInInvoices);
    const [listSearchStaticDashBoards,] = useState(dashboard.listSearchStaticDashBoards);
    const [content, setContent] = useState(null);
    const userInfor = useSelector(state => state.userReducer);
    const messagesEndRef = useRef(null);
    const scrollToBottom = () => {
        listChats && messagesEndRef.current.scrollIntoView({ behavior: "smooth", block: 'nearest', inline: 'start' })
    }
    useEffect(scrollToBottom, [listChats]);

    return (
        <>
            <div className="card-body">
                <div class="content-header">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Dashboard v1</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-3 col-6">
                        {/* small box */}
                        <div className="small-box bg-yellow">
                            <div className="inner">
                                <h3>{dashboard.static.productCount}</h3>
                                <p>Total Products</p>
                            </div>
                            <div className="icon">
                                <i className="fa fa-shopping-basket" />
                            </div>
                            <Can I="read" a="Product">
                                <Link
                                    to="/product/home"
                                    className="small-box-footer"
                                >
                                    More info <i className="fas fa-arrow-circle-right" />
                                </Link>
                            </Can>
                        </div>
                    </div>
                    <div className="col-lg-3 col-6">
                        {/* small box */}
                        <div className="small-box bg-info">
                            <div className="inner">
                                <h3>{dashboard.static.outInvoiceCount}</h3>
                                <p>Total Orders</p>
                            </div>
                            <div className="icon">
                                <i className="ion ion-bag" />
                            </div>
                            <Can I="read" a="OutportProduct">
                                <Link
                                    to="/outInvoice/home"
                                    className="small-box-footer"
                                >
                                    More info <i className="fas fa-arrow-circle-right" />
                                </Link>
                            </Can>

                        </div>
                    </div>
                    <div className="col-lg-3 col-6">
                        {/* small box */}
                        <div className="small-box bg-blue">
                            <div className="inner">
                                <h3>{dashboard.static.discountCount}</h3>
                                <p>Total Cupons</p>
                            </div>
                            <div className="icon">
                                <i class="fas fa-dollar-sign"></i>
                            </div>

                            <Can I="read" a="Discount">
                                <Link
                                    to="/discount/home"
                                    className="small-box-footer"
                                >
                                    More info <i className="fas fa-arrow-circle-right" />
                                </Link>
                            </Can>
                        </div>
                    </div>
                    <div className="col-lg-3 col-6">
                        {/* small box */}
                        <div className="small-box bg-olive">
                            <div className="inner">
                                <h3>{dashboard.static.categoryCount}</h3>
                                <p>Total Categories</p>
                            </div>
                            <div className="icon">
                                <i class="fa fa-list-alt"></i>
                            </div>

                            <Can I="read" a="Category">
                                <Link
                                    to="/category/home"
                                    className="small-box-footer"
                                >
                                    More info <i className="fas fa-arrow-circle-right" />
                                </Link>
                            </Can>
                        </div>
                    </div>
                    <div className="col-lg-3 col-6">
                        {/* small box */}
                        <div className="small-box bg-red">
                            <div className="inner">
                                <h3>{dashboard.static.brandCount}</h3>
                                <p>Total Brands</p>
                            </div>
                            <div className="icon">
                                <i class="far fa-copyright"></i>
                            </div>

                            <Can I="read" a="Brand">
                                <Link
                                    to="/brand/home"
                                    className="small-box-footer"
                                >
                                    More info <i className="fas fa-arrow-circle-right" />
                                </Link>
                            </Can>
                        </div>
                    </div>
                    <div className="col-lg-3 col-6">
                        {/* small box */}
                        <div className="small-box bg-red">
                            <div className="inner">
                                <h3>{dashboard.static.distributorCount}</h3>
                                <p>Total Distributors</p>
                            </div>
                            <div className="icon">
                                <i class="fas fa-chart-network"></i>
                            </div>

                            <Can I="read" a="Distributor">
                                <Link
                                    to="/distributor/home"
                                    className="small-box-footer"
                                >
                                    More info <i className="fas fa-arrow-circle-right" />
                                </Link>
                            </Can>
                        </div>
                    </div>
                    <div className="col-lg-3 col-6">
                        {/* small box */}
                        <div className="small-box bg-maroon">
                            <div className="inner">
                                <h3>{dashboard.static.inInvoiceCount}</h3>
                                <p>Total InInvoices</p>
                            </div>
                            <div className="icon">
                                <i class="fas fa-file-invoice-dollar"></i>
                            </div>

                            <Can I="read" a="ImportProduct">
                                <Link
                                    to="/import-product/home"
                                    className="small-box-footer"
                                >
                                    More info <i className="fas fa-arrow-circle-right" />
                                </Link>
                            </Can>
                        </div>
                    </div>
                    <div className="col-lg-3 col-6">
                        {/* small box */}
                        <div className="small-box bg-green">
                            <div className="inner">
                                <h3>{dashboard.static.keywordCount}</h3>
                                <p>Total Keywords</p>
                            </div>
                            <div className="icon">
                                <i class="fas fa-file-word"></i>
                            </div>

                            <Can I="read" a="Keyword">
                                <Link
                                    to="/keyword/home"
                                    className="small-box-footer"
                                >
                                    More info <i className="fas fa-arrow-circle-right" />
                                </Link>
                            </Can>
                        </div>
                    </div>
                    <div className="col-lg-3 col-6">
                        {/* small box */}
                        <div className="small-box bg-teal">
                            <div className="inner">
                                <h3>{dashboard.static.userCount}</h3>
                                <p>New users this month</p>
                            </div>
                            <div className="icon">
                                <i class="fas fa-user-friends"></i>
                            </div>

                            <Can I="read" a="User">
                                <Link
                                    to="/user/home/Customer"
                                    className="small-box-footer"
                                >
                                    More info <i className="fas fa-arrow-circle-right" />
                                </Link>
                            </Can>
                        </div>
                    </div>
                    <div className="col-lg-3 col-6">
                        {/* small box */}
                        <div className="small-box bg-olive">
                            <div className="inner">
                                <h3>{dashboard.static.roleCount}</h3>
                                <p>Total Roles</p>
                            </div>
                            <div className="icon">
                                <i class="far fa-address-book"></i>
                            </div>

                            <Can I="read" a="Role">
                                <Link
                                    to="/role/home"
                                    className="small-box-footer"
                                >
                                    More info <i className="fas fa-arrow-circle-right" />
                                </Link>
                            </Can>
                        </div>
                    </div>
                </div>
            </div>

            <div className="card-body">
                <div class="content-header">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Director chat</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div id="frame">
                    <div className="content">
                        <div className="messages">
                            <ul
                                style={{ display: "flex", flexDirection: "column-reverse" }}
                            >
                                <li ref={messagesEndRef}></li>
                                {
                                    listChats && listChats.map(item => (
                                        <li
                                            className={
                                                item.staffId === userInfor.staffInfor.id
                                                    ? "replies"
                                                    : "send"
                                            }
                                            key={item.createdAt}
                                        // ref={messagesEndRef}
                                        >
                                            {
                                                item.staffAvatar ?
                                                    <img
                                                        src={`${rootPathImg}${item.staffAvatar}`}
                                                        style={{
                                                            width: "30px",
                                                            height: "30px",
                                                            objectFit: "cover"
                                                        }}
                                                        alt="user avatar here  "
                                                    />
                                                    :
                                                    <img
                                                        src={`${rootPathImg}/Images/Users/user.png`}
                                                        style={{
                                                            width: "30px",
                                                            height: "30px",
                                                            objectFit: "cover"
                                                        }}
                                                        alt="user avatar here  "
                                                    />
                                            }
                                            <p>{item.content}</p>
                                        </li>
                                    ))
                                }
                            </ul>
                        </div>
                        <div className="message-input">
                            <div className="wrap">
                                <input
                                    style={{ marginLeft: "20px" }}
                                    type="text"
                                    onChange={(e) => setContent(e.target.value)}
                                    placeholder="Write your message..."
                                />
                                <button
                                    className="submit"
                                    onClick={() => addChat(content)}
                                >
                                    <i className="fa fa-paper-plane" aria-hidden="true" />
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>



            <div style={{ height: "50px", background: "#f4f6f9" }}></div>

            <BestProduct
                listTop10PopularProducts={listTop10PopularProducts}
                listCategoryStatics={listCategoryStatics}
            />
            <div style={{ height: "50px", background: "#f4f6f9" }}></div>
            <Invoice
                listInInvoices={listInInvoices}
                listSearchStaticDashBoards={listSearchStaticDashBoards}
            />
        </>
    );
}