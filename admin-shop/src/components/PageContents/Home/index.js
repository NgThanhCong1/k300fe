import React, { useState, useEffect } from 'react';
import BoxHome from './home';
import Loading from 'components/Commons/Loading';
import { connectionChatHub } from 'components/Constant';
import generalApi from 'api/generalApi';
import { useSelector } from 'react-redux';

export default function HomeAdmin() {
    const [isFetchComplete, setIsFetchComplete] = useState(false);
    const [dashboard, setDashboard] = useState(null);
    const [listChats, setListChats] = useState([]);
    const userInfor = useSelector(state => state.userReducer);
    useEffect(() => {
        if (connectionChatHub.connectionState === 'Connected') {
            connectionChatHub.send("UpdateListDirectChat")
        }
    }, [])

    useEffect(() => {
        console.log(connectionChatHub)
        if (connectionChatHub) {
            connectionChatHub.on("ReceiveMessage", newListChats => {
                setListChats(newListChats)
            });
        }
    }, [])

    useEffect(() => {

        async function getDataFromApi() {
            try {
                const response = await generalApi.getDataForDashboard();
                if (response.code === 1) {
                    setDashboard(response.data)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api get data for dashboard", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete]);

    async function addChat(content) {
        try {
            const response = await generalApi.addChat({
                content: content,
                staffId: userInfor.staffInfor.id,
                staffAvatar: userInfor.staffInfor.avatar
            });
            if (response.code === 1) {
                if (connectionChatHub) {
                    connectionChatHub.send("UpdateListDirectChat")
                }
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api get data for dashboard", error);
        }
    }

    return (
        isFetchComplete && listChats
            ? <BoxHome
                dashboard={dashboard}
                addChat={addChat}
                listChats={listChats}
            />
            : <Loading />
    );
}