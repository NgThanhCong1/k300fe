import React, { useState } from 'react';
import './login.css'
import { useForm } from "react-hook-form";


Register.propTypes = {

};

function Register(props) {
    const [usename, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [passwordConfirm, setPasswordConfirm] = useState('')
    const { onRegisterSubmit, onLoginBtnClick } = props;
    const { register, handleSubmit, formState: { errors } } = useForm({
        mode: 'onBlur',
    });

    const onSubmit = data => {
        if (!onRegisterSubmit) return
        onRegisterSubmit({
            username: usename,
            password: password,
        })
    }

    // function checkConfirmPassword(e) {
    //     return password === e.target.value ? true : false
    // }

    function handleLoginBtnClick() {
        if (!onLoginBtnClick) return
        onLoginBtnClick()
    }

    return (
        <>
            <div class="overlay" onclick="hide()"></div>
            <div class="formwrap">
                <div class="close-btn">

                </div>
                <div class="form-left">
                    <div class="form-left__content">
                        <h3>The K300</h3>
                        <p>Upgrade your fashion now</p>
                    </div>
                    <img src="https://i.pinimg.com/originals/6b/a6/e2/6ba6e2c31411a00988f952a556509c70.jpg" alt="" />
                </div>
                <div class="form-right">
                    <form action="" class="signup-form" id="signup-form" onSubmit={handleSubmit(onSubmit)}>
                        <div class="form-group">
                            <label for="username">USERNAME</label>
                            <input
                                type="text"
                                id="username"
                                {...register('username', { required: true })}
                                onChange={(e) => setUsername(e.target.value)}
                            />
                            {errors.username && <span style={{ color: "red" }}>This field is required</span>}

                        </div>
                        <div class="form-group">
                            <label for="Password">PASSWORD</label>
                            <input
                                type="password"
                                id="password"
                                {...register('password', { minLength: 10 })}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                            {errors.password && <span style={{ color: "red" }}>10 character atleast</span>}
                        </div>
                        <div class="form-group">
                            <label for="PasswordConfirm">PASSWORD CONFIRM</label>
                            <input
                                type="password"
                                name="PasswordConfirm"
                                id="PasswordConfirm"
                                {...register('passwordConfirm', { validate: value => (value == password) })}
                                onChange={(e) => setPasswordConfirm(e.target.value)}
                            />
                            {errors.passwordConfirm && <span style={{ color: "red" }}>Do not match</span>}
                        </div>
                        <div class="form-submit">
                            <button class="form-btn" type="submit">SUGN UP NOW</button>
                            <p onClick={() => handleLoginBtnClick()}>LOGIN NOW</p>
                        </div>
                    </form>
                </div>
            </div>
        </>
    );
}

export default Register;