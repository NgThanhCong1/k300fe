import { defineAbility } from '@casl/ability';

export default defineAbility((can, cannot) => {
    const listPermissions = localStorage.getItem('listPermissions') ? JSON.parse(localStorage.getItem('listPermissions')) : [];
    listPermissions.forEach(element => {
        can(element.permission, element.object);
    });
});