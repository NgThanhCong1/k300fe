import React from 'react';
import './login.css';
import { useForm } from "react-hook-form";
import authApi from 'api/authApi';
import { useDispatch } from 'react-redux';
import { userLogin } from 'actions';

function Login() {
    const { register, handleSubmit, formState: { errors } } = useForm({
        mode: 'onBlur',
    });
    const dispatch = useDispatch();

    const onSubmit = async data => {
        const userInfor = {
            userName: data.username,
            password: data.password
        };
        try {
            const response = await authApi.login(userInfor);
            if (response.code === 1) {
                localStorage.setItem('staffToken', response.data.jwtInfor.accessToken);
                localStorage.setItem('listPermissions', JSON.stringify(response.data.listPermissions))
                await dispatch(userLogin(response.data.user, response.data.listPermissions));
                window.location.assign('/')
            } else {
                alert(response.message);
            }
        } catch (error) {
            console.log("fail to fetch api login", error);
        }
    }

    return (
        <>
            <div class="overlay" onclick="hide()"></div>
            <div class="formwrap">
                <div class="close-btn">

                </div>
                <div class="form-left">
                    <div class="form-left__content">
                        <h3>The K300</h3>
                        <p>Upgrade your fashion now</p>
                    </div>
                    <img src="https://i.pinimg.com/originals/6b/a6/e2/6ba6e2c31411a00988f952a556509c70.jpg" alt="" />
                </div>
                <div class="form-right">
                    <form action="" class="login-form" id="login-form" onSubmit={handleSubmit(onSubmit)}>
                        <div class="form-group">
                            <label for="Username">USERNAME</label>
                            <input
                                type="text"
                                name="Username"
                                id="Username"
                                {...register('username', { required: true })}
                            />
                            {errors.username && <span style={{ color: "red" }}>This field is required</span>}
                        </div>
                        <div class="form-group">
                            <label for="Password">PASSWORD</label>
                            <input
                                type="password"
                                name="Password"
                                id="Password"
                                {...register('password', { minLength: 10 })}
                            />
                            {errors.password && <span style={{ color: "red" }}>10 character atleast</span>}

                        </div>
                        <div class="form-submit">
                            <button type="submit" class="form-btn">LOGIN NOW</button>
                            {/* <p onClick={() => handleRegisterBtnClick()}>SIGN UP HERE</p> */}
                        </div>
                    </form>
                </div>
            </div>
        </>
    );
}

export default Login;
