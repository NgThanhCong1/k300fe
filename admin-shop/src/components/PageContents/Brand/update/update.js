import { useState } from "react";
import { useForm } from "react-hook-form";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

BoxUpdate.propTypes = {
    brand: PropTypes.object,
    handleUpdateBrand: PropTypes.func,
}
BoxUpdate.defaultProps = {
    brand: null,
    handleUpdateBrand: null
}
function BoxUpdate(props) {
    const { brand, handleUpdateBrand } = props
    const [description, setDescription] = useState(brand.description)
    const { register, handleSubmit, formState: { errors } } = useForm();

    const onSubmit = data => {
        handleUpdateBrand({
            name: data.brandName,
            description: description,
        })
    }

    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h4 class="mb-2 text-primary">Update brand</h4>
                 
                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
            <Link
                        to="/brand/home"
                    >
                        <button className="btn btn-primary">
                            Back
                            <i class="fas fa-arrow-left"></i>
                        </button>
                    </Link>
                    </div>
            <div className="card-body">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div class="row">

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="brandName">Brand name</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="brandName"
                                    placeholder="Enter brand name"
                                    defaultValue={brand.name}
                                    {...register('brandName', { required: true })}
                                />
                                {errors.brandName && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">Description</label>
                                <CKEditor
                                    editor={ClassicEditor}
                                    data="<p>Hello from CKEditor 5!</p>"
                                    onReady={editor => {
                                        editor.setData(description);
                                    }}
                                    onChange={(event, editor) => {
                                        setDescription(editor.getData());
                                    }}
                                />
                            </div>
                        </div>

                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <button className="btn btn-primary" type="submit">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>

    );
}

export default BoxUpdate;