import BoxUpdate from './update';
import { useParams } from 'react-router';
import { useState, useEffect } from 'react';
import Loading from 'components/Commons/Loading';
import brandApi from 'api/brandApi';

function UpdateBrand() {
    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [brand, setBrand] = useState(null)
    let { id } = useParams()
    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await brandApi.getDataForUpdateBrand(id);
                if (response.code === 1) {
                    setBrand(response.data)
                    setIsFetchComplete(true)
                } else {
                    alert(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api get data for update brand", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, id]);

    async function handleUpdateBrand(updateBrand) {
        try {
            const response = await brandApi.updateBrand({
                ...updateBrand,
                id: id
            });
            alert(response.message);
        } catch (error) {
            console.log("fail to fetch api add brand", error);
        }
    }
    return (
        isFetchComplete ?
            <BoxUpdate
                brand={brand}
                handleUpdateBrand={handleUpdateBrand}
            />
            :
            <Loading />
    )
}

export default UpdateBrand;