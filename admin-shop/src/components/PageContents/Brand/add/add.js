import { useState } from "react";
import { useForm } from "react-hook-form";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Link } from 'react-router-dom';
import CustomAlert from "components/Commons/Alert";

function BoxAdd(props) {
    const { handleAddBrand, alertInfor } = props;
    const [description, setDescription] = useState('');
    const { register, handleSubmit, formState: { errors }, reset } = useForm({
        mode: 'onBlur',
    });

    const onSubmit = data => {
        handleAddBrand({
            name: data.brandName,
            description: description,
        })
        reset();
    }
    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h4 class="mb-2 text-primary">Add brand</h4>

                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
                <Link
                    to="/brand/home"
                >
                    <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                </Link>
            </div>

            <div
                style={{ padding: "10px 25px" }}
            >
                <CustomAlert
                    {...alertInfor}
                />
            </div>


            <div className="card-body">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="brandName">Brand name</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="brandName"
                                    placeholder="Enter brand name"
                                    {...register('brandName', { required: true })}
                                />
                                {errors.brandName && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">Description</label>
                                <CKEditor
                                    editor={ClassicEditor}
                                    onChange={(event, editor) => {
                                        setDescription(editor.getData());
                                    }}
                                />
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <button
                                    className="btn btn-primary"
                                    type="submit"
                                >
                                    Add
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>

    );
}

export default BoxAdd;