import React, { useState } from 'react';
import BoxAdd from './add';
import brandApi from 'api/brandApi';

function AddBrand() {
    const [alertInfor, setAlertInfor] = useState({
        type: 0,
        message: ''
    })

    async function handleAddBrand(addBrand) {
        try {
            const response = await brandApi.addBrand(addBrand);
            if (response.code === 1) {
                setAlertInfor({
                    type: 1,
                    message: response.message
                })
            } else {
                setAlertInfor({
                    type: -1,
                    message: response.message
                })
            }
        } catch (error) {
            console.log("fail to fetch api add brand", error);
        }
    }

    return (
        <BoxAdd
            alertInfor={alertInfor}
            handleAddBrand={handleAddBrand}
        />
    );
}

export default AddBrand;