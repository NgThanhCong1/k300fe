import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import Pagination from "react-js-pagination";
import { Link } from 'react-router-dom';
import { Button, Table } from 'reactstrap'
import { Can } from '../../Auth/CASL/can';


BoxHome.propTypes = {
    listProducts: PropTypes.array,
    handleSearch: PropTypes.func,
    pagination: PropTypes.object,
    handlePageChange: PropTypes.func,
}

BoxHome.defaultProps = {
    listProducts: [],
    handleSearch: null,
    pagination: null,
    handlePageChange: null,
}

function BoxHome(props) {
    const { listRoles, handleSearch, handleDelete, pagination, handlePageChange } = props
    const { _page, _limit, _totalRows } = pagination

    const totalPage = Math.ceil(_totalRows / _limit)
    const [searchTerm, setSearchTerm] = useState('')
    const [activePage, setActivePage] = useState(_page)
    const typingTimeOutRef = useRef(null)

    function onPageChange(pageNumber) {
        if (!handlePageChange) return
        setActivePage(pageNumber)
        handlePageChange(pageNumber)
    }
    function handleSearchInput(e) {
        setSearchTerm(e.target.value)
        const searchValue = e.target.value
        if (!handleSearch) return
        if (typingTimeOutRef.current) clearTimeout(typingTimeOutRef.current)
        typingTimeOutRef.current = setTimeout(() => {
            handleSearch({
                searchTerm: searchValue
            })
        }, 300)
    }

    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h4>All roles | TotalPages: {totalPage} | You are now at: {_page}</h4>


                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
                <Link
                    to="/"
                >
                    <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                </Link>
            </div>
            <div className="card-body">
                <div style={{ display: "flex", marginBottom: "4px" }}>
                    <Can I="create" a="Role">
                        <Link
                            to="/role/add"
                            style={{ flex: "0.15", marginRight: "4px" }}
                        >
                            <Button
                                color="primary"
                                style={{ width: "100%" }}
                            >
                                <i className="fas fa-plus-square"></i>
                            </Button>
                        </Link>
                    </Can>

                    <div className="search-container" >
                        <form action="#">
                            <input
                                type="text"
                                placeholder="search.."
                                name="searchData"
                                value={searchTerm}
                                onChange={(e) => handleSearchInput(e)}
                            />
                        </form>
                    </div>
                </div>
                <div>
                    <Table hover>
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <Can I="update" a="Role">
                                    <th>Action</th>
                                </Can>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                listRoles.map((item) => (
                                    <tr key={item.id}>
                                        <td>{item.id}</td>
                                        <td>{item.name}</td>
                                        <td>
                                            <Can I="update" a="Role">
                                                <Link
                                                    to={`/role/update/${item.name}`}
                                                >
                                                    <Button
                                                        color="warning"
                                                    >
                                                        <i className="fas fa-edit"></i>
                                                    </Button>
                                                </Link>
                                                <Button
                                                    onClick={() => handleDelete(item.name)}
                                                    color="danger"
                                                >
                                                    <i className="fas fa-trash"></i>
                                                </Button>
                                            </Can>

                                        </td>

                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                    <div className="pagination">
                        <Pagination
                            itemClass="page-item"
                            linkClass="page-link"
                            activePage={activePage}
                            itemsCountPerPage={_limit}
                            totalItemsCount={_totalRows}
                            pageRangeDisplayed={5}
                            onChange={onPageChange}
                        />
                    </div>
                </div>
            </div>
        </>
    );
}

export default BoxHome;