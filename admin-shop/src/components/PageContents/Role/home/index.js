import React, { useState, useEffect } from 'react';
import BoxHome from './home';
import Loading from 'components/Commons/Loading';
import userApi from 'api/userApi';

//handle data
function HomeRole() {
    const [listRoles, setListRoles] = useState([])
    const [isFetchComplete, setIsFetchComplete] = useState(false)

    const [filter, setFilter] = useState({
        _limit: 10,
        _page: 1,
        nameLike: 'null',
    })

    const [pagination, setPagination] = useState({
        _page: 1,
        _limit: 10,
        _totalRows: 1,
    })

    useEffect(() => {

        async function getDataFromApi() {
            try {
                const response = await userApi.getAllRoles(filter);
                if (response.code === 1) {
                    setListRoles(response.data.listRoles)
                    setPagination(response.data.pagination)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, filter]);
    function handleSearch(newFilter) {
        if (!newFilter) return
        setFilter({
            ...filter,
            _page: 1,
            nameLike: newFilter.searchTerm ? newFilter.searchTerm : 'null',
        })
    }
    function handlePageChange(newPage) {
        if (!newPage) return
        setFilter({
            ...filter,
            _page: newPage,
        })
    }

    async function deleteRole(roleName) {
        if (!roleName) return
        try {
            const response = await userApi.deleteRole({ name: roleName });
            if (response.code === 1) {
                setIsFetchComplete(false);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    function handleDelete(roleName) {
        if (window.confirm("delete ?" + roleName)) {
            deleteRole(roleName);
        } else {
            console.log("no delete")
        }
    }

    return (
        isFetchComplete ?
            <BoxHome
                listRoles={listRoles}
                pagination={pagination}
                handleDelete={handleDelete}
                handleSearch={handleSearch}
                handlePageChange={handlePageChange}
            />
            :
            <Loading />
    );
}

export default HomeRole;