import React, { useState, useEffect } from 'react';
import BoxUpdate from './update';
import { useParams } from 'react-router-dom';
import Loading from 'components/Commons/Loading';
import userApi from 'api/userApi';

function UpdateRole() {
    let { role } = useParams();
    const [isFetchComplete, setIsFetchComplete] = useState(false);
    const [listRolePermissions, setListRolePermissions] = useState([]);

    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await userApi.getAllPermissionsOfRole(role);
                if (response.code === 1) {
                    setListRolePermissions(response.data)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, role]);

    async function handleUpdatePermissionOfRole(permissionOfRole) {
        try {
            const response = await userApi.updatePermissionsOfRole(permissionOfRole);
            if (response.code === 1) {
                alert(response.message)
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    return (
        isFetchComplete
            ? <BoxUpdate
                role={role}
                listRolePermissions={listRolePermissions}
                handleUpdatePermissionOfRole={handleUpdatePermissionOfRole}
            />
            : <Loading />
    );
}

export default UpdateRole;