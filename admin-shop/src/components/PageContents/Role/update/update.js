import { useState } from "react";
import { useForm } from "react-hook-form";
import { Link } from 'react-router-dom';
import { Table, Button } from 'reactstrap';

function BoxUpdate(props) {
    const { role, listRolePermissions, handleUpdatePermissionOfRole } = props
    const { handleSubmit } = useForm();
    const listFixedPermisions = ["create", "read", "update", "delete"];
    const listFixedObjects = ["Product", "Brand", "Category", "Discount", "Distributor", "ImportProduct", "OutportProduct", "User", "Role", "Keyword", "Store"];

    const [tmpListPermissions, setTmpListPermissions] = useState([]);
    const [object, setObject] = useState('');
    const [reload, setReload] = useState(false);
    const [newListPermissions, setNewListPermissions] = useState(listRolePermissions && listRolePermissions.length > 0
        ? listRolePermissions
        : []
    );

    const onSubmit = data => {
        handleUpdatePermissionOfRole({
            Role: {
                name: role,
            },
            listRolePermissions: newListPermissions
        })
    }

    function addPermission() {
        tmpListPermissions.forEach(item => {
            if (newListPermissions.filter(tpmItem => tpmItem.object === object && tpmItem.permission === item).length === 0) {
                newListPermissions.push({
                    object: object,
                    permission: item,
                    role: role,
                    createdBy: ''
                })
            }
        })
        setReload(!reload)
    }

    function handleDeleteRolePermission(item) {
        if (window.confirm("delete ?" + item.permission + " " + item.object)) {
            setNewListPermissions(newListPermissions.filter(tmpItem => `${tmpItem.object}${tmpItem.permission}` !== `${item.object}${item.permission}`))
        }
    }
    // handleListPermisionsChange
    function handleListPermisionsChange(e, permision) {
        if (!e.target.checked && tmpListPermissions.includes(permision)) {
            setTmpListPermissions(tmpListPermissions.filter(item => item !== permision))
        } else if (e.target.checked && !tmpListPermissions.includes(permision)) {
            tmpListPermissions.push(permision)
        }
    }

    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h6 class="mb-2 text-primary">Update role</h6>
                  
                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
            <Link
                        to="/role/home"
                    >
                        <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                    </Link>
                    </div>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="card-body">
                    <div class="row">

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="roleName">Role name</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="roleName"
                                    value={role}
                                    disabled
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div style={{ height: "50px", background: "#f4f6f9" }}>

                </div>
                <div className="card-body">
                    <div class="row">
                        <div
                            class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                            style={{ margin: "10px 0" }}
                        >
                            <h6>Permision of role</h6>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">Objects</label>
                                <select
                                    class="form-control"
                                    id="object"
                                    onChange={(e) => setObject(e.target.value)}
                                    disabled={role !== '' ? false : true}
                                >
                                    <option value="-1">pick a object </option>
                                    {
                                        listFixedObjects.map((object, key) => (
                                            <option key={key} value={object}>{object}</option>
                                        ))
                                    }
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <label for="material" style={{ display: "block" }}>Sizes</label>
                            {
                                listFixedPermisions.map((permisions, key) => (
                                    <div key={key} class="form-check form-check-inline">
                                        <input
                                            class="form-check-input"
                                            type="checkbox"
                                            id="inlineCheckbox1"
                                            value={permisions}
                                            onChange={(e) => handleListPermisionsChange(e, permisions)}
                                            disabled={role !== '' ? false : true}
                                        />
                                        <label class="form-check-label" for="inlineCheckbox1">{permisions}</label>
                                    </div>
                                ))
                            }
                        </div>

                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="form-group">
                                <p
                                    className="btn btn-primary"
                                    onClick={() => addPermission()}
                                >
                                    Add permission
                                </p>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="form-group">
                                <p>List permission</p>
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>Action</th>
                                            <th>Object</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            newListPermissions.length > 0 && newListPermissions.map((item, key) => (
                                                <tr key={key}>
                                                    <td>{item.permission}</td>
                                                    <td>{item.object}</td>
                                                    <td>
                                                        <Button
                                                            onClick={() => handleDeleteRolePermission(item)}
                                                            color="danger"
                                                        >
                                                            <i className="fas fa-trash"></i>
                                                        </Button>
                                                    </td>
                                                </tr>
                                            ))
                                        }
                                    </tbody>
                                </Table>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <button className="btn btn-primary" type="submit">Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </>

    );
}

export default BoxUpdate;