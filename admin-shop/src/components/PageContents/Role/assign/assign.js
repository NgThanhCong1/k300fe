import { useState } from "react";
import { Link } from 'react-router-dom';

function BoxAssign(props) {
    const { listRoles, listRolesOfUser, handleAssignListRoles } = props
    const [listAssignRoles, setListAssignRoles] = useState(listRolesOfUser)

    function handleListRoleChange(name) {
        if (listAssignRoles.includes(name)) {
            setListAssignRoles(listAssignRoles.filter(role => role !== name))
        } else {
            listAssignRoles.push(name)
        }
    }
    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h6 class="mb-2 text-primary">Update role</h6>
                   
                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
            <Link
                        to="/user/home"
                    >
                        <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                    </Link>
                    </div>
            <div className="card-body">
                <form>
                    <div class="row">

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <label for="role" style={{ display: "block" }}>Roles</label>
                            {
                                listRoles.map((role) => (
                                    <div key={role.id} class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1"
                                            value={role.name}
                                            defaultChecked={listRolesOfUser.includes(role.name) ? 'checked' : null}
                                            onChange={() => handleListRoleChange(role.name)}
                                        />
                                        <label class="form-check-label" for="inlineCheckbox1">{role.name}</label>
                                    </div>
                                ))
                            }
                        </div>

                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <button className="btn btn-primary"
                                    onClick={(e) => { e.preventDefault(); handleAssignListRoles(listAssignRoles) }}
                                >Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>

    );
}

export default BoxAssign;