import BoxAssign from './assign';
import { useParams } from 'react-router';
import { useState, useEffect } from 'react';
import Loading from 'components/Commons/Loading';
import userApi from 'api/userApi';

function AssignRole() {

    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [listRoles, setListRoles] = useState(null)
    const [listRolesOfUser, setListRolesOfUser] = useState(null)
    let { userName } = useParams()

    useEffect(() => {

        async function getDataFromApi() {
            try {
                const response = await userApi.getDataForAssignRole(userName);
                if (response.code === 1) {
                    setListRoles(response.data.listRoles)
                    setListRolesOfUser(response.data.listRolesOfUser)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, userName]);

    async function handleAssignListRoles(listAssignRoles) {
        if (!listAssignRoles) return;
        try {
            const response = await userApi.removeAllRolesOfUser({
                ListRoleNames: listAssignRoles,
                UserName: userName
            });
            if (response.code === 1) {
                alert(response.message)
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }
    return (
        isFetchComplete ?
            <BoxAssign
                listRoles={listRoles}
                listRolesOfUser={listRolesOfUser}
                handleAssignListRoles={handleAssignListRoles}
            />
            :
            <Loading />
    )
}

export default AssignRole;