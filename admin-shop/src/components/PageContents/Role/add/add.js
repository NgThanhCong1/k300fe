import { useState } from "react";
import { useForm } from "react-hook-form";
import { Link } from 'react-router-dom';
import { Table, Button } from 'reactstrap';

function BoxAdd(props) {

    const { handleAddRole } = props;
    const { register, handleSubmit, formState: { errors } } = useForm();
    const listPermisions = [
        "create",
        "read",
        "update",
        "delete"
    ];
    const listObjects = [
        "Product",
        "Brand",
        "Category",
        "Discount",
        "Distributor",
        "ImportProduct",
        "OutportProduct",
        "User",
        "Role",
        "Keyword",
        "Store"
    ];
    const [roleName, setRoleName] = useState('');
    const [object, setObject] = useState(null);
    const [tmpListPermisions, setTmpListPermisions] = useState([]);
    const [listObjectPermission, setListObjectPermission] = useState([]);
    const [reload, setReload] = useState(false);

    const onSubmit = data => {
        handleAddRole({
            Role: {
                name: roleName,
            },
            listRolePermissions: listObjectPermission
        })
    }

    function addPermission() {
        tmpListPermisions.forEach(item => {
            if (listObjectPermission.filter(tpmItem => tpmItem.object === object && tpmItem.permission === item).length === 0) {
                listObjectPermission.push({
                    object: object,
                    permission: item,
                    role: roleName
                })
            }
        })
        setReload(!reload)
    }

    function handleDeleteRolePermission(item) {
        if (window.confirm("delete ?" + item.permission + " " + item.object)) {
            setListObjectPermission(listObjectPermission.filter(tmpItem => `${tmpItem.object}${tmpItem.permission}` !== `${item.object}${item.permission}`))
        }
    }

    function handleListPermisionsChange(e, permision) {
        if (!e.target.checked && tmpListPermisions.includes(permision)) {
            setTmpListPermisions(tmpListPermisions.filter(item => item !== permision))
        } else if (e.target.checked && !tmpListPermisions.includes(permision)) {
            tmpListPermisions.push(permision)
        }
    }

    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h4 class="mb-2 text-primary">Add role</h4>
                    
                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
            <Link
                        to="/role/home"
                    >
                        <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                    </Link>
                    </div>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="card-body">
                    <div class="row">

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="roleName">Role name</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="roleName"
                                    placeholder="Enter product name"
                                    {...register('roleName1', { required: true })}
                                    onChange={e => setRoleName(e.target.value)}
                                />
                                {errors.roleName1 && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                    </div>
                </div>
                <div style={{ height: "50px", background: "#f4f6f9" }}>

                </div>
                <div className="card-body">
                    <div class="row">
                        <div
                            class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                            style={{ margin: "10px 0" }}
                        >
                            <h6>Permision of role</h6>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">Objects</label>
                                <select
                                    class="form-control"
                                    id="object"
                                    onChange={(e) => setObject(e.target.value)}
                                    disabled={roleName === ''}
                                >
                                    <option value="-1">pick a object </option>
                                    {
                                        listObjects.map((object, key) => (
                                            <option key={key} value={object}>{object}</option>
                                        ))
                                    }
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <label for="material" style={{ display: "block" }}>Roles</label>
                            {
                                listPermisions.map((permisions, key) => (
                                    <div key={key} class="form-check form-check-inline">
                                        <input
                                            class="form-check-input"
                                            type="checkbox"
                                            id={permisions}
                                            value={permisions}
                                            onChange={(e) => handleListPermisionsChange(e, permisions)}
                                            disabled={roleName === ''}
                                        />
                                        <label class="form-check-label" for={permisions}>{permisions}</label>
                                    </div>
                                ))
                            }
                        </div>

                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="form-group">
                                <p
                                    className="btn btn-primary"
                                    onClick={() => addPermission()}
                                >
                                    Add permission
                                </p>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="form-group">
                                <p>List permission</p>
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>Action</th>
                                            <th>Object</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            listObjectPermission.map((item, key) => (
                                                <tr key={key}>
                                                    <td>{item.permission}</td>
                                                    <td>{item.object}</td>
                                                    <td>
                                                        <Button
                                                            onClick={() => handleDeleteRolePermission(item)}
                                                            color="danger"
                                                        >
                                                            <i className="fas fa-trash"></i>
                                                        </Button>
                                                    </td>
                                                </tr>
                                            ))
                                        }
                                    </tbody>
                                </Table>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <button className="btn btn-primary" type="submit">Add role</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </>

    );
}

export default BoxAdd;