import React from 'react';
import BoxAdd from './add';
import userApi from 'api/userApi';

function AddRole() {

    async function handleAddRole(addRole) {
        if (!addRole) return;
        try {
            const response = await userApi.addRole(addRole);
            if (response.code === 1) {
                alert(response.message)
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    return (
        <BoxAdd
            handleAddRole={handleAddRole}
        />
    );
}

export default AddRole;