import BoxDetail from './detail';
import { useParams } from 'react-router';
import { useState, useEffect } from 'react';
import Loading from 'components/Commons/Loading';
import productApi from 'api/productApi';

function DetailProduct() {
    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [product, setProduct] = useState(null)
    const [listCategoriesOfProduct, setListCategoriesOfProduct] = useState([])
    const [listCategories, setListCategories] = useState(null)
    const [listSubProducts, setListSubProducts] = useState(null)
    const [listBrands, setListBrands] = useState(null)
    let { id } = useParams();

    useEffect(() => {

        async function getDataFromApi() {
            try {
                const response = await productApi.getDataForUpdateProduct(id);
                if (response.code === 1) {
                    setProduct(response.data.product)
                    setListCategoriesOfProduct(response.data.listCategoriesOfProduct)
                    setListCategories(response.data.listCategories)
                    setListBrands(response.data.listBrands)
                    setListSubProducts(response.data.listSubProducts)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, id]);

    return (
        isFetchComplete ?
            <BoxDetail
                product={product}
                listCategoriesOfProduct={listCategoriesOfProduct}
                listCategories={listCategories}
                listBrands={listBrands}
                listSubProducts={listSubProducts}
            />
            :
            <Loading />
    )
}

export default DetailProduct;