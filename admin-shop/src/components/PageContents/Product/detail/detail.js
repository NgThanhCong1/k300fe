import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Can } from '../../Auth/CASL/can';

function BoxDetail(props) {
    const { product, listSubProducts, listCategoriesOfProduct, listCategories } = props

    return (
        <>
            <div className="card-header">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h4 class="mb-2 text-primary">View detail product</h4>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6"
                    style={{ textAlign: "right" }}
                >

                    <Can I="modify" a="product">
                        <Link
                            style={{ display: "block", margin: "10px 0" }}
                            to={`/product/update/${product.id}`}
                        >
                            <button className="btn btn-danger">Update <i class="fas fa-edit"></i></button>
                        </Link>
                    </Can>
                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
                <Link
                    style={{ display: "block" }}
                    to="/product/home"
                >
                    <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                </Link>
            </div>
            <div className="card-body">
                <form>
                    <div class="row">

                        <div
                            class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                            style={{ margin: "10px 0" }}
                        >
                            <h6>Product infor</h6>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="productName">Product name</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="productName"
                                    placeholder="Enter product name"
                                    readOnly
                                    value={product.name}
                                />
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="price">Price</label>
                                <input
                                    type="price"
                                    class="form-control"
                                    id="price"
                                    placeholder="Enter price"
                                    readOnly
                                    value={product.price}
                                />
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">Brand</label>
                                <input
                                    type="brandName"
                                    class="form-control"
                                    id="brandName"
                                    placeholder="Enter brandName"
                                    readOnly
                                    value={product.brandName}
                                />
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <label for="material" style={{ display: "block" }}>Categories</label>
                            {
                                listCategories.map((category) => (
                                    <div key={category.id} class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1"
                                            value={category.id}
                                            defaultChecked={listCategoriesOfProduct.includes(category.id) ? 'checked' : null}
                                            disabled
                                        />
                                        <label class="form-check-label" for="inlineCheckbox1">{category.name}</label>
                                    </div>
                                ))
                            }
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">Description</label>
                                <CKEditor
                                    editor={ClassicEditor}
                                    data={product.description}
                                    disabled
                                />
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">Material</label>
                                <CKEditor
                                    editor={ClassicEditor}
                                    data={product.material}
                                    disabled
                                />
                            </div>
                        </div>

                        <div
                            class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                            style={{ margin: "10px 0" }}
                        >
                            <h6>Configs of product</h6>
                        </div>

                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="form-group">
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>Product code</th>
                                            <th>Size</th>
                                            <th>Color</th>
                                            <th>Quantity</th>
                                            <th>Image count</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            listSubProducts.map((item) => (
                                                <tr key={item.productCode}>
                                                    <td>
                                                        {item.productCode}
                                                        <i
                                                            style={{
                                                                marginLeft: "8px",
                                                                cursor: "pointer",
                                                                transform: "translateY(-20%)",
                                                                fontSize: "12px"
                                                            }}
                                                            className="fa fa-pen"
                                                            onClick={() => navigator.clipboard.writeText(item.productCode)}
                                                        >

                                                        </i>
                                                    </td>
                                                    <td>{item.size}</td>
                                                    <td>{item.color}</td>
                                                    <td>{item.quantity}</td>
                                                    <td>{item.listImagesLink.length}</td>
                                                </tr>
                                            ))
                                        }
                                    </tbody>
                                </Table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>

    );
}

export default BoxDetail;