import { useState } from "react";
import { useForm } from "react-hook-form";
import { CKEditor } from '@ckeditor/ckeditor5-react';

import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Button, Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import CustomAlert from "components/Commons/Alert";

BoxAdd.propTypes = {
    listCategories: PropTypes.array,
    listBrands: PropTypes.array,
    handleAddProduct: PropTypes.func,
}

BoxAdd.defaultProps = {
    listCategories: [],
    listBrands: [],
    handleAddProduct: null,
}

function BoxAdd(props) {
    const {
        alertInfor,
        listCategories,
        listBrands,
        handleAddProduct
    } = props;

    const [description, setDescription] = useState('')
    const [reload, setReload] = useState(false);
    const [material, setMaterial] = useState('')

    const [listCategoriesOfProduct, setlistCategoriesOfProduct] = useState([])
    const [tmplistCategories, setTmplistCategories] = useState([])
    const [tmplistSizes, setTmplistSizes] = useState([])
    const [productCode, setProductCode] = useState('')
    const [color, setColor] = useState('')

    const [listFiles, setListFiles] = useState([])
    const [listFilesObj, setListFilesObj] = useState([])

    const { register, handleSubmit, formState: { errors }, reset } = useForm({
        mode: 'onBlur'
    });

    const listColors = ["BLACK", "WHITE", "RED", "YELLOW", "PURPLE", "BLUE", "GREEN", "PINK", "GREY", "BROWN", "ORANGE"]
    const listSizes = ["S", "M", "L", "XL", "XXL", "XXXL", "OVER"]
    const fileArray = []
    const fileObj = []
    const [listProductConfigs, setListProductConfigs] = useState([])

    const onSubmit = async data => {
        if (listProductConfigs.length === 0) {
            alert("add product by color please");
            return;
        }
        setlistCategoriesOfProduct([])
        tmplistCategories.forEach(element => {
            listCategoriesOfProduct.push({
                productId: 0,
                categoryId: parseInt(element)
            })
        });
        await handleAddProduct({
            productNew: {
                name: data.productName,
                price: data.price,
                brandId: parseInt(data.brandId),
                description: description,
                material: material,
            },
            listProductCategoryNews: listCategoriesOfProduct,
            listProductConfigs: listProductConfigs
        })
        setListProductConfigs([])
        reset();
    }

    function handleUploadFiles(e) {
        fileObj.push(e.target.files)
        for (let i = 0; i < fileObj[0].length; i++) {
            fileArray.push(URL.createObjectURL(fileObj[0][i]))
        }
        setListFiles(fileArray)
        setListFilesObj(e.target.files)
    }

    function handleListCateChange(e, id) {
        if (!e.target.checked && tmplistCategories.includes(id)) {
            setTmplistCategories(tmplistCategories.filter(category => category !== id))
        } else if (e.target.checked && !tmplistCategories.includes(id)) {
            tmplistCategories.push(id)
        }
    }

    function handleListSizeChange(e, size) {
        if (!e.target.checked && tmplistSizes.includes(size)) {
            setTmplistSizes(tmplistSizes.filter(item => item !== size))
        } else if (e.target.checked && !tmplistSizes.includes(size)) {
            tmplistSizes.push(size)
        }
    }

    function addProductConifg() {
        tmplistSizes.forEach(item => {
            if (listProductConfigs.filter(tpmItem => tpmItem.productCode === `${productCode}-${color}-${item}`).length === 0) {
                listProductConfigs.push({
                    productCode: `${productCode}-${color}-${item}`,
                    color: color,
                    size: item,
                    listFiles: listFilesObj
                })
            }
        })
        setReload(!reload)
    }

    function handleDeleteProductConfig(productCode) {
        if (window.confirm("delete ?" + productCode)) {
            setListProductConfigs(listProductConfigs.filter(tpmItem => tpmItem.productCode !== productCode))
        }
    }

    return (
        <>
            <div className="card-header">
                <div
                    class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h4 class="mb-2 text-primary">Add product</h4>

                </div>
                <div
                    style={{ display: "flex", justifyContent: "space-between" }}
                >

                </div>

            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
                <Link
                    to="/product/home"
                >
                    <button className="btn btn-primary">
                        Back <i class="fas fa-arrow-left"></i>
                    </button>
                </Link>
            </div>

            <div
                style={{ padding: "10px 25px" }}
            >
                <CustomAlert
                    {...alertInfor}
                />
            </div>

            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="card-body">
                    <div class="row">
                        <div
                            class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                            style={{ margin: "10px 0" }}
                        >
                            <h6>Product infor</h6>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="productName">Product name</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="productName"
                                    placeholder="Enter product name"
                                    {...register('productName', { required: true })}

                                />
                                {errors.productName && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="price">Price</label>
                                <input
                                    type="price"
                                    class="form-control"
                                    id="price"
                                    placeholder="Enter price"
                                    {...register('price', { required: true, valueAsNumber: true })}
                                />
                                {errors.price?.type === "required" && <span style={{ color: "red" }}>This field is required</span>}
                                {errors.price?.type === "valueAsNumber" && <span style={{ color: "red" }}>This field is number required</span>}
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">Brand</label>
                                <select
                                    class="form-control" id="brand"
                                    {...register('brandId')}
                                >
                                    <option value="-1">pick a brand </option>
                                    {
                                        listBrands.map((brand) => (
                                            <option key={brand.id} value={brand.id}>{brand.name}</option>
                                        ))
                                    }
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <label for="material" style={{ display: "block" }}>Categories</label>
                            {
                                listCategories.map((category) => (
                                    <div key={category.id} class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id={'cate' + category.id}
                                            value={category.id}
                                            onChange={(e) => handleListCateChange(e, category.id)}
                                        />
                                        <label class="form-check-label" for={'cate' + category.id}>{category.name}</label>
                                    </div>
                                ))
                            }
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">Description</label>
                                <CKEditor
                                    editor={ClassicEditor}
                                    onChange={(event, editor) => {
                                        setDescription(editor.getData());
                                    }}
                                />
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">Material</label>
                                <CKEditor
                                    editor={ClassicEditor}
                                    onChange={(event, editor) => {
                                        setMaterial(editor.getData());
                                    }}
                                />
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="productCode">Product code</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="productCode"
                                    placeholder="Enter product code"
                                    {...register('productCode', { required: true })}
                                    onChange={(e) => setProductCode(e.target.value)}
                                />
                                {errors.productCode && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                    </div>
                </div>
                <div style={{ height: "50px", background: "#f4f6f9" }}>

                </div>
                <div className="card-body">
                    <div class="row">
                        <div
                            class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                            style={{ margin: "10px 0" }}
                        >
                            <h6>Product by color</h6>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">Color</label>
                                <select
                                    class="form-control"
                                    id="color"
                                    onChange={(e) => setColor(e.target.value)}
                                    disabled={productCode !== '' ? false : true}
                                >
                                    <option value="-1">pick a color </option>
                                    {
                                        listColors.map((color, key) => (
                                            <option key={key} value={color}>{color}</option>
                                        ))
                                    }
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <label for="material" style={{ display: "block" }}>Sizes</label>
                            {
                                listSizes.map((size, key) => (
                                    <div key={key} class="form-check form-check-inline">
                                        <input
                                            class="form-check-input"
                                            type="checkbox"
                                            id={size}
                                            value={size}
                                            onChange={(e) => handleListSizeChange(e, size)}
                                            disabled={productCode !== '' ? false : true}

                                        />
                                        <label class="form-check-label" for={size}>{size}</label>
                                    </div>
                                ))
                            }
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label htmlFor="listFiles">Pictures of product:</label>
                                <input
                                    type="file"
                                    className="form-control"
                                    id="listFiles"
                                    multiple
                                    onChange={(e) => handleUploadFiles(e)}
                                    disabled={productCode !== '' ? false : true}
                                />
                                <div>
                                    {
                                        (listFiles).map(url => (
                                            <img style={{ maxWidth: "100px" }} src={url} alt="..." />
                                        ))
                                    }
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="form-group">
                                <p
                                    className="btn btn-primary"
                                    onClick={() => addProductConifg()}
                                    style={{ cursor: "pointer" }}
                                >
                                    Add config
                                </p>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="form-group">
                                <p>List product config</p>
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>Product code</th>
                                            <th>Size</th>
                                            <th>Color</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            listProductConfigs.map((item) => (
                                                <tr key={item.productCode}>
                                                    <td>{item.productCode}</td>
                                                    <td>{item.size}</td>
                                                    <td>{item.color}</td>
                                                    <td>
                                                        <Button
                                                            onClick={() => handleDeleteProductConfig(item.productCode)}
                                                            color="danger"
                                                        >
                                                            <i className="fas fa-trash"></i>
                                                        </Button>
                                                    </td>
                                                </tr>
                                            ))
                                        }
                                    </tbody>
                                </Table>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <button className="btn btn-primary" type="submit">Add product</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </>
    );
}

export default BoxAdd;