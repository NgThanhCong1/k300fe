import React, { useState, useEffect } from 'react';
import BoxAdd from './add';
import Loading from 'components/Commons/Loading';
import productApi from 'api/productApi';

function AddProduct() {
    const [alertInfor, setAlertInfor] = useState({
        type: 0,
        message: ''
    })

    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [listCategories, setListCategories] = useState([])
    const [listBrands, setListBrands] = useState([])

    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await productApi.getDataForAddProduct();
                if (response.code === 1) {
                    setListBrands(response.data.listBrands)
                    setListCategories(response.data.listCategories)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete]);

    async function handleAddProduct(addProduct) {
        if (!addProduct) return;
        try {
            const response = await productApi.addProduct(addProduct);
            if (response.code === 1) {
                addProduct.listProductConfigs.forEach(item => {
                    handleAddSubProduct({ ...item, productId: response.data })
                })
                setAlertInfor({
                    type: 1,
                    message: response.message
                })
            } else {
                setAlertInfor({
                    type: -1,
                    message: response.message
                })
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
        window.scrollTo(0, 0);
    }

    async function handleAddSubProduct(subProduct) {
        if (!subProduct) return;
        var formData = new FormData();
        for (var index = 0; index < subProduct.listFiles.length; ++index) {
            formData.append('ListImageFiles', subProduct.listFiles[index])
        }
        formData.append("ProductCode", subProduct.productCode)
        formData.append("ProductId", subProduct.productId)
        formData.append("Color", subProduct.color)
        formData.append("Size", subProduct.size)

        try {
            const response = await productApi.addSubProduct(formData);
            if (response.code === 1) {

            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    return (
        isFetchComplete ?
            <BoxAdd
                listCategories={listCategories}
                listBrands={listBrands}
                handleAddProduct={handleAddProduct}
                alertInfor={alertInfor}
            />
            :
            <Loading />
    );
}

export default AddProduct;