import BoxUpdate from './update';
import { useParams } from 'react-router';
import { useState, useEffect } from 'react';
import Loading from 'components/Commons/Loading';
import productApi from 'api/productApi';

function UpdateProduct() {
    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [product, setProduct] = useState(null)
    const [listCategoriesOfProduct, setListCategoriesOfProduct] = useState([])
    const [listCategories, setListCategories] = useState(null)
    const [listSubProducts, setListSubProducts] = useState(null)
    const [listBrands, setListBrands] = useState(null)
    let { id } = useParams();

    const [alertInfor, setAlertInfor] = useState({
        type: 0,
        message: ''
    })

    useEffect(() => {

        async function getDataFromApi() {
            try {
                const response = await productApi.getDataForUpdateProduct(id);
                if (response.code === 1) {
                    setProduct(response.data.product)
                    setListCategoriesOfProduct(response.data.listCategoriesOfProduct)
                    setListCategories(response.data.listCategories)
                    setListBrands(response.data.listBrands)
                    setListSubProducts(response.data.listSubProducts)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, id]);

    async function handleUpdateProduct(updateProduct) {
        if (!updateProduct) return;
        try {
            const response = await productApi.updateProduct({
                ...updateProduct,
                listSubProducts: updateProduct.listSubProductForDeletes
            });
            if (response.code === 1) {
                updateProduct.listSubProductForUpdates.forEach(item => {
                    if (listSubProducts.filter(mainItem => mainItem.productCode === item.productCode).length === 0) {
                        handleAddSubProduct(item);
                    }
                })
                setAlertInfor({
                    type: 1,
                    message: response.message
                })
            } else {
                setAlertInfor({
                    type: -1,
                    message: response.message
                })
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
        window.scrollTo(0, 0);
    }

    async function handleAddSubProduct(subProduct) {
        if (!subProduct) return;
        var formData = new FormData();
        for (var index = 0; index < subProduct.listFiles.length; ++index) {
            formData.append('ListImageFiles', subProduct.listFiles[index])
        }
        formData.append("ProductCode", subProduct.productCode)
        formData.append("ProductId", id)
        formData.append("Color", subProduct.color)
        formData.append("Size", subProduct.size)

        try {
            const response = await productApi.addSubProduct(formData);
            if (response.code === 1) {

            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    return (
        isFetchComplete ?
            <BoxUpdate
                product={product}
                handleUpdateProduct={handleUpdateProduct}
                listCategoriesOfProduct={listCategoriesOfProduct}
                listCategories={listCategories}
                listBrands={listBrands}
                listSubProducts={listSubProducts}
                alertInfor={alertInfor}
            />
            :
            <Loading />
    )
}

export default UpdateProduct;