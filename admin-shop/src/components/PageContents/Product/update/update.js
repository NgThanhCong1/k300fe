import { useState } from "react";
import { useForm } from "react-hook-form";
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Button, Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import CustomAlert from "components/Commons/Alert";

function BoxUpdate(props) {
    const { alertInfor, product, listSubProducts, handleUpdateProduct, listCategoriesOfProduct, listCategories, listBrands } = props
    const [description, setDescription] = useState(product.description)
    const [material, setMaterial] = useState(product.material)

    const [newlistCategoriesOfProduct, setnewListCategoriesOfProduct] = useState([])
    const [newlistSubProduct, setNewListSubProduct] = useState(listSubProducts)
    const [newlistSubProductForDelete,] = useState([])
    const [tmplistCategories, setTmplistCategories] = useState(listCategoriesOfProduct)
    const [color, setColor] = useState('')

    const listColors = ["BLACK", "WHITE", "RED", "YELLOW", "PURPLE", "BLUE", "GREEN", "PINK", "GREY", "BROWN", "ORANGE"]
    const listSizes = ["S", "M", "L", "XL", "XXL", "XXXL", "oversize"]
    const [tmplistSizes, setTmplistSizes] = useState([])
    const [listFiles, setListFiles] = useState([])
    const fileArray = []
    const [listFilesObj, setListFilesObj] = useState([])

    const [productCode,] = useState(listSubProducts[0].productCode.split("-")[0])
    const [reload, setReload] = useState(false);
    const fileObj = []
    const { register, handleSubmit, formState: { errors } } = useForm();

    const onSubmit = data => {

        setnewListCategoriesOfProduct([])
        tmplistCategories.forEach(element => {
            newlistCategoriesOfProduct.push({
                productId: 0,
                categoryId: parseInt(element)
            })
        });
        handleUpdateProduct({
            productNew: {
                id: product.id,
                name: data.productName,
                price: data.price,
                brandId: parseInt(data.brandId),
                description: description,
                material: material,
            },
            listProductCategoryNews: newlistCategoriesOfProduct,
            listSubProductForDeletes: newlistSubProductForDelete,
            listSubProductForUpdates: newlistSubProduct
        })
    }

    function addProductConifg() {
        tmplistSizes.forEach(item => {
            if (newlistSubProduct.filter(tpmItem => tpmItem.productCode === `${productCode}-${color}-${item}`).length === 0) {
                newlistSubProduct.push({
                    productCode: `${productCode}-${color}-${item}`,
                    color: color,
                    size: item,
                    listFiles: listFilesObj,
                    isNew: 1
                })
                setReload(!reload)
            }
        })
    }

    function handleUploadFiles(e) {
        fileObj.push(e.target.files)
        for (let i = 0; i < fileObj[0].length; i++) {
            fileArray.push(URL.createObjectURL(fileObj[0][i]))
        }
        setListFiles(fileArray)
        setListFilesObj(e.target.files)
    }

    function handleListSizeChange(size) {
        if (tmplistSizes.includes(size)) {
            setTmplistSizes(tmplistSizes.filter(item => item !== size))
        } else {
            tmplistSizes.push(size)
        }
    }

    function handleListCateChange(id) {
        if (tmplistCategories.includes(id)) {
            setTmplistCategories(tmplistCategories.filter(category => category !== id))
        } else {
            tmplistCategories.push(id)
        }
    }

    function handleDeleteSubProducts(productCode) {
        if (window.confirm("delete ?" + productCode)) {
            newlistSubProductForDelete.push({ productCode: productCode })
            setNewListSubProduct(newlistSubProduct.filter(item => item.productCode !== productCode))
        }
    }

    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h4 class="mb-2 text-primary">Update product</h4>

                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
                <Link
                    to="/product/home"
                >
                    <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                </Link>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
                <CustomAlert
                    {...alertInfor}
                />
            </div>
            <div className="card-body">


                <form onSubmit={handleSubmit(onSubmit)}>
                    <div class="row">

                        <div
                            class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                            style={{ margin: "10px 0" }}
                        >
                            <h6>Configs of product</h6>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="productName">Product name</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="productName"
                                    placeholder="Enter product name"
                                    {...register('productName', { required: true })}
                                    defaultValue={product.name}
                                />
                                {errors.productName && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="price">Price</label>
                                <input
                                    type="price"
                                    class="form-control"
                                    id="price"
                                    placeholder="Enter price"
                                    {...register('price', { required: true, valueAsNumber: true })}
                                    defaultValue={product.price}
                                />
                                {errors.price?.type === "required" && <span style={{ color: "red" }}>This field is required</span>}
                                {errors.price?.type === "valueAsNumber" && <span style={{ color: "red" }}>This field is number required</span>}
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">Brand</label>
                                <select
                                    class="form-control"
                                    id="brand"
                                    value={product.brandId}
                                    {...register('brandId')}
                                >
                                    {
                                        listBrands.map((brand) => (
                                            <option key={brand.id} value={brand.id}>{brand.name}</option>
                                        ))
                                    }
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <label for="material" style={{ display: "block" }}>Categories</label>
                            {
                                listCategories.map((category) => (
                                    <div key={category.id} class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1"
                                            value={category.id}
                                            defaultChecked={listCategoriesOfProduct.includes(category.id) ? 'checked' : null}
                                            onChange={() => handleListCateChange(category.id)}
                                        />
                                        <label class="form-check-label" for="inlineCheckbox1">{category.name}</label>
                                    </div>
                                ))
                            }
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">Description</label>
                                <CKEditor
                                    editor={ClassicEditor}
                                    onReady={editor => {
                                        editor.setData(description);
                                    }}
                                    onChange={(event, editor) => {
                                        setDescription(editor.getData());
                                    }}
                                />
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">Material</label>
                                <CKEditor
                                    editor={ClassicEditor}
                                    onReady={editor => {
                                        editor.setData(material);
                                    }}
                                    onChange={(event, editor) => {
                                        setMaterial(editor.getData());
                                    }}
                                />
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="productCode">Product code</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="productCode"
                                    placeholder="Enter product code"
                                    value={productCode}
                                    disabled
                                />
                            </div>
                        </div>
                        <div
                            class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                            style={{ margin: "10px 0" }}
                        >
                            <h6>Configs of product</h6>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">Color</label>
                                <select
                                    class="form-control"
                                    id="color"
                                    onChange={(e) => setColor(e.target.value)}
                                    disabled={productCode !== '' ? false : true}
                                >
                                    <option value="-1">pick a color </option>
                                    {
                                        listColors.map((color, key) => (
                                            <option key={key} value={color}>{color}</option>
                                        ))
                                    }
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <label for="material" style={{ display: "block" }}>Sizes</label>
                            {
                                listSizes.map((size, key) => (
                                    <div key={key} class="form-check form-check-inline">
                                        <input
                                            class="form-check-input"
                                            type="checkbox"
                                            id="inlineCheckbox1"
                                            value={size}
                                            onChange={() => handleListSizeChange(size)}
                                            disabled={productCode !== '' ? false : true}

                                        />
                                        <label class="form-check-label" for="inlineCheckbox1">{size}</label>
                                    </div>
                                ))
                            }
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label htmlFor="listFiles">Pictures of product:</label>
                                <input
                                    type="file"
                                    className="form-control"
                                    id="listFiles"
                                    multiple
                                    onChange={(e) => handleUploadFiles(e)}
                                    disabled={productCode !== '' ? false : true}
                                />
                                <div>
                                    {
                                        (listFiles).map(url => (
                                            <img style={{ maxWidth: "100px" }} src={url} alt="..." />
                                        ))
                                    }
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="form-group">
                                <p
                                    className="btn btn-primary"
                                    onClick={() => addProductConifg()}
                                >
                                    Add config
                                </p>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="form-group">
                                <p>List product config</p>
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>Product code</th>
                                            <th>Size</th>
                                            <th>Color</th>
                                            <th>Quantity</th>
                                            <th>Image count</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            newlistSubProduct.map((item) => (
                                                <tr key={item.productCode}>
                                                    <td>{item.productCode}</td>
                                                    <td>{item.size}</td>
                                                    <td>{item.color}</td>
                                                    <td>{!item.isNew ? item.quantity : 0}</td>
                                                    <td>{!item.isNew ? item.listImagesLink.length : "..."}</td>
                                                    <td>
                                                        {
                                                            !item.isNew
                                                            && <>
                                                                <Link to={`/sub-product/update/${item.productCode}`}>
                                                                    <Button
                                                                        color="warning"
                                                                    >
                                                                        <i className="fas fa-edit"></i>
                                                                    </Button>
                                                                </Link>
                                                                <Button
                                                                    onClick={() => handleDeleteSubProducts(item.productCode)}
                                                                    color="danger"
                                                                >
                                                                    <i className="fas fa-trash"></i>
                                                                </Button>
                                                            </>
                                                        }

                                                    </td>
                                                </tr>
                                            ))
                                        }
                                    </tbody>
                                </Table>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <button className="btn btn-primary" type="submit">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>
    );
}

export default BoxUpdate;