import React, { useState, useEffect } from 'react';
import BoxHome from './home';
import Loading from 'components/Commons/Loading';
import productApi from 'api/productApi';

function HomeProduct() {

    const [listProducts, setListProducts] = useState([])
    const [isFetchComplete, setIsFetchComplete] = useState(false)

    const [filter, setFilter] = useState({
        _limit: 10,
        _page: 1,
        nameLike: 'null',
    })

    const [pagination, setPagination] = useState({
        _page: 1,
        _limit: 10,
        _totalRows: 1,
    })

    useEffect(() => {

        async function getDataFromApi() {
            try {
                const response = await productApi.getAllProducts(filter);
                if (response.code === 1) {
                    setListProducts(response.data.listProducts)
                    setPagination(response.data.pagination)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
        setIsFetchComplete(true)

    }, [filter, isFetchComplete]);

    async function deleteProduct(id) {
        if (id < 1) return;
        try {
            const response = await productApi.deleteProduct(id);
            if (response.code === 1) {
                setIsFetchComplete(false);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    function handleSearch(newFilter) {
        if (!newFilter) return
        setFilter({
            ...filter,
            _page: 1,
            nameLike: newFilter.searchTerm ? newFilter.searchTerm : 'null',
        })
    }

    function handlePageChange(newPage) {
        if (!newPage) return
        setFilter({
            ...filter,
            _page: newPage,
        })
    }

    function handleDelete(id, name) {
        if (window.confirm("delete ?" + name)) {
            deleteProduct(id);
        } else {
            console.log("no delete")
        }
    }

    return (
        isFetchComplete ?
            <BoxHome
                listProducts={listProducts}
                pagination={pagination}
                handleDelete={handleDelete}
                handleSearch={handleSearch}
                handlePageChange={handlePageChange}
            />
            :
            <Loading />
    );
}

export default HomeProduct;