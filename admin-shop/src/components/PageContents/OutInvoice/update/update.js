import { useForm } from "react-hook-form";
import { Link } from 'react-router-dom';

function BoxUpdate(props) {
    const { outInvoice, handleUpdateOutInvoice } = props

    const { register, handleSubmit, formState: { errors } } = useForm();

    const onSubmit = data => {
        handleUpdateOutInvoice({
            ...outInvoice,
            totalMoney: data.totalMoney,
            shipPhone: data.shipPhone,
            shipName: data.shipName,
            shipAt: data.shipAt,
        })
    }

    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h4 class="mb-2 text-primary">Update outInvoiceId for {outInvoice.id}</h4>
                    
                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
            <Link
                        to={`/outInvoice/detail/${outInvoice.id}`}
                    >
                        <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                    </Link>
                    </div>
            <div className="card-body">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div class="row">

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="totalMoney">Total money</label>
                                <input type="text" class="form-control" id="totalMoney" placeholder="Enter Total money"
                                    {...register('totalMoney', { required: true })}
                                    defaultValue={outInvoice.totalMoney}
                                />
                                {errors.totalMoney && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="shipPhone">Ship phone</label>
                                <input type="text" class="form-control" id="shipPhone" placeholder="Enter Shipphone"
                                    {...register('shipPhone', { required: true })}
                                    defaultValue={outInvoice.shipPhone}
                                />
                                {errors.shipPhone && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="shipName">Ship name</label>
                                <input type="text" class="form-control" id="shipName" placeholder="Enter Ship name"
                                    {...register('shipName', { required: true })}
                                    defaultValue={outInvoice.shipName}
                                />
                                {errors.shipName && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="shipAt">Ship at</label>
                                <input type="text" class="form-control" id="shipAt" placeholder="Enter Ship at"
                                    {...register('shipAt', { required: true })}
                                    defaultValue={outInvoice.shipAt}
                                />
                                {errors.shipAt && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <button className="btn btn-primary" type="submit">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>

    );
}

export default BoxUpdate;