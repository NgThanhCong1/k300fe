import BoxUpdate from './update';
import { useParams } from 'react-router';
import { useState, useEffect } from 'react';
import Loading from 'components/Commons/Loading';
import outInvoiceApi from 'api/outInvoiceApi';

function UpdateOutInvoice() {

    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [outInvoice, setOutInvoice] = useState(null)
    let { id } = useParams()

    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await outInvoiceApi.getDataForUpadteOutInvoice(id);
                if (response.code === 1) {
                    setOutInvoice(response.data)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, id]);

    async function handleUpdateOutInvoice(updateOutInvoice) {
        if (!updateOutInvoice) return;
        try {
            const response = await outInvoiceApi.updateOutInvoiceData(updateOutInvoice);
            if (response.code === 1) {
                alert(response.message)

            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    return (
        isFetchComplete ?
            <BoxUpdate
                outInvoice={outInvoice}
                handleUpdateOutInvoice={handleUpdateOutInvoice}
            />
            :
            <Loading />
    )
}

export default UpdateOutInvoice;