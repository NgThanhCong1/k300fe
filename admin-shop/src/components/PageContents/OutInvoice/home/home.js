import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import Pagination from "react-js-pagination";
import { Button, ButtonDropdown, DropdownItem, DropdownMenu, DropdownToggle, Table } from 'reactstrap';
import { Can } from '../../Auth/CASL/can';
import { Link } from 'react-router-dom';

BoxHome.propTypes = {
    listOutInvoices: PropTypes.array,
    isLoading: PropTypes.bool,
    onBtnEditClick: PropTypes.func,
    handleSearch: PropTypes.func,
    pagination: PropTypes.object,
    onPageChange: PropTypes.func,
    onBtnAddClick: PropTypes.func,
    onBtnAddListClick: PropTypes.func,
    onBtnDeleteClick: PropTypes.func,
    onBtnHistoryClick: PropTypes.func,
}

BoxHome.defaultProps = {
    listOutInvoices: [],
    isLoading: true,
    onBtnEditClick: null,
    handleSearch: null,
    pagination: null,
    onPageChange: null,
    onBtnAddClick: null,
    onBtnAddListClick: null,
    onBtnDeleteClick: null,
    onBtnHistoryClick: null
}

function BoxHome(props) {
    const { listOutInvoices, onBtnEditClick, handleSearch, pagination, onPageChange, onBtnFilterClick } = props
    const { _page, _limit, _totalRows } = pagination;

    const [dropdownOpen, setOpen] = useState(false);
    const toggle = () => setOpen(!dropdownOpen);

    const listTrackingTransactions = [
        {
            name: 'All',
            value: -99
        },
        {
            name: 'Pending',
            value: 1
        },
        {
            name: 'Approve',
            value: 2
        },
        {
            name: 'Shipping',
            value: 3
        },
        {
            name: 'Recieved',
            value: 4
        },
        {
            name: 'Cancel',
            value: 0
        }
    ];

    const totalPage = Math.ceil(_totalRows / _limit)
    const [searchTerm, setSearchTerm] = useState(null)
    const [activePage, setActivePage] = useState(_page)
    const typingTimeOutRef = useRef(null)

    function handlePageChange(pageNumber) {
        if (!onPageChange) return
        setActivePage(pageNumber)
        onPageChange(pageNumber)
    }
    function handleBtnEditClick(item) {
        if (!onBtnEditClick || item.status === 4 || item.status === 0) return
        onBtnEditClick(item, "edit")
    }
    function handleBtnFilterClick(status) {
        if (!onBtnFilterClick) return
        onBtnFilterClick(status)
    }
    function handleBtnCancleClick(item) {
        if (!onBtnEditClick || item.status === 4) return
        onBtnEditClick(item, "cancel")
    }
    function handleSearchInput(e) {
        setSearchTerm(e.target.value)
        if (!handleSearch) return
        if (typingTimeOutRef.current) clearTimeout(typingTimeOutRef.current)
        typingTimeOutRef.current = setTimeout(() => {
            handleSearch({
                searchTerm: e.target.value
            })
        }, 300)
    }

    return (
        <div className="row">
            <div className="col-lg-12">
                <div className="card">
                    <div className="card-header">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                            style={{ display: "flex", justifyContent: "space-between" }}
                        >
                            <h4>All outinvoices | Total pages: {totalPage} | You are now at: {_page}</h4>


                        </div>
                    </div>
                    <div
                        style={{ padding: "10px 25px" }}
                    >
                        <Link
                            to="/"
                        >
                            <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                        </Link>
                    </div>
                    <div className="card-body">
                        <div className="card-header">
                            <h6>Quick filer</h6>
                        </div>

                        <div style={{ display: "flex", marginBottom: "4px" }}>
                            <Can I="update" a="OutportProduct">
                                {/* <Button
                                    onClick={() => handleBtnFilterClick(-99)}
                                    color="primary"
                                    style={{ flex: "0.08", marginRight: "4px" }}
                                >
                                    All
                                </Button>
                                <Button
                                    onClick={() => handleBtnFilterClick(1)}
                                    color="primary"
                                    style={{ flex: "0.08", marginRight: "4px" }}
                                >
                                    Pendding
                                </Button>
                                <Button
                                    onClick={() => handleBtnFilterClick(2)}
                                    color="primary"
                                    style={{ flex: "0.08", marginRight: "4px" }}
                                >
                                    Approve
                                </Button>
                                <Button
                                    onClick={() => handleBtnFilterClick(3)}
                                    color="primary"
                                    style={{ flex: "0.08", marginRight: "4px" }}
                                >
                                    Shipping
                                </Button>
                                <Button
                                    onClick={() => handleBtnFilterClick(4)}
                                    color="primary"
                                    style={{ flex: "0.08", marginRight: "4px" }}
                                >
                                    Recieved
                                </Button>
                                <Button
                                    onClick={() => handleBtnFilterClick(0)}
                                    color="primary"
                                    style={{ flex: "0.08", marginRight: "4px" }}
                                >
                                    Cancel
                                </Button> */}
                                <ButtonDropdown isOpen={dropdownOpen} toggle={toggle}>
                                    <DropdownToggle caret color="primary">
                                        Filter by invoice
                                    </DropdownToggle>
                                    <DropdownMenu
                                        style={{
                                            width: "100%"
                                        }}
                                    >
                                        {
                                            listTrackingTransactions.map(item => (
                                                <DropdownItem
                                                    onClick={() => handleBtnFilterClick(item.value)}
                                                    style={{
                                                        width: "100%",
                                                        height: "30px"
                                                    }}
                                                >
                                                    {item.name}
                                                </DropdownItem>
                                            ))
                                        }

                                    </DropdownMenu>
                                </ButtonDropdown>
                            </Can>
                            <div class="search-container">
                                <form action="#">
                                    <input
                                        type="text"
                                        placeholder="OutInvoiceId here...."
                                        name="searchData"
                                        value={searchTerm}
                                        onChange={(e) => handleSearchInput(e)}
                                    />
                                </form>
                            </div>
                        </div>
                        <div>
                            <Table>
                                <thead>
                                    <tr>
                                        <th>Out invoice id</th>
                                        <th>Total money</th>
                                        <th>Status</th>
                                        <Can I="update" a="OutportProduct">
                                            <th>Action</th>
                                        </Can>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        listOutInvoices.map((item) => (
                                            <tr key={item.id}>
                                                <td>{item.id}</td>
                                                <td>{item.totalMoney}</td>
                                                <td>{
                                                    item.status === 0 ? 'cancel'
                                                        : item.status === 1 ? 'pendding'
                                                            : item.status === 2 ? 'approve'
                                                                : item.status === 3 ? 'shipping'
                                                                    : item.status === 4 ? 'recieved'
                                                                        : ''
                                                }</td>
                                                <Can I="update" a="OutportProduct">
                                                    <td>
                                                        <div style={{ display: "flex" }}>
                                                            {/* <CusOutInvoicePropDown item={item} handleBtnEditClick={handleBtnEditClick} handleBtnCancleClick={handleBtnCancleClick} /> */}
                                                            <Button
                                                                onClick={() => handleBtnEditClick(item)}
                                                                color="warning"
                                                                style={{
                                                                    flex: "0.3"
                                                                }}
                                                            >
                                                                {
                                                                    item.status === 1 ? "Approve"
                                                                        : item.status === 2 ? "Shipping"
                                                                            : item.status === 3 ? "Recieved"
                                                                                : item.status === 0 ? "Done"
                                                                                    : item.status === 4 ? "Done"
                                                                                        : ""}
                                                            </Button>
                                                            <Button
                                                                onClick={() => handleBtnCancleClick(item)}
                                                                color="danger"
                                                            >
                                                                <i class="far fa-times-circle"></i>
                                                            </Button>
                                                            <Link
                                                                to={`/outInvoice/detail/${item.id}`}
                                                            >
                                                                <Button
                                                                    color="primary"
                                                                >
                                                                    <i class="fas fa-info-circle"></i>
                                                                </Button>
                                                            </Link>
                                                        </div>
                                                    </td>
                                                </Can>
                                            </tr>
                                        ))
                                    }
                                </tbody>
                            </Table>
                            <div className="pagination">
                                <Pagination
                                    itemClass="page-item"
                                    linkClass="page-link"
                                    activePage={activePage}
                                    itemsCountPerPage={_limit}
                                    totalItemsCount={_totalRows}
                                    pageRangeDisplayed={5}
                                    onChange={handlePageChange}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default BoxHome;