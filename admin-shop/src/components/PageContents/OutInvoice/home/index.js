import React, { useState, useEffect } from 'react';
import BoxHome from './home';
import Loading from 'components/Commons/Loading';
import { connectionNotifyHub } from "components/Constant";
import notifyApi from 'api/notifyApi';
import outInvoiceApi from 'api/outInvoiceApi';

//handle data
function HomeOutInvoice(props) {
    const [listOutInvoices, setListOutInvoices] = useState([])
    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [forceLoad, setForceLoad] = useState(true)

    const [filter, setFilter] = useState({
        _limit: 10,
        _page: 1,
        status: -99,
        id: 0,
    })

    const [pagination, setPagination] = useState({
        _page: 1,
        _limit: 10,
        _totalRows: 1,
    })

    async function addNotify(customerId, invoiceId, type) {
        try {
            const response = await notifyApi.addNotify({
                customerId: customerId,
                type: parseInt(type) + 1,
                invoiceId: invoiceId
            });
            if (response.code === 1) {
                if (connectionNotifyHub)
                    connectionNotifyHub.send("UpdateListNotify", customerId);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api add discount", error);
        }
    }

    useEffect(() => {

        async function getDataFromApi() {
            setListOutInvoices([]);
            try {
                const response = await outInvoiceApi.getAllOutInvoices(filter);
                if (response.code === 1) {
                    setListOutInvoices(response.data.listOutInvoiceRequest)
                    setPagination(response.data.pagination)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, filter, forceLoad]);

    async function onBtnEditClick(item, type) {
        if (!item) return
        if (type === "cancel")
            item.status = -1;

        try {
            const response = await outInvoiceApi.updateOutInvoice(type, item);
            if (response.code === 1) {
                addNotify(item.customerId, item.id, item.status)
                setForceLoad(!forceLoad)
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    async function onBtnFilterClick(status) {
        console.log(status)
        await setFilter({
            ...filter,
            _page: 1,
            status: status,
        })
    }

    function handleSearch(newFilter) {
        if (!newFilter) return
        setFilter({
            ...filter,
            _page: 1,
            id: newFilter.searchTerm ? newFilter.searchTerm : 0,
        })
    }

    function handlePageChange(newPage) {
        if (!newPage) return
        setFilter({
            ...filter,
            _page: newPage,
        })
    }

    return (
        isFetchComplete
            ? <BoxHome
                listOutInvoices={listOutInvoices}
                onBtnFilterClick={onBtnFilterClick}
                onBtnEditClick={onBtnEditClick}
                handleSearch={handleSearch}
                pagination={pagination}
                onPageChange={handlePageChange}
            />
            : <Loading />
    );
}

export default HomeOutInvoice;