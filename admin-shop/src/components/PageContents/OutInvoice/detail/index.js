import React, { useState, useEffect } from 'react';
import BoxDetail from './detail';
import Loading from 'components/Commons/Loading';
import { useParams } from "react-router-dom";
import outInvoiceApi from 'api/outInvoiceApi';

//handle data
function DetailOutInvoice() {

    let { id } = useParams();
    const [isFetchComplete, setIsFetchComplete] = useState(false);
    const [outInvoiceMaster, setOutInvoiceMaster] = useState(null);
    const [listSubProducts, setListSubProducts] = useState(null);
    const [listHistoryChanges, setListHistoryChanges] = useState(null);

    useEffect(() => {

        async function getDataFromApi() {
            try {
                const response = await outInvoiceApi.getOutInvoiceDetail(id);
                if (response.code === 1) {
                    setOutInvoiceMaster(response.data.outInvoiceMaster)
                    setListSubProducts(response.data.listSubProducts)
                    setListHistoryChanges(response.data.listHistoryChanges)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api add discount", error);
            }
        }

        getDataFromApi();
    }, [id]);

    return (
        isFetchComplete ?
            <BoxDetail
                outInvoiceMaster={outInvoiceMaster}
                listSubProducts={listSubProducts}
                listHistoryChanges={listHistoryChanges}
            />
            :
            <Loading />
    );
}

export default DetailOutInvoice;