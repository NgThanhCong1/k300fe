import { Card, Table, CardTitle, CardText } from 'reactstrap';
import { Link } from 'react-router-dom';

function BoxDetail(props) {
    const { outInvoiceMaster, listSubProducts, listHistoryChanges } = props
    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h6 class="mb-2 text-primary">Update subproduct for {outInvoiceMaster.id}</h6>
                   
                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
            <span>
                        <Link
                            to="/outInvoice/home"
                        >
                            <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                        </Link>
                        <Link
                            to={`/outInvoice/update/${outInvoiceMaster.id}`}
                        >
                            <button className="btn btn-warning"><i class="fas fa-edit"></i></button>
                        </Link>
                    </span>
                    </div>
            <div className="card-body">
                <div>
                    <div className="row">

                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                            <Card body outline color="secondary">
                                <CardTitle tag="h6">Customer information</CardTitle>
                                <CardText>Fullname: {outInvoiceMaster.fullName}</CardText>
                                <CardText>Email: {outInvoiceMaster.email}</CardText>
                                <CardText>Phone: {outInvoiceMaster.phoneNumber}</CardText>
                                <CardText>Address: {outInvoiceMaster.address}</CardText>
                            </Card>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                            <Card body outline color="secondary">
                                <CardTitle tag="h6">Shipping Information</CardTitle>
                                <CardText>Fullname: {outInvoiceMaster.shipName}</CardText>
                                <CardText>Phone: {outInvoiceMaster.shipPhone}</CardText>
                                <CardText>Address: {outInvoiceMaster.shipAt}</CardText>
                            </Card>
                        </div>
                    </div>
                </div>
                <h6 style={{ padding: "20px", background: "#f4f6f9" }}>Order Summary</h6>
                <div>
                    <div className="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                            <Card body outline color="secondary">
                                <CardText>Total quantity: {outInvoiceMaster.totalQuantity}</CardText>
                                <CardText>Order total: {outInvoiceMaster.totalMoney}</CardText>
                                <CardText>Payment method: {outInvoiceMaster.paymentMethod}</CardText>
                            </Card>
                        </div><div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                            <Card body outline color="secondary">
                                <CardText>Payment method: {outInvoiceMaster.paymentMethod}</CardText>
                                <CardText>Shipping charge: {outInvoiceMaster.shippingCharge}</CardText>
                            </Card>
                        </div><div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                            <Card body outline color="secondary">
                                <CardText>Order date: {outInvoiceMaster.createdAt}</CardText>
                                <CardText>CreatedBy: {outInvoiceMaster.createdBy}</CardText>
                            </Card>
                        </div>
                    </div>
                </div>
                <h6 style={{ padding: "20px", background: "#f4f6f9" }}>Order products</h6>
                <Table style={{ marginBottom: "unset" }} hover>
                    <thead>
                        <tr>
                            <th>ProductCode</th>
                            <th>ProductName</th>
                            <th>Price</th>
                            <th>Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            listSubProducts.map(item => (
                                <tr style={{ height: "150px", margin: "4px 0" }} key={item.productCode}>
                                    <td>{item.productCode}</td>
                                    <td
                                        style={{ display: "flex", maxWidth: "500px", height: "100%" }}
                                    >
                                        <img
                                            style={{ maxHeight: "150px" }}
                                            src={`${process.env.REACT_APP_IMAGE_URL}${item.listImagesLink[0]}`}
                                            alt="alt here"
                                        />
                                        <div style={{ marginRight: "4px" }}>
                                            <p>Name: {item.productName}</p>
                                            <p>Color: {item.color}</p>
                                            <p>Size: {item.size}</p>
                                        </div>
                                    </td>
                                    <td>{item.price}</td>
                                    <td>{item.quantity}</td>
                                </tr>
                            ))
                        }
                    </tbody>
                </Table>

                <h6 style={{
                    padding: "20px",
                    background: "#f4f6f9",
                    marginTop: "30px"
                }}
                >
                    History changes
                </h6>
                <Table style={{ marginBottom: "unset" }} hover>
                    <thead>
                        <tr>
                            <th>OutInvoiceId</th>
                            <th>TotalMoney</th>
                            <th>ShipPhone</th>
                            <th>ShipName</th>
                            <th>ShipAt</th>
                            <th>Status</th>
                            <th>UpdatedBy</th>
                            <th>UpdatedAt</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            listHistoryChanges && listHistoryChanges.map((item, key) => (
                                <tr key={key}>
                                    <td>{item.id}</td>
                                    <td>{item.totalMoney}</td>
                                    <td>{item.shipPhone}</td>
                                    <td>{item.shipName}</td>
                                    <td>{item.shipAt}</td>
                                    <td>
                                        {
                                            item.status === 0 ? 'cancel'
                                                : item.status === 1 ? 'pendding'
                                                    : item.status === 2 ? 'approve'
                                                        : item.status === 3 ? 'shipping'
                                                            : item.status === 4 ? 'recieved'
                                                                : ''
                                        }
                                    </td>
                                    <td>{item.updatedBy}</td>
                                    <td>{item.updatedAt}</td>
                                </tr>
                            ))
                        }
                    </tbody>
                </Table>
            </div>
        </>

    );
}

export default BoxDetail;