import { useState } from "react";
import { useForm } from "react-hook-form";
import { Link } from 'react-router-dom';

function BoxAdd(props) {
    const { listCategories, handleAddCategory } = props
    const { register, handleSubmit, formState: { errors }, reset } = useForm();

    const [listFiles, setListFiles] = useState([])
    const [listFilesObj, setListFilesObj] = useState([])
    const fileArray = []
    const fileObj = []

    const onSubmit = data => {
        handleAddCategory({
            name: data.categoryName,
            parentCategory: data.parentCategory,
            images: listFilesObj,
        })
        reset();
    }
    function handleUploadFiles(e) {
        fileObj.push(e.target.files)
        for (let i = 0; i < fileObj[0].length; i++) {
            fileArray.push(URL.createObjectURL(fileObj[0][i]))
        }
        setListFiles(fileArray)
        setListFilesObj(e.target.files)
    }
    return (
        <>
            <div className="card-header">
                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h4 className="mb-2 text-primary">Add category</h4>
                  
                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
            <Link
                        to="/category/home"
                    >
                        <button className="btn btn-primary">Back <i className="fas fa-arrow-left"></i></button>
                    </Link>
                    </div>
            <div className="card-body">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="row">
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label htmlFor="exampleFormControlSelect1">ParentCategory</label>
                                <select
                                    className="form-control"
                                    id="ParentCategory"
                                    name="ParentCategory"
                                    {...register('parentCategory')}
                                >
                                    <option value="-1">pick a ParentCategory </option>
                                    {
                                        listCategories.map((category) => (
                                            <option
                                                key={category.id}
                                                value={category.id}
                                            >
                                                {category.name}
                                            </option>
                                        ))
                                    }
                                </select>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label htmlFor="categoryName">Category name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="categoryName"
                                    placeholder="Enter category name"
                                    {...register('categoryName', { required: true })}
                                />
                                {errors.categoryName && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label htmlFor="listFiles">Picture:</label>
                                <input
                                    type="file"
                                    className="form-control"
                                    id="listFiles"
                                    onChange={(e) => handleUploadFiles(e)}
                                />
                                <div>
                                    {
                                        (listFiles).map(url => (
                                            <img style={{ maxWidth: "100px" }} src={url} alt="..." />
                                        ))
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="form-group">
                                <button className="btn btn-primary" type="submit">Add</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>
    );
}

export default BoxAdd;