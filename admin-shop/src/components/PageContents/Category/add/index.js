import React, { useState, useEffect } from 'react';
import BoxAdd from './add';
import categoryApi from 'api/categoryApi';

function AddCategory() {
    const [listCategories, setListCategories] = useState([])
    const [isFetchComplete, setIsFetchComplete] = useState(false)

    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await categoryApi.getParentCategories();
                if (response.code === 1) {
                    setListCategories(response.data)
                    setIsFetchComplete(true)

                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api add category", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete]);
    async function handleAddCategory(addCategory) {

        // validate inputs
        if (addCategory === null) return;
        let formData = new FormData()
        for (var index = 0; index < addCategory.images.length; ++index) {
            formData.append('imageFiles', addCategory.images[index])
        }
        formData.append('name', addCategory.name);
        if (addCategory.parentCategory > 0)
            formData.append('parentCategory', addCategory.parentCategory);

        //fetch api
        try {
            const response = await categoryApi.addCategory(formData);
            alert(response.message);
        } catch (error) {
            console.log("fail to fetch api add category", error);
        }
    }

    return (
        isFetchComplete && <BoxAdd
            listCategories={listCategories}
            handleAddCategory={handleAddCategory}
        />
    );
}

export default AddCategory;