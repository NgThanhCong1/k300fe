import { useState } from "react";
import { useForm } from "react-hook-form";
import { Link } from 'react-router-dom';
import { rootPathImg } from 'components/Constant';

function BoxUpdate(props) {
    const { category, listCategories, handleUpdateCategory } = props
    // const [categoryName, setCategoryName] = useState(category.name)
    // const [parentCategory, setParentCategory] = useState(category.parentCategory)

    const [listFiles, setListFiles] = useState([])
    const [listFilesObj, setListFilesObj] = useState([])
    const { register, handleSubmit, formState: { errors } } = useForm();
    const fileArray = []
    const fileObj = []

    const onSubmit = data => {
        handleUpdateCategory({
            name: data.categoryName,
            parentCategory: data.parentCategory,
            images: listFilesObj,
        })
    }
    function handleUploadFiles(e) {
        fileObj.push(e.target.files)
        for (let i = 0; i < fileObj[0].length; i++) {
            fileArray.push(URL.createObjectURL(fileObj[0][i]))
        }
        setListFiles(fileArray)
        setListFilesObj(e.target.files)
    }
    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h4 class="mb-2 text-primary">Update category</h4>
                
                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
            <Link
                        to="/category/home"
                    >
                        <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                    </Link>
                    </div>
            <div className="card-body">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">ParentCategory</label>
                                <select class="form-control" id="ParentCategory"
                                    defaultValue={category.parentCategory}
                                    {...register('parentCategory')}
                                >
                                    <option value={0}>No parents category</option>
                                    {
                                        listCategories.map((brand) => (
                                            <option key={brand.id} value={brand.id}>{brand.name}</option>
                                        ))
                                    }
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="categoryName">Category name</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="categoryName"
                                    placeholder="Enter Category name"
                                    {...register('categoryName', { required: true })}
                                    defaultValue={category.name}
                                />
                                {errors.categoryName && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <label htmlFor="listFiles">Pictures of product:</label>
                            <input
                                type="file"
                                className="form-control"
                                id="listFiles"
                                onChange={(e) => handleUploadFiles(e)}
                            />
                            <div>
                                {listFiles.length === 0 ?
                                    <img style={{ maxWidth: "100px" }} src={`${rootPathImg}${category.images}`} alt="..." />
                                    :
                                    (listFiles).map(url => (
                                        <img style={{ maxWidth: "100px" }} src={url} alt="..." />
                                    ))
                                }
                            </div>
                        </div>

                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <button className="btn btn-primary" type="submit">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>

    );
}

export default BoxUpdate;