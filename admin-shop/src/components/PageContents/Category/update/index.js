import BoxUpdate from './update';
import { useParams } from 'react-router';
import { useState, useEffect } from 'react';
import Loading from 'components/Commons/Loading';
import categoryApi from 'api/categoryApi';

function UpdateCategory() {

    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [category, setCategory] = useState(null)
    const [listCategories, setListCategories] = useState([])
    let { id } = useParams()

    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await categoryApi.getDataForUpdateCategory(id);
                if (response.code === 1) {
                    setCategory(response.data.category)
                    setListCategories(response.data.listCategories)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api get all brands", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, id]);

    async function handleUpdateCategory(updateCategory) {

        if (!updateCategory) return
        let formData = new FormData()
        for (var index = 0; index < updateCategory.images.length; ++index) {
            formData.append('imageFiles', updateCategory.images[index])
        }
        formData.append('id', id);
        formData.append('name', updateCategory.name);
        formData.append('parentCategory', updateCategory.parentCategory);

        try {
            const response = await categoryApi.updateCategory(formData);
            if (response.code === 1) {
                alert(response.message)
                setIsFetchComplete(false)
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api update category", error);
        }
    }
    return (
        isFetchComplete ?
            <BoxUpdate
                category={category}
                listCategories={listCategories}
                handleUpdateCategory={handleUpdateCategory}
            />
            :
            <Loading />
    )
}

export default UpdateCategory;