import React, { useState, useEffect } from 'react';
import BoxHome from './home';
import Loading from 'components/Commons/Loading';
import categoryApi from 'api/categoryApi';

//handle data
function HomeCategory() {
    const [listCategories, setListCategories] = useState([])
    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [filter, setFilter] = useState({
        _limit: 10,
        _page: 1,
        nameLike: 'null',
    })
    const [pagination, setPagination] = useState({
        _page: 1,
        _limit: 10,
        _totalRows: 1,
    })
    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await categoryApi.getAllCategories(filter);
                if (response.code === 1) {
                    setListCategories(response.data.listCate)
                    setPagination(response.data.pagination)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api get all brands", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, filter]);
    function handleSearch(newFilter) {
        if (!newFilter) return
        setFilter({
            ...filter,
            _page: 1,
            nameLike: newFilter.searchTerm ? newFilter.searchTerm : 'null',
        })
    }
    function handlePageChange(newPage) {
        if (!newPage) return
        setFilter({
            ...filter,
            _page: newPage,
        })
    }

    async function deleteCategory(id) {
        if (!id) return
        try {
            const response = await categoryApi.deleteCategory({
                id: id
            });
            if (response.code === 1) {
                setIsFetchComplete(false);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api get all brands", error);
        }
    }
    function handleDelete(id, name) {
        if (window.confirm("delete ?" + name)) {
            deleteCategory(id);
        } else {
            console.log("no delete")
        }
    }
    return (
        isFetchComplete ?
            <BoxHome
                listCategories={listCategories}
                pagination={pagination}
                handleDelete={handleDelete}
                handleSearch={handleSearch}
                handlePageChange={handlePageChange}
            />
            :
            <Loading />
    );
}

export default HomeCategory;