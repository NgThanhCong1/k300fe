import React, { useState, useEffect } from 'react';
import BoxHome from './home';
import Loading from 'components/Commons/Loading';
import importProductApi from 'api/importProductApi';

function HomeInInvoice() {
    const [listInInvoices, setListInInvoices] = useState([])
    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [filter, setFilter] = useState({
        _limit: 10,
        _page: 1,
        inInvoiceId: -1,
    })
    const [pagination, setPagination] = useState({
        _page: 1,
        _limit: 10,
        _totalRows: 1,
    })

    useEffect(() => {

        async function getDataFromApi() {
            try {
                const response = await importProductApi.getAllIninvoices(filter);
                if (response.code === 1) {
                    setListInInvoices(response.data.listInInvoices)
                    setPagination(response.data.pagination)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api getAllIninvoices", error);
            }
        }
        getDataFromApi();
    }, [filter, isFetchComplete]);

    function handleSearch(newFilter) {
        if (!newFilter) return
        setFilter({
            ...filter,
            _page: 1,
            inInvoiceId: newFilter.searchTerm ? parseInt(newFilter.searchTerm) : -1,
        })
    }

    function handlePageChange(newPage) {
        if (!newPage) return
        setFilter({
            ...filter,
            _page: newPage,
        })
    }

    return (
        isFetchComplete ?
            <BoxHome
                listInInvoices={listInInvoices}
                handleSearch={handleSearch}
                pagination={pagination}
                handlePageChange={handlePageChange}
            />
            :
            <Loading />
    );
}

export default HomeInInvoice;