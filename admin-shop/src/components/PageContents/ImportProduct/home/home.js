import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import Pagination from "react-js-pagination";
import { Link } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { Can } from '../../Auth/CASL/can';


BoxHome.propTypes = {
    listInInvoices: PropTypes.array,
    handleSearch: PropTypes.func,
    pagination: PropTypes.object,
    handlePageChange: PropTypes.func,
}

BoxHome.defaultProps = {
    listInInvoices: [],
    handleSearch: null,
    pagination: null,
    handlePageChange: null,
}

function BoxHome(props) {
    const { listInInvoices, handleSearch, pagination, handlePageChange } = props
    const { _page, _limit, _totalRows } = pagination

    const totalPage = Math.ceil(_totalRows / _limit)
    const [searchTerm, setSearchTerm] = useState('')
    const [activePage, setActivePage] = useState(_page)
    const typingTimeOutRef = useRef(null)

    function onPageChange(pageNumber) {
        if (!handlePageChange) return
        setActivePage(pageNumber)
        handlePageChange(pageNumber)
    }
    function handleSearchInput(e) {
        setSearchTerm(e.target.value)
        const searchValue = e.target.value
        if (!handleSearch) return
        if (typingTimeOutRef.current) clearTimeout(typingTimeOutRef.current)
        typingTimeOutRef.current = setTimeout(() => {
            handleSearch({
                searchTerm: searchValue
            })
        }, 300)
    }

    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h4>All ininvoices | Total pages: {totalPage} | You are now at: {_page}</h4>


                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
                <Link
                    to="/"
                >
                    <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                </Link>
            </div>
            <div className="card-body">
                <div style={{ display: "flex", marginBottom: "4px" }}>
                    <Can I="create" a="ImportProduct">
                        <Link
                            to="/import-product/add"
                            className="addGGSheet-btn"
                        >
                            <Button
                                color="basic"
                                style={{ width: "100%" }}
                            >
                                <img
                                    src="https://cdn.iconscout.com/icon/free/png-512/google-sheets-4-569453.png"
                                    style={{ height: "18px" }}
                                    alt="alt here"
                                />
                            </Button>
                        </Link>
                    </Can>
                    <div className="search-container">
                        <form action="#">
                            <input
                                type="text"
                                placeholder="search.."
                                name="searchData"
                                value={searchTerm}
                                onChange={(e) => handleSearchInput(e)}
                            />
                        </form>
                    </div>
                </div>
                <div>
                    <Table>
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>TotalMoney</th>
                                <th>Import At</th>
                                <Can I="update" a="ImportProduct">
                                    <th>Action</th>
                                </Can>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                listInInvoices.map((item) => (
                                    <tr key={item.id}>
                                        <td>{item.id}</td>
                                        <td>{item.totalMoney}</td>
                                        <td>{item.createdAt}</td>
                                        <Can I="update" a="ImportProduct">
                                            <td>
                                                <Link
                                                    to={`/import-product/viewdetail/${item.id}`}
                                                >
                                                    <Button
                                                        color="primary"
                                                        style={{ marginRight: "16px" }}
                                                    >
                                                        <i class="fas fa-info-circle"></i>
                                                    </Button>
                                                </Link>
                                            </td>
                                        </Can>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                    <div className="pagination">
                        <Pagination
                            itemClass="page-item"
                            linkClass="page-link"
                            activePage={activePage}
                            itemsCountPerPage={_limit}
                            totalItemsCount={_totalRows}
                            pageRangeDisplayed={5}
                            onChange={onPageChange}
                        />
                    </div>
                </div>
            </div>
        </>

    );
}

export default BoxHome;