import React from 'react';
import { Table } from 'reactstrap';
import { Link } from 'react-router-dom';

function BoxDetail(props) {
    const { listInInvoiceDetails } = props
    return (
        <>
            <div className="card-header" style={{ display: "flex", justifyContent: "space-between" }}>
                <h4>All InInvoiceDetails by IninvoiceId</h4>
         
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
            <Link
                    to="/import-product/home"
                >
                    <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                </Link>
                </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                <div class="form-group">
                    <label for="createdBy">CreatedBy</label>
                    <input type="text" class="form-control" id="createdBy" placeholder="Enter CreatedBy"
                        value={listInInvoiceDetails[0].createdBy}
                        disabled
                    />
                </div>
            </div>
            <div className="card-body">
                <div>
                    <Table>
                        <thead>
                            <tr>
                                <th>InInvoiceId</th>
                                <th>ProductCode</th>
                                <th>InPrice</th>
                                <th>Quantity</th>
                                <th>Import At</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                listInInvoiceDetails.map((item) => (
                                    <tr key={item.inInvoiceId}>
                                        <td>{item.inInvoiceId}</td>
                                        <td>{item.productCode}</td>
                                        <td>{item.inPrice}</td>
                                        <td>{item.quantity}</td>
                                        <td>{item.createdAt}</td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                </div>
            </div>
        </>

    );
}

export default BoxDetail;
