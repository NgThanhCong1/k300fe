import { useState, useEffect } from 'react';
import BoxDetail from './detail';
import { useParams } from 'react-router-dom';
import Loading from 'components/Commons/Loading';
import importProductApi from 'api/importProductApi';

function InInvoiceDetail() {
    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [listInInvoiceDetails, setListInInvoiceDetails] = useState([])
    let { id } = useParams();

    useEffect(() => {

        async function getDataFromApi() {
            try {
                const response = await importProductApi.getInInvoiceDetails(id);
                if (response.code === 1) {
                    setListInInvoiceDetails(response.data.listInInvoiceDetails)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api add discount", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, id]);

    return (
        listInInvoiceDetails && isFetchComplete ?
            <BoxDetail
                listInInvoiceDetails={listInInvoiceDetails}
            />
            :
            <Loading />
    );
}

export default InInvoiceDetail;