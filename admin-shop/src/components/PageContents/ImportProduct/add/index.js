import { useState, useEffect } from 'react';
import BoxAdd from './add';
import importProductApi from 'api/importProductApi';

function AddInInvoice() {

    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [listDistributors, setListDistributors] = useState([])
    const [alertInfor, setAlertInfor] = useState({
        type: 0,
        message: ''
    })

    useEffect(() => {

        async function getDataFromApi() {
            try {
                const response = await importProductApi.getDataForAddInInvoice();
                if (response.code === 1) {
                    setListDistributors(response.data.listDistributors)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api add discount", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete]);

    async function handleAddInInvoice(addInInvoice) {
        if (!addInInvoice) return;
        try {
            const response = await importProductApi.addInInvoice(addInInvoice);
            if (response.code === 1) {
                setAlertInfor({
                    type: 1,
                    message: response.message
                })
            } else {
                setAlertInfor({
                    type: -1,
                    message: response.message
                })
            }
        } catch (error) {
            console.log("fail to fetch api add discount", error);
        }
        window.scrollTo(0, 0);
    }

    return (
        isFetchComplete &&
        <BoxAdd
            listDistributors={listDistributors}
            handleAddInInvoice={handleAddInInvoice}
            alertInfor={alertInfor}
        />
    );
}

export default AddInInvoice;