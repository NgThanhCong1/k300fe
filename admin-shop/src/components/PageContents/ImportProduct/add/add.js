import React from 'react';
import { Link } from 'react-router-dom';
import { useForm } from "react-hook-form";
import CustomAlert from 'components/Commons/Alert';

function BoxAdd(props) {
    const {
        listDistributors,
        handleAddInInvoice,
        alertInfor
    } = props

    const { register, handleSubmit, formState: { errors }, reset } = useForm({
        mode: 'onBlur',
    });

    const onSubmit = data => {
        if (!handleAddInInvoice) return
        handleAddInInvoice(
            {
                GGSheet: {
                    link: data.linkGGSheet,
                    sheetName: data.sheetName,
                    rangeData: data.rangeData,
                },
                DistributorId: data.distributorId
            }
        )
        reset();
        // console.log(data)
    }

    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h4 class="mb-2 text-primary">Import product</h4>

                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
                <Link
                    to="/import-product/home"
                >
                    <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                </Link>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
                <CustomAlert
                    {...alertInfor}
                />
            </div>
            <div className="card-body">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div class="row">

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="linkGGSheet">LinkGGSheet</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="linkGGSheet"
                                    placeholder="Enter LinkGGSheet"
                                    {...register('linkGGSheet', { required: true })}
                                />
                                {errors.linkGGSheet && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="sheetName">SheetName</label>
                                <input
                                    type="sheetName"
                                    class="form-control"
                                    id="sheetName"
                                    placeholder="Enter sheetName"
                                    {...register('sheetName', { required: true })}
                                />
                                {errors.sheetName && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="rangeData">RangeData</label>
                                <input
                                    type="rangeData"
                                    class="form-control"
                                    id="rangeData"
                                    placeholder="Enter rangeData"
                                    {...register('rangeData', { required: true })}
                                />
                                {errors.rangeData && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">Distributor</label>
                                <select
                                    class="form-control"
                                    id="distributor"
                                    {...register('distributorId', { required: true })}
                                >
                                    <option value="-1">pick a distributor </option>
                                    {
                                        listDistributors.map((distributor) => (
                                            <option key={distributor.id} value={distributor.id}>{distributor.name}</option>
                                        ))
                                    }
                                </select>
                                {errors.distributorId && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <button className="btn btn-primary" type="submit">Add</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>

    );
}

export default BoxAdd;