import BoxUpdate from './update';
import { useParams } from 'react-router';
import { useState, useEffect } from 'react';
import Loading from 'components/Commons/Loading';
import distributorApi from 'api/distributorApi';

function UpdateDistributor() {

    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [distributor, setDistributor] = useState(null)
    let { id } = useParams()

    useEffect(() => {

        async function getDataFromApi() {
            try {
                const response = await distributorApi.getDataForUpdateDistributor(id);
                if (response.code === 1) {
                    setDistributor(response.data)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api add discount", error);
            }
        }
        getDataFromApi();

    }, [isFetchComplete, id]);

    async function handleUpdateDistributor(updateDistributor) {

        if (!updateDistributor) return;
        try {
            const response = await distributorApi.updateDistributor({ ...updateDistributor, id: id });
            if (response.code === 1) {
                alert(response.message)
                setIsFetchComplete(false)
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api add discount", error);
        }
    }

    return (
        isFetchComplete ?
            <BoxUpdate
                distributor={distributor}
                handleUpdateDistributor={handleUpdateDistributor}
            />
            :
            <Loading />
    )
}

export default UpdateDistributor;