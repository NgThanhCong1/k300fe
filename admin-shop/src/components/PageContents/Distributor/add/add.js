import { useForm } from "react-hook-form";
import { Link } from 'react-router-dom';

function BoxAdd(props) {
    const { handleAddDistributor } = props

    const { register, handleSubmit, formState: { errors }, reset } = useForm();
    const onSubmit = data => {
        handleAddDistributor({
            name: data.distributorName,
            phone: data.phone,
            address: data.address,
        })
        reset();
    }

    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h4 class="mb-2 text-primary">Add distributor</h4>
              
                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
            <Link
                        to="/distributor/home"
                    >
                        <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                    </Link>
                    </div>
            <div className="card-body">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div class="row">

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="distributorName">Distributor name</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="distributorName"
                                    placeholder="Enter distributor name"
                                    {...register('distributorName', { required: true })}
                                />
                                {errors.distributorName && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="phone"
                                    placeholder="Enter phone"
                                    {...register('phone', { required: true })}
                                />
                                {errors.phone && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="address"
                                    placeholder="Enter address"
                                    {...register('address', { required: true })}
                                />
                                {errors.address && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>

                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <button className="btn btn-primary" type="submit">Add</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>

    );
}

export default BoxAdd;