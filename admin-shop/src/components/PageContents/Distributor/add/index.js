import React from 'react';
import BoxAdd from './add';
import distributorApi from 'api/distributorApi';

function AddDistributor() {
    async function handleAddDistributor(addDistributor) {

        if (!addDistributor) return;
        try {
            const response = await distributorApi.addDistributor(addDistributor);
            if (response.code === 1) {
                alert(response.message)
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api add discount", error);
        }
    }

    return (
        <BoxAdd
            handleAddDistributor={handleAddDistributor}
        />
    );
}

export default AddDistributor;