import { useEffect, useState } from "react";
import BoxHome from "./home";
import Loading from "components/Commons/Loading";
import accountApi from "api/accountApi";
import { useDispatch, useSelector } from "react-redux";
import { updateUser } from "actions";

function HomeAccount() {
    const [user, setUser] = useState(null);
    const [isFetchComplete, setIsFetchComplete] = useState(false);
    const userInfor = useSelector(state => state.userReducer).staffInfor;
    const dispatch = useDispatch();

    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await accountApi.getStaff(userInfor.id)
                setUser(response.data)
                setIsFetchComplete(true);
            } catch (error) {
                console.log("fail to fetch api get staff", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, userInfor.id]);

    async function handleUpdate(item) {
        if (!item) return;
        let formData = new FormData();
        formData.append('imageFile', item.imageFile)
        formData.append('id', userInfor.id)
        formData.append('userName', item.userName)
        formData.append('address', item.address)
        formData.append('email', item.email)
        formData.append('phone', item.phone)
        formData.append('firstName', item.firstName)
        formData.append('lastName', item.lastName)

        try {
            const response = await accountApi.updateStaff(formData)
            if (response.code === 1) {
                dispatch(updateUser(response.data));
            }
            alert(response.message);
            // setIsFetchComplete(false);
        } catch (error) {
            console.log("fail to fetch api get staff", error);
        }
    }
    return (
        isFetchComplete
            ? <BoxHome
                user={user}
                handleUpdate={handleUpdate}
            />
            : <Loading />
    );
}

export default HomeAccount