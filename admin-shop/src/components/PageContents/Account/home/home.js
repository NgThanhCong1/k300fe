import { useState } from "react";
import { rootPathImg } from "components/Constant";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";

function BoxHome(props) {
    const { user, handleUpdate } = props;
    const { register, handleSubmit, formState: { errors } } = useForm({
        mode: 'onBlur',
    });

    const userInfor = useSelector(state => state.userReducer);

    const [listFiles, setListFiles] = useState([])
    const [listFilesObj, setListFilesObj] = useState([])
    const fileArray = []
    const fileObj = []

    function handleUploadFiles(e) {
        fileObj.push(e.target.files)
        for (let i = 0; i < fileObj[0].length; i++) {
            fileArray.push(URL.createObjectURL(fileObj[0][i]))
        }
        setListFiles(fileArray)
        setListFilesObj(e.target.files)
    }

    const onSubmit = async data => {
        handleUpdate({
            ...user,
            lastName: data.lastName,
            address: data.address,
            phone: data.phone,
            email: data.email,
            firstName: data.firstName,
            imageFile: listFilesObj[0]
        });
    };
    return (
        <>
            <div className="card-header">
                <div
                    class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h6 class="mb-2 text-primary">Staff Profile</h6>
                </div>
            </div>

            <div className="card-body">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div
                                class="form-group"
                                style={{ textAlign: "center" }}
                            >
                                <label for="listFiles">
                                    {
                                        listFiles.length > 0 ?
                                            (listFiles).map(url => (
                                                <img
                                                    className="rounded-circle mr-1"
                                                    style={{ width: "100px", objectFit: "cover", height: "100px" }} src={url} alt="..."
                                                />
                                            ))
                                            :
                                            user.avatar ?
                                                <img
                                                    className="rounded-circle mr-1"
                                                    style={{ width: "100px", objectFit: "cover", height: "100px" }}
                                                    src={`${rootPathImg}${user.avatar}?v=${Math.random()}`}
                                                    alt="..."
                                                />
                                                :
                                                <img
                                                    className="rounded-circle mr-1"
                                                    style={{ width: "100px", objectFit: "cover", height: "100px" }}
                                                    src={`${rootPathImg}/Images/Users/user.png`}
                                                    alt="..."
                                                />
                                    }
                                </label>
                                <input
                                    type="file"
                                    className="form-control"
                                    id="listFiles"
                                    onChange={(e) => handleUploadFiles(e)}
                                    hidden
                                />
                                <h4 className="text-center">
                                    {userInfor && `${userInfor.staffInfor.firstName} ${userInfor.staffInfor.lastName}`}
                                </h4>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Email</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    disabled
                                    value={user.email}
                                />
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label>First Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    defaultValue={user.firstName}
                                    {...register('firstName', { required: true })}
                                />
                                {errors.firstName && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Last Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    defaultValue={user.lastName}
                                    {...register('lastName', { required: true })}
                                />
                                {errors.lastName && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Phone number</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    defaultValue={user.phoneNumber}
                                    {...register('phone', { required: true })}
                                />
                                {errors.phone && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Address</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    defaultValue={user.address}
                                    {...register('address', { required: true })}
                                />
                                {errors.address && <span style={{ color: "red" }}>This field is required</span>}
                            </div>
                        </div>

                        <div className="col-md-6">
                            <button className="btn btn-primary" type="submit">Update</button>
                            {/* <WaitButton
                                type="submit"
                                className="btn btn-primary"
                                btnText="Update"
                                isOff={isOffSubmitBtn}
                            /> */}
                        </div>
                    </div>
                </form>
            </div>
        </>
    );
}

export default BoxHome;