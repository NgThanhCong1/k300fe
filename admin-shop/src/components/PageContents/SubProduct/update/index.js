import BoxUpdate from './update';
import { useParams } from 'react-router';
import { useState, useEffect } from 'react';
import Loading from 'components/Commons/Loading';
import productApi from 'api/productApi';

function UpdateSubProduct() {
    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [subProduct, setSubProduct] = useState(null)
    let { productCode } = useParams()

    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await productApi.getDataForUpdateSubProduct(productCode);
                if (response.code === 1) {
                    setSubProduct(response.data)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, productCode]);

    async function handleUpdateSubProduct(updateSubProduct) {
        var formData = new FormData();
        for (var index = 0; index < updateSubProduct.listFiles.length; ++index) {
            formData.append('ListImageFiles', updateSubProduct.listFiles[index])
        }
        formData.append("ProductCode", updateSubProduct.productCode)
        formData.append("Color", updateSubProduct.color)
        formData.append("Size", updateSubProduct.size)

        try {
            const response = await productApi.updateSubProduct(formData);
            if (response.code === 1) {
                alert(response.message)
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }

        // let url = `${rootPath}Product/update-subProduct`
        // await fetch(url,
        //     {
        //         method: 'POST',
        //         mode: 'cors',
        //         cache: 'no-cache',
        //         credentials: 'same-origin',
        //         headers: {
        //             'Authorization': `Bearer ${localStorage.staffToken}`
        //         },
        //         body: formData
        //     }
        // )
        //     .then(response => response.json())
        //     .then(result => {
        //         if (result.code == 1) {
        //             alert(result.message)
        //         } else {
        //             alert(result.message)
        //         }
        //     })
        //     .catch(error => error.status === 401 ?
        //         localStorage.removeItem('staffToken')
        //         : alert(error.message))
        // console.log(updateSubProduct)
    }
    return (
        isFetchComplete ?
            <BoxUpdate
                productCode={productCode}
                subProduct={subProduct}
                handleUpdateSubProduct={handleUpdateSubProduct}
            />
            :
            <Loading />
    )
}

export default UpdateSubProduct;