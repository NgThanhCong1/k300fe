import { useState } from "react";
import { useForm } from "react-hook-form";
import { Link } from 'react-router-dom';

function BoxUpdate(props) {
    const { subProduct, productCode, handleUpdateSubProduct } = props

    const [listFiles, setListFiles] = useState(() => {
        let newListImagesLink = []
        for (let i = 0; i < subProduct.listImagesLink.length; i++) {
            newListImagesLink.push(`${process.env.REACT_APP_IMAGE_URL}${subProduct.listImagesLink[i]}`)
        }
        return newListImagesLink;
    })

    const [listFilesObj, setListFilesObj] = useState([])
    const { handleSubmit } = useForm();

    const fileArray = []
    const fileObj = []

    const onSubmit = data => {
        handleUpdateSubProduct({
            productCode: productCode,
            color: subProduct.color,
            size: subProduct.size,
            listFiles: listFilesObj
        })
    }

    function handleUploadFiles(e) {
        fileObj.push(e.target.files)
        for (let i = 0; i < fileObj[0].length; i++) {
            fileArray.push(URL.createObjectURL(fileObj[0][i]))
        }
        setListFiles(fileArray)
        setListFilesObj(e.target.files)
    }

    return (
        <>
            <div className="card-header">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h6 class="mb-2 text-primary">Update sub product</h6>
                 
                </div>
            </div>
            <div
                style={{ padding: "10px 25px" }}
            >
            <Link
                        to="/product/home"
                    >
                        <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                    </Link>
                    </div>
            <div className="card-body">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="productCode">Product code</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="productCode"
                                    placeholder="Enter product code"
                                    value={subProduct.productCode}
                                    disabled
                                />
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">Color</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="productCode"
                                    placeholder="Enter product code"
                                    value={subProduct.color}
                                    disabled
                                />
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label for="exampleFormControlSelect1">Size</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="productCode"
                                    placeholder="Enter product code"
                                    value={subProduct.size}
                                    disabled
                                />
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="form-group">
                                <label htmlFor="listFiles">Pictures of product:</label>
                                <input
                                    type="file"
                                    className="form-control"
                                    id="listFiles"
                                    multiple
                                    onChange={(e) => handleUploadFiles(e)}
                                />
                                <div>
                                    {
                                        (listFiles).map(url => (
                                            <img style={{ maxWidth: "100px" }} src={url} alt="..." />
                                        ))
                                    }
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <button className="btn btn-primary" type="submit">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>
    );
}

export default BoxUpdate;