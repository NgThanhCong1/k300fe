import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import Pagination from "react-js-pagination";
import { Link } from 'react-router-dom';
import { Button, Table } from 'reactstrap';


BoxHome.propTypes = {
    listSubProducts: PropTypes.array,
    handleSearch: PropTypes.func,
    pagination: PropTypes.object,
    handlePageChange: PropTypes.func,
}

BoxHome.defaultProps = {
    listSubProducts: [],
    handleSearch: null,
    pagination: null,
    handlePageChange: null,
}

function BoxHome(props) {
    const { listSubProducts, handleSearch, handleDelete, pagination, handlePageChange } = props
    const { _page, _limit, _totalRows } = pagination

    const totalPage = Math.ceil(_totalRows / _limit)
    const [searchTerm, setSearchTerm] = useState('')
    const [activePage, setActivePage] = useState(_page)
    const typingTimeOutRef = useRef(null)

    function onPageChange(pageNumber) {
        if (!handlePageChange) return
        setActivePage(pageNumber)
        handlePageChange(pageNumber)
    }
    function handleSearchInput(e) {
        setSearchTerm(e.target.value)
        const searchValue = e.target.value
        if (!handleSearch) return
        if (typingTimeOutRef.current) clearTimeout(typingTimeOutRef.current)
        typingTimeOutRef.current = setTimeout(() => {
            handleSearch({
                searchTerm: searchValue
            })
        }, 300)
    }

    return (
        <div className="row">
            <div className="col-lg-12">
                <div className="card">
                    <div className="card-header">
                        <h4>All SubProducts | TotalPages: {totalPage} | You are now at: {_page}</h4>
                    </div>
                    <div className="card-body">
                        <div style={{ display: "flex", marginBottom: "4px" }}>
                            <div className="search-container">
                                <form action="#" >
                                    <input
                                        type="text"
                                        placeholder="search.."
                                        name="searchData"
                                        value={searchTerm}
                                        onChange={(e) => handleSearchInput(e)}
                                    />
                                </form>
                            </div>
                        </div>
                        <div>
                            <Table>
                                <thead>
                                    <tr>
                                        <th>ProductCode</th>
                                        <th>Size</th>
                                        <th>Color</th>
                                        <th>Quantity</th>
                                        <th>Images Count</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        listSubProducts.map((item) => (
                                            <tr key={item.productCode}>
                                                <td>{item.productCode}</td>
                                                <td>{item.size}</td>
                                                <td>{item.color}</td>
                                                <td>{item.quantity}</td>
                                                <td>{item.listImagesLink.length}</td>
                                                <td>
                                                    <Link
                                                        to={`/sub-product/update/${item.productCode}`}
                                                    >
                                                        <Button
                                                            color="primary"
                                                            style={{ marginRight: "16px" }}
                                                        >
                                                            <i className="fas fa-edit"></i>
                                                        </Button>
                                                    </Link>
                                                    <Button
                                                        onClick={() => handleDelete(item.productCode)}
                                                        color="danger"
                                                    >
                                                        <i className="fas fa-trash"></i>
                                                    </Button>
                                                </td>
                                            </tr>
                                        ))
                                    }
                                </tbody>
                            </Table>
                            <div className="pagination">
                                <Pagination
                                    itemClass="page-item"
                                    linkClass="page-link"
                                    activePage={activePage}
                                    itemsCountPerPage={_limit}
                                    totalItemsCount={_totalRows}
                                    pageRangeDisplayed={5}
                                    onChange={onPageChange}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    );
}

export default BoxHome;