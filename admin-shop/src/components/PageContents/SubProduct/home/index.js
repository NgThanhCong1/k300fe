import React, { useState, useEffect } from 'react';
import BoxHome from './home';
import queryString from 'query-string';
import { rootPath } from 'components/Constant';
import Loading from 'components/Commons/Loading';

function HomeSubProduct() {
    const [listSubProducts, setListSubProducts] = useState([])
    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [filter, setFilter] = useState({
        _limit: 10,
        _page: 1,
        productCode: 'null',
    })
    const [pagination, setPagination] = useState({
        _page: 1,
        _limit: 10,
        _totalRows: 1,
    })
    useEffect(() => {
        async function getDataFromApi() {
            const paramStrings = queryString.stringify(filter)
            let url = `${rootPath}Product/get-all-subProduct?${paramStrings}`
            await fetch(url,
                {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': `Bearer ${localStorage.staffToken}`
                    },
                }
            )
                .then(response => response.json())
                .then(result => {
                    if (result.code === 1) {
                        setListSubProducts(result.data.listSubProducts)
                        setPagination(result.data.pagination)
                        setIsFetchComplete(true)
                    } else {
                        alert(result.message)
                    }
                })
                .catch(error => error.status === 401 ?
                    localStorage.removeItem('staffToken')
                    : alert(error.message))
        }
        getDataFromApi();
    }, [filter, isFetchComplete]);
    async function deleteSubProduct(productCode) {
        let url = `${rootPath}Product/delete-subProduct`
        await fetch(url,
            {
                method: 'POST',
                mode: 'cors',
                cache: 'no-cache',
                credentials: 'same-origin',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${localStorage.staffToken}`
                },
                body: JSON.stringify(productCode)
            }
        )
            .then(response => response.json())
            .then(result => {
                if (result.code === 1) {
                    alert(result.message)
                } else {
                    alert(result.message)
                }
            })
            .catch(error => error.status === 401 ?
                localStorage.removeItem('staffToken')
                : alert(error.message))
    }
    function handleSearch(newFilter) {
        if (!newFilter) return
        setFilter({
            ...filter,
            _page: 1,
            productCode: newFilter.searchTerm ? newFilter.searchTerm : 'null',
        })
    }
    function handlePageChange(newPage) {
        if (!newPage) return
        setFilter({
            ...filter,
            _page: newPage,
        })
    }
    function handleDelete(productCode) {
        console.log(productCode)
        if (window.confirm("delete ?" + productCode)) {
            deleteSubProduct(productCode);
            setIsFetchComplete(false);
        } else {
            console.log("no delete")
        }
    }
    return (
        isFetchComplete ?
            <BoxHome
                listSubProducts={listSubProducts}
                handleDelete={handleDelete}
                handleSearch={handleSearch}
                pagination={pagination}
                handlePageChange={handlePageChange}
            />
            :
            <Loading />
    );
}

export default HomeSubProduct;