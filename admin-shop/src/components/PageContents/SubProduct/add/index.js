import React from 'react';
import BoxAddSubProduct from './add';
import { rootPath } from '../../../Constant';
import { useParams } from 'react-router-dom';

function AddSubProduct() {
    const { id, name } = useParams();
    async function handleAddSubProduct(subProduct) {
        var formData = new FormData();
        for (var index = 0; index < subProduct.listFiles.length; ++index) {
            formData.append('ListImageFiles', subProduct.listFiles[index])
        }
        formData.append("ProductCode", subProduct.productCode)
        formData.append("ProductName", name)
        formData.append("ProductId", id)
        formData.append("Color", subProduct.color)
        formData.append("Size", subProduct.size)

        let url = `${rootPath}Product/add-subProduct`
        await fetch(url,
            {
                method: 'POST',
                mode: 'cors',
                cache: 'no-cache',
                credentials: 'same-origin',
                headers: {
                    'Authorization': `Bearer ${localStorage.staffToken}`
                },
                body: formData
            }
        )
            .then(response => response.json())
            .then(result => {
                if (result.code === 1) {
                    alert(result.message)
                } else {
                    alert(result.message)
                }
            })
            .catch(error => error.status === 401 ?
                localStorage.removeItem('staffToken')
                : alert(error.message))
    }

    return (
        <BoxAddSubProduct
            productId={id}
            productName={name}
            handleAddSubProduct={handleAddSubProduct}
        />
    );
}

export default AddSubProduct;