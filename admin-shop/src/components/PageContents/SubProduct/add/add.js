import { useState } from "react";
import { useForm } from "react-hook-form";
import { Link } from 'react-router-dom';

function BoxAddSubProduct(props) {
    const { productId, productName, handleAddSubProduct } = props
    const [productCode, setProductCode] = useState('')
    const [color, setColor] = useState('')
    const [size, setSize] = useState('')
    const [listFiles, setListFiles] = useState([])
    const [listFilesObj, setListFilesObj] = useState([])
    const { register, handleSubmit, formState: { errors } } = useForm();

    const listColors = ["black", "white", "red", "yellow", "purple", "blue", "green", "pink", "grey", "brown", "orange"]
    const listSizes = ["S", "M", "L", "XL", "XXL", "XXXL", "oversize"]
    const fileArray = []
    const fileObj = []


    const onSubmit = data => {
        handleAddSubProduct({
            productId: productId,
            productCode: productCode,
            color: color,
            size: size,
            listFiles: listFilesObj
        })
    }
    function handleUploadFiles(e) {
        fileObj.push(e.target.files)
        for (let i = 0; i < fileObj[0].length; i++) {
            fileArray.push(URL.createObjectURL(fileObj[0][i]))
        }
        setListFiles(fileArray)
        setListFilesObj(e.target.files)
    }
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <h6 class="mb-2 text-primary">Add subproduct for {productName}</h6>
                 
                </div>
                <div
                style={{ padding: "10px 25px" }}
            >
                <Link
                        to="/product/home"
                    >
                        <button className="btn btn-primary">Back <i class="fas fa-arrow-left"></i></button>
                    </Link>
                    </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                    <div class="form-group">
                        <label for="productCode">Product code</label>
                        <input type="text" class="form-control" id="productCode" placeholder="Enter product code"
                            {...register('productCode', { required: true })}
                            onChange={(e) => setProductCode(e.target.value)}
                        />
                        {errors.productCode && <span style={{ color: "red" }}>This field is required</span>}
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                    <div className="form-group">
                        <label for="exampleFormControlSelect1">Color</label>
                        <select class="form-control" id="color"
                            onChange={(e) => setColor(e.target.value)}
                        >
                            <option value="-1">pick a color </option>
                            {
                                listColors.map((color, key) => (
                                    <option key={key} value={color}>{color}</option>
                                ))
                            }
                        </select>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                    <div className="form-group">
                        <label for="exampleFormControlSelect1">Size</label>
                        <select class="form-control" id="size"
                            onChange={(e) => setSize(e.target.value)}
                        >
                            <option value="-1">pick a size </option>
                            {
                                listSizes.map((size, key) => (
                                    <option key={key} value={size}>{size}</option>
                                ))
                            }
                        </select>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                    <div className="form-group">
                        <label htmlFor="listFiles">Pictures of product:</label>
                        <input
                            type="file"
                            className="form-control"
                            id="listFiles"
                            multiple
                            onChange={(e) => handleUploadFiles(e)}
                        />
                        <div>
                            {
                                (listFiles).map(url => (
                                    <img style={{ maxWidth: "100px" }} src={url} alt="..." />
                                ))
                            }
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="form-group">
                        <button className="btn btn-primary" type="submit">Add</button>
                    </div>
                </div>
            </div>
        </form>
    );
}

export default BoxAddSubProduct;