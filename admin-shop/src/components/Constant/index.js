import * as signalR from "@microsoft/signalr";
const rootPath = "http://localhost:62284/api/";
const rootPathImg = "http://localhost:62284/";
const userName = localStorage.staffInfor ? localStorage.staffInfor : null;

const connectionNotifyHub = new signalR.HubConnectionBuilder()
    .withUrl("http://localhost:62284/notifyHub", {
        skipNegotiation: true,
        transport: signalR.HttpTransportType.WebSockets
    })
    .withAutomaticReconnect()
    .build();
connectionNotifyHub.start()
    .then(() => {
        // connectionNotifyHub.send("UpdateListNotify", token['http://schemas.microsoft.com/ws/2008/06/identity/claims/primarysid'])
    })
    .catch((error) => console.log(error));

const connectionChatHub = new signalR.HubConnectionBuilder()
    .withUrl("http://localhost:62284/chatHub", {
        skipNegotiation: true,
        transport: signalR.HttpTransportType.WebSockets
    })
    .withAutomaticReconnect()
    .build();
connectionChatHub.start()
    .then(() => {
        connectionChatHub.send("UpdateListDirectChat")
    })
    .catch((error) => console.log(error));

export {
    rootPath,
    rootPathImg,
    userName,
    connectionNotifyHub,
    connectionChatHub
}

