import { combineReducers } from 'redux';

const forceUpdateReducers = (state = false, action) => {
    switch (action.type) {
        case 'FORCE_UPDATE':
            return !state;
        default:
            return state;
    }
};

const userInit = null;
const userReducer = (state = userInit, action) => {
    switch (action.type) {
        case 'USER_LOGIN':
            return action.data;
        case 'USER_LOGOUT':
            return null;
        case 'UPDATE_USER':
            return {
                ...state,
                staffInfor: action.data
            };
        default:
            return state;
    }
};

export default combineReducers({
    forceUpdateReducers: forceUpdateReducers,
    userReducer: userReducer
})