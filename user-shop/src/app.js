import React from 'react';
import { connect } from 'react-redux';
import { ConnectHub } from './actions';
import { Route, Switch, Redirect } from 'react-router-dom';
import Home from './components/PageContents/Home';
import Detail from './components/PageContents/Detail';
import Collection from './components/PageContents/Collection';
import Header from './components/Commons/Header';
import Footer from './components/Commons/Footer';
import ScrollAndChat from './components/Commons/ScrollAndChat'
import ScrollTop from './components/Commons/ScrollTop'
import CartPage from './components/PageContents/CartPage/home';
import AccountSetting from './components/PageContents/AccountSetting/home';
import OrderPage from './components/PageContents/OrderPage/home';
import OrderComplete from './components/PageContents/OrderPage/order-complete';
import SearchCollection from './components/PageContents/SearchProducts';
import Login from './components/PageContents/Auth/login/index';
import Register from './components/PageContents/Auth/rigister/index';
import EmailConfirm from './components/PageContents/Auth/email-confirm/index';
import EmailConfirmResult from './components/PageContents/Auth/email-confirm-result/index';
import ForgotPassword from './components/PageContents/Auth/forgot-password/index';
import ResetPassword from './components/PageContents/Auth/reset-password/index';
import ResetPasswordSuccess from './components/PageContents/Auth/reset-password-success/index';
import PrivateRoute from './components/PageContents/Auth/PrivateRoute';
import VNPay from './components/PageContents/VNPay';
import Store from './components/PageContents/StoreSystem';
import AboutUs from './components/PageContents/AboutUs';
import ChangeEmailConfirmResult from 'components/PageContents/Auth/change-email-confirm-result';
import LoginFirebase from 'components/PageContents/Auth/fire-base';

function App() {
    return (
        <>
            <Header />
            <ScrollTop />
            <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/fire-base" exact component={LoginFirebase} />
                <Route path="/vnpay" exact component={VNPay} />
                <Route path="/detail/:slug" exact component={Detail} />
                <Redirect exact from="/some-route/reload/:slug" to="/detail/:slug" />
                <Route path="/collection/:type" component={Collection} />
                <Redirect exact from="/some-route-2/reload/:type" to="/collection/:type" />
                <Route path="/search-collection" component={SearchCollection} />
                <PrivateRoute path="/cart" component={CartPage} />
                <PrivateRoute path="/account-setting" component={AccountSetting} />
                <PrivateRoute path="/order" component={OrderPage} />
                <Route path="/login" component={Login} />
                <Route path="/register" component={Register} />
                <Route path="/confirm-email" component={EmailConfirm} />
                <Route path="/forgot-password" component={ForgotPassword} />
                <Route path="/reset-password/:code/:email" component={ResetPassword} />
                <Route path="/reset-password-success" component={ResetPasswordSuccess} />
                <Route path="/confirm-email-result/:id/:code" component={EmailConfirmResult} />
                <Route path="/confirm-change-email-result/:id/:code/:newEmail" component={ChangeEmailConfirmResult} />
                <Route path="/order-complete" component={OrderComplete} />
                <Route path="/store" component={Store} />
                <Route path="/about-us" component={AboutUs} />
            </Switch>
            <ScrollAndChat />
            <Footer />
        </>
    );
}



const mapDispatchToProps = dispatch => ({
    startConnect: () => dispatch(ConnectHub())
})
export default connect(null, mapDispatchToProps)(App)



// export default App;
