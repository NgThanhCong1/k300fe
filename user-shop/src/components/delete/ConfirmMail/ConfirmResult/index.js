import { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';

function ConfirmEmailResult() {
    let { id, code } = useParams()
    const [result, setResult] = useState('')
    const [isSuccess, setIsSuccess] = useState('')

    useEffect(() => {
        async function ConfirmEmailResult(id, code) {
            let url = "http://localhost:62284/api/User/confirm-email"
            await fetch(url, {
                method: 'POST',
                mode: 'cors',
                cache: 'no-cache',
                credentials: 'same-origin',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${localStorage.access_token}`
                },
                body: JSON.stringify({
                    UserId: id,
                    Code: code
                })
            })
                .then(response => response.json())
                .then(result => {
                    if (result.code == 1) {
                        setResult(result.data)
                        setIsSuccess(result.code)
                    } else {
                        alert(result.message)
                    }
                })
                .catch(error => error.status === 401 ?
                    localStorage.removeItem('access_token')
                    : alert(error.message))
        }
        ConfirmEmailResult(id, code)
    }, [])
    return (
        <div className="main">
            <div className="my-login-page">
                <section className="h-100">
                    <div className="container h-100">
                        <div className="row justify-content-md-center align-items-center h-100">
                            <div className="card-wrapper">
                                <div className="brand">
                                    <img src="img/logo.jpg" alt="bootstrap 4 login page" />
                                </div>
                                <div className="card fat">
                                    <div className="card-body">
                                        <h4 className="card-title">Confirm your mail</h4>
                                        <p>{isSuccess === 1 ?
                                            <>
                                                let login and shopping
                                                <Link to="/">

                                                </Link>
                                            </> : result}</p>
                                    </div>
                                </div>
                                <div className="footer">
                                    Copyright © 2021 — K300
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
}

export default ConfirmEmailResult