import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

function ConfirmEmail() {
    return (
        <div className="grid wide">
            <div className="confirm-mail-warp">
                <h1>Please confirm your Email</h1>
            </div>
        </div>
    );
}

export default ConfirmEmail