import { useParams } from 'react-router-dom';
import { useState } from 'react'

function ResetPassword() {
    const [newPassword, setNewPassword] = useState('')
    let { code, email } = useParams()
    async function handleResetPassword() {
        let url = "http://localhost:62284/api/User/reset-password"
        await fetch(url, {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${localStorage.access_token}`
            },
            body: JSON.stringify({
                code: code,
                email: email,
                newPassword: newPassword
            })
        })
            .then(response => response.json())
            .then(result => {
                if (result.code == 1) {
                    alert(result.message)
                } else {
                    alert(result.message)
                }
            })
            .catch(error => error.status === 401 ?
                localStorage.removeItem('access_token')
                : alert(error.message))
    }

    return (
        <div className="grid wide">
            <div className="confirm-mail-warp">
                <div class="form-group">
                    <label for="newPassword">*newPassword</label>
                    <input
                        type="newPassword"
                        name="newPassword"
                        id="newPassword"
                        onChange={(e) => setNewPassword(e.target.value)}
                    />
                </div>

                <button className="btn btn-primary btn-account-primary btn-accountSetting" onClick={() => handleResetPassword()}>Update</button>
            </div>
        </div>
    );
}

export default ResetPassword