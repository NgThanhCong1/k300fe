import { useState } from "react";

function ForgotPassword() {
    const [email, setEmail] = useState('')
    async function handleForgotPassword() {
        let url = "http://localhost:62284/api/User/forgot-password"
        await fetch(url, {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${localStorage.access_token}`
            },
            body: JSON.stringify({
                email: email
            })
        })
            .then(response => response.json())
            .then(result => {
                if (result.code == 1) {
                    alert(result.message)
                } else {
                    alert(result.message)
                }
            })
            .catch(error => error.status === 401 ?
                localStorage.removeItem('access_token')
                : alert(error.message))
    }

    return (
        <div className="grid wide">
            <div className="confirm-mail-warp">
                <div class="form-group">
                    <label for="email">*email</label>
                    <input
                        type="email"
                        name="email"
                        id="email"
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </div>
                <button className="btn btn-primary btn-account-primary btn-accountSetting" onClick={() => handleForgotPassword()}>Update</button>
            </div>
        </div>
    );
}

export default ForgotPassword