import React from 'react'

const Panigation = function (params) {
    return (
        <div class="panigation">
            <ul class="panigation__list">
                <li class="panigation__item btn">
                    <a href="" class="panigation__link"><i class="ti-angle-double-left"></i></a>
                </li>
                <li class="panigation__item btn">
                    <a href="" class="panigation__link"><i class="ti-angle-left"></i></a>
                </li>
                <li class="panigation__item btn">
                    <a href="" class="panigation__link">1</a>
                </li>
                <li class="panigation__item btn">
                    <a href="" class="panigation__link">2</a>
                </li>
                <li class="panigation__item btn">
                    <a href="" class="panigation__link">3</a>
                </li>
                <li class="panigation__item btn">
                    <a href="" class="panigation__link">4</a>
                </li>
                <li class="panigation__item btn">
                    <a href="" class="panigation__link">....</a>
                </li>
                <li class="panigation__item btn">
                    <a href="" class="panigation__link">10</a>
                </li>
                <li class="panigation__item btn">
                    <a href="" class="panigation__link"><i class="ti-angle-right"></i></a>
                </li>
                <li class="panigation__item btn">
                    <a href="" class="panigation__link"><i class="ti-angle-double-right"></i></a>
                </li>
            </ul>
        </div>
    );
}

export default Panigation