import React from 'react'

function CollectionHeader(props) {
    const { collectionTitle, handelSortChange, handelProductFilterChange } = props
    return (
        <div class="collection-header">
            <span
                style={{
                    flex: "0.5"
                }}
            >
                <label for="filter-check" class="collection-header__filter">Filters<i class="ti-angle-down"></i>
                    <input hidden type="checkbox" name="" id="filter-check" />
                    <div class="filter">
                        <div class="filter__header">
                            <h2 class="filter__header-label">Filter by</h2>
                            <i class="ti-close"></i>
                        </div>
                        <div class="menu-filter">
                            <li class="filter-item">
                                <label for="protype-check" class="btn-dropdown">Price
                                    <i class="ti-close"></i>
                                </label>
                                <input hidden checked type="checkbox" name="" id="protype-check" />
                                <div class="smenu">
                                    <p onClick={() => handelProductFilterChange(-1)} class="smenu__item">All</p>
                                    <p onClick={() => handelProductFilterChange(0)} class="smenu__item">+ 0 - 300</p>
                                    <p onClick={() => handelProductFilterChange(1)} class="smenu__item">+ 300 - 500</p>
                                    <p onClick={() => handelProductFilterChange(2)} class="smenu__item">+ 500 up</p>
                                </div>
                            </li>
                        </div>
                    </div>
                </label>
                <div class="collection-header__sort">
                    <label for="sort" class="sort__option">Sort options <i class="ti-angle-down"></i>
                        <input hidden type="checkbox" name="" id="sort" class="sortcheck" />
                        <ul class="option__list">
                            <li class="option__item" onClick={() => handelSortChange(-1)}>All</li>
                            <li class="option__item" onClick={() => handelSortChange(0)}>Best seller</li>
                            <li class="option__item" onClick={() => handelSortChange(1)}>A-Z</li>
                            <li class="option__item" onClick={() => handelSortChange(2)}>Z-A</li>
                            <li class="option__item" onClick={() => handelSortChange(3)}>Price up</li>
                            <li class="option__item" onClick={() => handelSortChange(4)}>Price down</li>
                        </ul>
                    </label>
                </div>
            </span>
            <h1 class="collection-header__label">{collectionTitle}</h1>
        </div>
    );
}

export default CollectionHeader;