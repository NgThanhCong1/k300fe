import React from 'react';
import ListProduct from "../../Commons/ListProduct";
import Pagination from "react-js-pagination";

function CollectionList(props) {
    const { listProducts, pagination, handlePageChange } = props
    const { _page, _limit, _totalRows } = pagination

    function onPageChange(pageNumber) {
        if (!handlePageChange) return
        handlePageChange(pageNumber);
    }
    return (
        <div class="collection__list">
            <div class="grid-custom product__list">
                {
                    // listProducts.length > 0
                    //     ? <ListProduct
                    //         direction="horizon"
                    //         listProducts={listProducts}
                    //     />
                    //     : <Loading3
                    //         height="60vh"
                    //         spinnerWidth="50px"
                    //     />

                    listProducts.length > 0
                        ? <ListProduct
                            direction="horizon"
                            listProducts={listProducts}
                        />
                        : "No product found"
                }
                {
                    _totalRows > _limit
                    && <div class="panigation">
                        <Pagination
                            activeClass="pagination__item--active"
                            innerClass="panigation__list"
                            itemClass="panigation__item"
                            linkClass="panigation__link"
                            activePage={_page}
                            itemsCountPerPage={_limit}
                            totalItemsCount={_totalRows}
                            pageRangeDisplayed={5}
                            onChange={onPageChange}
                        />
                    </div>
                }
            </div>
        </div>
    );
}


export default CollectionList;