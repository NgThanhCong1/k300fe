import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { useParams } from 'react-router-dom';
import CollectionHeader from './collection-header';
import CollectionList from './collection-list';
import collectionApi from 'api/collectionApi';
import Loading3 from 'components/Commons/Loading3';

function Collection(props) {
    let { type } = useParams();
    const [listProducts, setListProducts] = useState([]);
    const [isFetchComplete, setIsFetchComplete] = useState(false);

    const [filter, setFilter] = useState({
        _page: 1,
        _limit: 8,
        _sortType: -1,
        _priceType: -1
    })

    const [pagination, setPagination] = useState({
        _page: 1,
        _limit: 8,
        _totalRows: 1,
    })

    useEffect(() => {
        document.title = "K300 | Collection page";
        window.scrollTo(0, 0);
    }, []);

    useEffect(() => {
        setIsFetchComplete(false);
        async function getDataFromApi() {
            setListProducts([]);
            try {
                const response = await collectionApi.getProductsByCategory({ ...filter, cateName: type })
                setIsFetchComplete(true);
                if (response.code === 1) {
                    setListProducts(response.data.listCombineProducts)
                    setPagination(response.data.pagination)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [filter, type]);

    function handelSortChange(sortType) {
        setFilter({
            ...filter,
            _sortType: sortType,
        })
    }
    function handelProductFilterChange(priceType) {
        setFilter({
            ...filter,
            _priceType: priceType,
        })
    }
    function handlePageChange(newPage) {
        if (!newPage) return
        setFilter({
            ...filter,
            _page: newPage,
        })
    }

    return (
        <div className="main">
            <div className="wrap-detail-page">
                <CollectionHeader
                    collectionTitle={type}
                    handelProductFilterChange={handelProductFilterChange}
                    handelSortChange={handelSortChange}
                />
                {
                    isFetchComplete
                        ? <CollectionList
                            handlePageChange={handlePageChange}
                            listProducts={listProducts}
                            pagination={pagination}
                        />
                        : <Loading3
                            height="60vh"
                            spinnerWidth="50px"
                        />
                }

            </div>
        </div>
    );
}
const mapStateToProps = (state, ownProps) => {
    return {
        new: state
    }
}
export default connect(mapStateToProps, null)(Collection)

