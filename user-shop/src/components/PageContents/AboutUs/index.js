import { useEffect } from "react";

export default function AboutUs() {
    useEffect(() => {
        document.title = "K300 | About us page";
        window.scrollTo(0, 0)
    }, []);

    return (
        <iframe
            title="cong website"
            src="https://nxgthanhcong.github.io/"
            style={{
                height: "100vh",
                width: "100vw",
                border: "none",
            }}
        // scrolling="no"
        >

        </iframe>
    );
}