import { useEffect } from "react";
import BoxHome from "./home";


function CartPage() {
    useEffect(() => {
        document.title = "K300 | Cart page";
        window.scrollTo(0, 0)
    }, []);

    return (
        <BoxHome />
    )
}

export default CartPage;

