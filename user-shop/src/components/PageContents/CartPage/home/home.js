import {
    addProductToCartDB,
    removeProductInCartDB,
    setCheckProductInCartDB
} from 'actions';
import { ButtonPrimary } from 'components/Commons/Button';
import { toVNCurency } from 'helpers';
import { useDispatch, useSelector } from 'react-redux';
import { Table } from 'reactstrap';
import { Link } from 'react-router-dom';

export default function BoxHome() {
    const listProductsInCart = useSelector(state => state.cartDBReducer);
    const dispatch = useDispatch();

    function handleUpdateQuantity(item, quantity) {
        item.cartQuantity = quantity
        dispatch(addProductToCartDB(item))
    }

    function setCheckProduct(item) {
        dispatch(setCheckProductInCartDB(item))
    }

    let total = 0;
    return (
        <div className="main">
            <div className="grid-custom wide">
                <div className="wrap-cart-page row">
                    <div className="col-md-8 box-cart-left">
                        <h2>Shopping Cart</h2>
                        <Table
                            className="cart-table"
                            hover
                        >
                            <thead>
                                <tr>
                                    <th>PRODUCT DETAIL</th>
                                    <th>QUANTITY</th>
                                    <th>TOTAL</th>
                                    <th>CHECK</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    listProductsInCart.map((item) => (
                                        <>
                                            <tr>
                                                <td>
                                                    <Link
                                                        to={`/some-route/reload/${item.slug}`}
                                                        style={{
                                                            color: "#000",
                                                            display: "flex"
                                                        }}
                                                    >
                                                        <img
                                                            className="box-info__img"
                                                            src={`${process.env.REACT_APP_IMAGE_URL}${item.listImagesLink[0]}`}
                                                            alt="alt here"
                                                        />
                                                        <div className="box-info__text">
                                                            <h3>Name: {item.name}</h3>
                                                            <h4>Color: {item.color}</h4>
                                                            <h4>Size: {item.size}</h4>
                                                            <h4>Price: {toVNCurency(item.price)}</h4>
                                                        </div>
                                                    </Link>

                                                </td>
                                                <td
                                                    style={{
                                                        verticalAlign: "middle"
                                                    }}
                                                >
                                                    <div
                                                        style={{
                                                            transform: "translateY(30%)"
                                                        }}
                                                    >
                                                        <ul class="dproduct__quantity-list">
                                                            <li
                                                                style={{ cursor: "pointer" }}
                                                                class={item.cartQuantity > 1 ? "dproduct__quantity-item" : "dproduct__quantity-item noClick"}
                                                                onClick={() => handleUpdateQuantity(item, -1)}
                                                            >
                                                                -
                                                            </li>
                                                            <li class="dproduct__quantity-item">{item.cartQuantity}</li>
                                                            <li
                                                                style={{ cursor: "pointer" }}
                                                                class="dproduct__quantity-item"
                                                                onClick={() => handleUpdateQuantity(item, 1)}
                                                            >
                                                                +
                                                            </li>
                                                        </ul>
                                                        <i
                                                            class="fas fa-trash-alt"
                                                            style={{
                                                                fontSize: "20px",
                                                                marginTop: "10px",
                                                                cursor: "pointer"
                                                            }}
                                                            onClick={() => dispatch(removeProductInCartDB(item))}
                                                        >
                                                        </i>
                                                    </div>

                                                </td>
                                                <td
                                                    style={{
                                                        verticalAlign: "middle"
                                                    }}
                                                >
                                                    <h4>
                                                        {toVNCurency((parseInt(item.price) * parseInt(item.cartQuantity)))}
                                                    </h4>
                                                </td>
                                                <td
                                                    style={{
                                                        verticalAlign: "middle"
                                                    }}
                                                >
                                                    {
                                                        item.isChecked !== 1
                                                            ? <i
                                                                class="fas fa-times"
                                                                style={{
                                                                    fontSize: "20px",
                                                                    cursor: "pointer"
                                                                }}
                                                                onClick={() => setCheckProduct(item)}
                                                            >
                                                            </i>
                                                            : <i
                                                                class="fas fa-check"
                                                                style={{
                                                                    fontSize: "20px",
                                                                    cursor: "pointer"
                                                                }}
                                                                onClick={() => setCheckProduct(item)}
                                                            >

                                                            </i>
                                                    }
                                                </td>
                                            </tr>
                                            <span hidden>{total += item.price * item.cartQuantity}</span>
                                        </>
                                    ))
                                }

                            </tbody>
                        </Table>
                    </div>
                    <div className="col-md-4 box-cart-right">
                        <div className="box-cart-right-warp">
                            <div
                                className="box-cart-right-warp"
                            >
                                <h3
                                    style={{ margin: "10px 0" }}
                                >
                                    ORDER SUMMARY
                                </h3>
                                <p style={{ margin: "10px 0", display: "flex", justifyContent: "space-between" }}>
                                    <h4>ITEMS: {listProductsInCart.length}</h4>
                                    <h4> {toVNCurency(total)}</h4>
                                </p>
                                <p style={{ margin: "10px 0", display: "flex", justifyContent: "space-between" }}>
                                    <h4>TOTAL COST: </h4>
                                    <h4>{toVNCurency(total)}</h4>
                                </p>
                                <ButtonPrimary
                                    text="Checkout"
                                    linkTo="/order"
                                    width="100%"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}