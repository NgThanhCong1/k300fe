import authApi from 'api/authApi';
import React from 'react';
import BoxForgotPassword from './forgot-password';

function ForgotPassword() {

    async function forgotPassword(email) {
        try {
            const response = await authApi.forgotPassword({
                email: email
            });
            if (response.code === 1) {
                alert(response.message)
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }


    return (
        <BoxForgotPassword
            forgotPassword={forgotPassword}
        />
    );
}

export default ForgotPassword;