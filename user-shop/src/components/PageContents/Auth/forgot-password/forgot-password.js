import { useForm } from "react-hook-form";

export default function BoxForgotPassword(props) {
    const { forgotPassword } = props
    const { register, handleSubmit, formState: { errors } } = useForm({

    });

    const onSubmit = async data => {
        forgotPassword(data.email)
    }

    return (
        <div className="main">
            <div className="my-login-page">
                <section className="h-100">
                    <div className="container h-100">
                        <div className="row justify-content-md-center align-items-center h-100">
                            <div className="card-wrapper">
                                <div className="brand">
                                    <img src="img/logo.jpg" alt="bootstrap 4 login page" />
                                </div>
                                <div className="card fat">
                                    <div className="card-body">
                                        <h4 className="card-title">Forgot Password</h4>
                                        <form className="my-login-validation" noValidate
                                            onSubmit={handleSubmit(onSubmit)}
                                        >
                                            <div className="form-group">
                                                <label htmlFor="email">E-Mail Address</label>
                                                <input
                                                    id="email"
                                                    type="email"
                                                    className="form-control"
                                                    name="email" autofocus
                                                    {...register('email', { required: true })}
                                                />
                                                {errors.email && <span style={{ color: "red" }}>This field is required</span>}
                                                <div className="form-text text-muted">
                                                    By clicking "Reset Password" we will send a password reset link
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <button type="submit" className="btn btn-primary btn-block">
                                                    Reset Password
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div className="footer">
                                    Copyright © 2017 — Your Company
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
}