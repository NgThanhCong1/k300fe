function EmailConfirm() {
    return (
        <div className="main">
            <div className="my-login-page">
                <section className="h-100">
                    <div className="container h-100">
                        <div className="row justify-content-md-center align-items-center h-100">
                            <div className="card-wrapper"
                            >
                                <div className="brand">
                                    <img src="./asset/img/logo.jpg" alt="bootstrap 4 login page" />
                                </div>
                                <div className="card fat"
                                    style={{
                                        width: "100%"
                                    }}
                                >
                                    <div className="card-body">
                                        <h4 className="card-title">Confirm your mail</h4>
                                        <p>Confirm link is Send to your mail, please check it</p>
                                    </div>
                                </div>
                                <div className="footer">
                                    Copyright © 2021 — K300
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
}

export default EmailConfirm;