import CustomAlert from "components/Commons/Alert";
import WaitButton from "components/Commons/WaitButton";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { Link } from 'react-router-dom';

export default function BoxRegister(props) {
    const { alertInfor, registerF } = props
    const { register, handleSubmit, formState: { errors } } = useForm({

    });
    const [isOfSubmitBtn, setIsOffSubmitBtn] = useState(false);

    const onSubmit = async data => {
        setIsOffSubmitBtn(true);
        await registerF({
            username: data.username,
            password: data.password,
            firstName: data.firstName,
            lastName: data.lastName,
        })
        setIsOffSubmitBtn(false);
    }

    return (
        <div className="main">
            <div className="my-login-page">
                <section className="h-100">
                    <CustomAlert
                        {...alertInfor}
                    />
                    <div className="container h-100">
                        <div className="row justify-content-md-center h-100">
                            <div className="card-wrapper">
                                <div className="brand">
                                    <img src="./asset/img/logo.jpg" alt="bootstrap 4 login page" />
                                </div>
                                <div className="card fat">
                                    <div className="card-body">
                                        <h4 className="card-title">Register</h4>
                                        <form className="my-login-validation" onSubmit={handleSubmit(onSubmit)}>
                                            <div style={{ display: "flex", justifyContent: "space-between" }}>

                                                <div className="form-group">
                                                    <label htmlFor="firstName">* FirstName</label>
                                                    <input
                                                        id="firstName"
                                                        type="firstName"
                                                        className="form-control"
                                                        name="firstName" autofocus
                                                        {...register('firstName', { required: true })}
                                                    />
                                                    {errors.firstName && <span style={{ color: "red" }}>This field is required</span>}
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="lastName">* LastName</label>
                                                    <input
                                                        id="lastName"
                                                        type="lastName"
                                                        className="form-control"
                                                        name="lastName" autofocus
                                                        {...register('lastName', { required: true })}
                                                    />
                                                    {errors.lastName && <span style={{ color: "red" }}>This field is required</span>}
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="username">* Email</label>
                                                <input
                                                    id="username"
                                                    type="username"
                                                    className="form-control"
                                                    name="username" autofocus
                                                    {...register('username', { required: true })}
                                                />
                                                {errors.username && <span style={{ color: "red" }}>This field is required</span>}
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="password">* Password</label>
                                                <input
                                                    id="password"
                                                    type="password"
                                                    className="form-control"
                                                    name="password" autofocus
                                                    {...register('password', { required: true })}
                                                />
                                                {errors.password && <span style={{ color: "red" }}>This field is required</span>}
                                            </div>
                                            <div className="form-group">
                                                <div className="custom-checkbox custom-control">
                                                    <input type="checkbox" name="agree" id="agree" className="custom-control-input" required />
                                                    <label htmlFor="agree" className="custom-control-label">I agree to the <a href="/">Terms and Conditions</a></label>
                                                    <div className="invalid-feedback">
                                                        You must agree with our Terms and Conditions
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                {/* <button type="submit" className="btn btn-primary btn-block">
                                                    Register
                                                </button> */}
                                                <WaitButton
                                                    type="submit"
                                                    className="btn btn-primary btn-block"
                                                    btnText="Register"
                                                    isOff={isOfSubmitBtn}
                                                />
                                            </div>
                                            <div className="mt-4 text-center">
                                                Already have an account? <Link to="/login">Login</Link>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div className="footer">
                                    Copyright © 2017 — Your Company
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
}