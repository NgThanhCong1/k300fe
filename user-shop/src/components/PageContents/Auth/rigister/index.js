import authApi from "api/authApi";
import React, { useState } from "react";
import { Redirect } from 'react-router-dom';
import BoxRegister from "./register";

function Login() {
    const [redirectToReferrer, setRedirectToReferrer] = useState(false)
    const [alertInfor, setAlertInfor] = useState({
        type: 0,
        message: ''
    })

    function redirectTo() {
        return <Redirect to="/confirm-email" />;
    }

    async function registerF(user) {
        try {
            const response = await authApi.createCustommer({
                UserName: user.username,
                Password: user.password,
                FirstName: user.firstName,
                LastName: user.lastName,
                Email: user.username
            });
            if (response.code === 1) {
                setRedirectToReferrer(true)
            } else {
                setAlertInfor({
                    type: -1,
                    message: response.message
                })
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    if (redirectToReferrer === true) return redirectTo();
    return (
        <BoxRegister
            registerF={registerF}
            alertInfor={alertInfor}
        />
    );
}

export default Login;