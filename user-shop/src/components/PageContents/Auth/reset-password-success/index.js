import '../../OrderPage/order-complete/order-complete.css';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { logoutAction } from 'actions';

function ResetPasswordSuccess() {
    const dispath = useDispatch();
    useEffect(() => {
        dispath(logoutAction());
    });
    return (
        <div className="main">
            <div className="order-complete-card">
                <div style={{ borderRadius: '200px', height: '200px', width: '200px', background: '#F8FAF5', margin: '0 auto' }}>
                    <i className="checkmark">✓</i>
                </div>
                <h1>Success</h1>
                <p>Congratulation you change success;<br /> Please login to Countinute</p>
                <Link
                    style={{ marginTop: "20px", display: "block" }}
                    to="/login"
                >
                    <Button color="dark">
                        Go Login
                    </Button>
                </Link>
            </div>
        </div>
    );
}

export default ResetPasswordSuccess;