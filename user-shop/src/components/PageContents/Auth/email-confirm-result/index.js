import authApi from 'api/authApi';
import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import BoxEmailConfirmResult from './email-confirm-result';

function EmailConfirmResult() {
    let { id, code } = useParams()
    const [result, setResult] = useState('')
    const [isSuccess, setIsSuccess] = useState('')

    useEffect(() => {
        async function EmailConfirmResult(id, code) {
            try {
                const response = await authApi.confirmMail({
                    UserId: id,
                    Code: code
                });
                if (response.code === 1) {
                    setResult(response.data)
                    setIsSuccess(response.code)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        EmailConfirmResult(id, code)
    }, [code, id])

    return (
        <BoxEmailConfirmResult
            isSuccess={isSuccess}
            result={result}
        />
    );
}

export default EmailConfirmResult