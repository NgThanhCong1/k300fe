import { useForm } from "react-hook-form";
import React, { useState } from "react";
import { Link } from 'react-router-dom';
import WaitButton from "components/Commons/WaitButton";
import CustomAlert from "components/Commons/Alert";

export default function BoxLogin(props) {
    const { alertInfor, login } = props;
    const { register, handleSubmit, formState: { errors } } = useForm({

    });
    const [isOfSubmitBtn, setIsOffSubmitBtn] = useState(false);

    const onSubmit = async data => {
        setIsOffSubmitBtn(true);
        await login({
            username: data.email,
            password: data.password
        })
        setIsOffSubmitBtn(false);
    }

    return (
        <div className="main">
            <div className="my-login-page">
                <section className="h-100">
                    <CustomAlert
                        {...alertInfor}
                    />
                    <div className="container h-100">
                        <div className="row justify-content-md-center h-100">
                            <div className="card-wrapper">
                                <div className="brand">
                                    <img src="./asset/img/logo.jpg" alt="logo" />
                                </div>
                                <div className="card fat"
                                    style={{
                                        width: "100%"
                                    }}
                                >
                                    <div className="card-body">
                                        <h4 className="card-title">Login</h4>
                                        {/* <span style={{ color: "red" }}>errorMessagesdddddddddddddddd</span> */}

                                        <form className="my-login-validation" onSubmit={handleSubmit(onSubmit)}>
                                            <div className="form-group">
                                                <label htmlFor="Email">Email</label>
                                                <input
                                                    id="Email"
                                                    type="Email"
                                                    className="form-control"
                                                    name="Email" autofocus
                                                    {...register('email', { required: true })}
                                                />
                                                {errors.email && <span style={{ color: "red" }}>This field is required</span>}
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="password">Password
                                                    <Link
                                                        to="/forgot-password"
                                                        className="float-right">
                                                        Forgot Password?
                                                    </Link>
                                                </label>
                                                <input
                                                    id="password"
                                                    type="password"
                                                    className="form-control"
                                                    name="password" data-eye
                                                    {...register('password', { required: true })}
                                                />
                                                {errors.password && <span style={{ color: "red" }}>This field is required</span>}
                                            </div>
                                            <div className="form-group">
                                                <div className="custom-checkbox custom-control">
                                                    <input type="checkbox" name="remember" id="remember" className="custom-control-input" />
                                                    <label htmlFor="remember" className="custom-control-label">Remember Me</label>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <WaitButton
                                                    type="submit"
                                                    className="btn btn-primary btn-block"
                                                    btnText="Login"
                                                    isOff={isOfSubmitBtn}
                                                />
                                                {/* <button type="submit" className="btn btn-primary btn-block">
                                                    Login
                                                </button> */}
                                            </div>
                                            <div className="mt-4 text-center">
                                                Don't have an account? <Link to="/register">Create One</Link>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div className="footer">
                                    Copyright © 2021 — K300
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
}