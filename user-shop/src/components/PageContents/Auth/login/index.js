import React, { useState } from "react";
import { Redirect, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { loginAction, mergeProductsInCartDB } from 'actions';
import BoxLogin from "./login";
import authApi from "api/authApi";

function Login(props) {
    const { from } = props.location.state || { from: { pathname: '/' } }
    const history = useHistory();
    const dispath = useDispatch();
    const selecter = useSelector(state => state);
    const userInfor = selecter.userReducer;
    const [alertInfor, setAlertInfor] = useState({
        type: 0,
        message: ''
    })

    // useEffect(() => {
    //     dispath(mergeProductsInCartDB(response.data.user.id));
    // })

    async function login(user) {
        try {
            const response = await authApi.login({
                userName: user.username,
                password: user.password
            });
            if (response.code === 1) {
                await dispath(loginAction({
                    ...response.data.user,
                    token: response.data.jwtInfor.accessToken
                }));
                dispath(mergeProductsInCartDB(response.data.user.id));
                history.push(from.pathname);
            } else {
                setAlertInfor({
                    type: -1,
                    message: response.message
                })
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    return (
        userInfor
            ? <Redirect to={from} />
            : <BoxLogin
                login={login}
                alertInfor={alertInfor}
            />
    );
}

export default Login;

