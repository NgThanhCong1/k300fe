// Import the functions you need from the SDKs you need
import firebase from "firebase";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCSU0K0yX0OatlPCLVbWiTQ-OHsFSg4ubs",
    authDomain: "auth-3ed1a.firebaseapp.com",
    projectId: "auth-3ed1a",
    storageBucket: "auth-3ed1a.appspot.com",
    messagingSenderId: "592462121561",
    appId: "1:592462121561:web:423fb080ade9687537ff48"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;
