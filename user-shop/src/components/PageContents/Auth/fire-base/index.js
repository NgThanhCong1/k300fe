
import React, { useState } from 'react';
import firebase from './firebase';


export default function LoginFirebase() {
    function handleClick() {
        let recapcha = new firebase.auth.RecaptchaVerifier('recaptcha-container');
        let number = "+84396399123";
        firebase.auth().signInWithPhoneNumber(number, recapcha).then(function (e) {
            let code = prompt('enter otp', '');
            if (code == null) return;
            e.confirm(code).then(result => {
                document.querySelector('#firebase').textContent = result.user.phoneNumber + " Number verified"
            })
                .catch(error => console.log(error))
        })
    }
    return (
        <div>
            <div id="recaptcha-container"></div>
            <h1 id="firebase"></h1>
            <button
                onClick={() => handleClick()}
            >
                click here
            </button>
        </div>
    );
}