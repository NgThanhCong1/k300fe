import { logoutAction } from 'actions';
import authApi from 'api/authApi';
import { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import BoxEmailConfirmResult from './change-email-confirm-result';

export default function ChangeEmailConfirmResult() {
    let { id, code, newEmail } = useParams();
    const [result, setResult] = useState('');
    const [isSuccess, setIsSuccess] = useState('');
    const dispatch = useDispatch();

    useEffect(() => {
        async function EmailConfirmResult() {
            try {
                const response = await authApi.confirmChangeMail({
                    UserId: id,
                    Code: code,
                    newEmail: newEmail
                });
                if (response.code === 1) {
                    setResult(response.data)
                    setIsSuccess(response.code)
                    dispatch(logoutAction());
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        EmailConfirmResult()
    }, [dispatch, code, id, newEmail])

    return (
        <BoxEmailConfirmResult
            isSuccess={isSuccess}
            result={result}
        />
    );
}
