export default function BoxEmailConfirmResult(props) {
    const { isSuccess, result } = props
    return (
        <div className="grid wide">
            <div className="confirm-mail-warp">
                <h1>{isSuccess === 1 ? `${result}. let login and shopping` : result}</h1>
            </div>
        </div>
    );
}