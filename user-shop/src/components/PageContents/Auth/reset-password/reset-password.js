import { useForm } from "react-hook-form";
import React from 'react';

export default function BoxResetPassword(props) {
    const { resetPassword } = props
    const { register, handleSubmit, formState: { errors } } = useForm({

    });

    const onSubmit = async data => {
        resetPassword(data.newPassword)
    }

    return (
        <div className="main">
            <div className="my-login-page">
                <section className="h-100">
                    <div className="container h-100">
                        <div className="row justify-content-md-center align-items-center h-100">
                            <div className="card-wrapper">
                                <div className="brand">
                                    <img src="./asset/img/logo.jpg" alt="bootstrap 4 login page" />
                                </div>
                                <div className="card fat">
                                    <div className="card-body">
                                        <h4 className="card-title">Reset Password</h4>
                                        <form className="my-login-validation" noValidate
                                            onSubmit={handleSubmit(onSubmit)}
                                        >
                                            <div className="form-group">
                                                <label htmlFor="new-password">New Password</label>
                                                <input
                                                    id="new-password"
                                                    type="password"
                                                    className="form-control"
                                                    name="password" autofocus data-eye
                                                    {...register('newPassword', { required: true })}
                                                />
                                                {errors.newPassword && <span style={{ color: "red" }}>This field is required</span>}

                                                <div className="form-text text-muted">
                                                    Make sure your password is strong and easy to remember
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <button type="submit" className="btn btn-primary btn-block">
                                                    Reset Password
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div className="footer">
                                    Copyright © 2017 — Your Company
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
}