import authApi from 'api/authApi';
import React, { useState } from 'react';
import { useParams, Redirect } from 'react-router-dom';
import BoxResetPassword from './reset-password';

function ResetPassword() {
    const [redirectToReferrer, setRedirectToReferrer] = useState(false)
    let { code, email } = useParams()

    function redirectTo() {
        return <Redirect to="/reset-password-success" />;
    }
    if (redirectToReferrer === true) return redirectTo();

    async function resetPassword(newPassword) {
        try {
            const response = await authApi.resetPassword({
                code: code,
                email: email,
                newPassword: newPassword
            });
            if (response.code === 1) {
                setRedirectToReferrer(true);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    return (
        <BoxResetPassword
            resetPassword={resetPassword}
        />
    );
}

export default ResetPassword;