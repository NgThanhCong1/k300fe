import React, { useState } from 'react';
import SwiperCore, { Navigation, Pagination, Autoplay, Thumbs } from 'swiper/core';
import { Swiper, SwiperSlide } from 'swiper/react';
import { rootPath } from "constants/root-server"
import { useDispatch } from 'react-redux';
import { addProductToCartDB } from 'actions';

import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import 'swiper/components/scrollbar/scrollbar.scss';
import {
    FacebookIcon,
    FacebookShareButton,
    TelegramIcon,
    TelegramShareButton
} from 'react-share';

import { toVNCurency } from 'helpers';
import CustomAlert from 'components/Commons/Alert';
SwiperCore.use([Navigation, Pagination, Autoplay, Thumbs]);

function DetailTop(props) {
    const dispath = useDispatch();
    const {
        ratingStatic,
        listProductDetails,
        listColors,
        alertInfor
    } = props;

    const [currentProductDetail, setCurrentProductDetail] = useState(listProductDetails[0]);
    const [buyQuantity, setBuyQuantity] = useState(1);

    let classNameSize = 'dproduct__size-item'
    let classNameSizeNoClick = 'dproduct__size-item noClick'
    let classNameSizeActive = 'dproduct__size-item dproduct__size-item--active'
    let classNameColor = 'dproduct__color-item'
    let classNameColorActive = 'dproduct__color-item dproduct__color-item--active'

    const [thumbsSwiper, setThumbsSwiper] = useState(null);

    function addToCartDB() {
        dispath(addProductToCartDB({
            ...currentProductDetail,
            cartQuantity: buyQuantity,
            isCheck: 0
        }))
    }

    function handleCurrentProductDetailChange(item, type) {
        if (type === "size") {
            const activeProduct = listProductDetails.filter(pro =>
                pro.size === item
                && pro.color === currentProductDetail.color
            )[0];
            setCurrentProductDetail(activeProduct);
        }
        if (type === "color") {
            let activeProduct = null;
            if (listProductDetails.filter(pro => pro.color === item && pro.quantity > 0).length > 0) {
                activeProduct = listProductDetails.filter(pro => pro.color === item && pro.quantity > 0)[0];
            } else {
                activeProduct = listProductDetails.filter(pro => pro.color === item)[0];
            }
            setCurrentProductDetail(activeProduct);
            setBuyQuantity(1);
        }
    }

    return (
        <div class="detail-top">
            <div class="grid-custom  wide detail-top__wide">
                <CustomAlert
                    {...alertInfor}
                />
                <div className="row">
                    <div class="col-md-5">
                        {
                            <>
                                <Swiper
                                    style={{ '--swiper-navigation-color': '#fff', '--swiper-pagination-color': '#fff' }}
                                    loop={true}
                                    spaceBetween={10}
                                    navigation={true}
                                    thumbs={{ swiper: thumbsSwiper }}
                                    className="mySwiper2"
                                >
                                    {
                                        currentProductDetail.listImagesLink.map((item) => (
                                            <SwiperSlide>
                                                <img
                                                    src={`${rootPath}${item}`}
                                                    alt="alt here"
                                                    width="100%"
                                                />
                                            </SwiperSlide>

                                        ))
                                    }
                                </Swiper>
                                <Swiper
                                    onSwiper={setThumbsSwiper}
                                    loop={true}
                                    spaceBetween={10}
                                    slidesPerView={4}
                                    freeMode={true}
                                    watchSlidesProgress={true}
                                    className="mySwiper"
                                >
                                    {
                                        currentProductDetail.listImagesLink.map((item) => (
                                            <SwiperSlide>
                                                <img
                                                    src={`${rootPath}${item}`}
                                                    alt="alt here"
                                                    width="100%"
                                                />
                                            </SwiperSlide>

                                        ))
                                    }
                                </Swiper>
                            </>
                        }
                    </div>
                    <div className="col-md-1"></div>
                    <div class="col-md-5">
                        <div class="dproduct">
                            <h1
                                class="dproduct__name x-fw700"
                            >
                                {currentProductDetail.name}
                            </h1>
                            <p
                                class="dproduct__price x-fw600"
                            >
                                Pirce: {toVNCurency(currentProductDetail.price)}
                            </p>
                            <div>
                                <div class="dproduct__brand-cate-code">
                                    <p class="dproduct__rate">
                                        <span
                                            className="x-fw600"
                                            style={{ fontSize: "12px", marginRight: "4px" }}
                                        >
                                            Rating:
                                        </span>
                                        {(ratingStatic.totalRatingPoint / ratingStatic.totalRatingCount).toFixed(1)}
                                        <i className="ti-star"></i>
                                        / {ratingStatic.totalRatingCount} rating
                                    </p>
                                    <p class="dproduct__brand">
                                        <span
                                            className="x-fw600"
                                            style={{ fontSize: "12px", marginRight: "4px" }}
                                        >
                                            Brand:
                                        </span>
                                        {currentProductDetail.brandName}
                                    </p>
                                    <p class="dproduct__code">
                                        <span
                                            className="x-fw600"
                                            style={{ fontSize: "12px", marginRight: "4px" }}
                                        >
                                            Product code:
                                        </span>
                                        {currentProductDetail.productCode}
                                    </p>
                                </div>

                            </div>

                            <div
                                class="dproduct__color"
                                style={{
                                    alignItems: "center"
                                }}
                            >
                                <h2
                                    class="dproduct__color-label x-fw600"
                                    style={{
                                        margin: 0
                                    }}
                                >
                                    COLOR:
                                </h2>
                                {
                                    listColors.map(item => (
                                        <span
                                            style={{
                                                cursor: "pointer",
                                                background: item,
                                                borderRadius: "50%",
                                            }}
                                            onClick={() => { handleCurrentProductDetailChange(item, "color") }}
                                            className={
                                                currentProductDetail.color === item
                                                    ? classNameColorActive
                                                    : classNameColor
                                            }
                                        >
                                        </span>
                                    ))
                                }
                            </div>

                            <div class="dproduct__size">
                                <h2 class="dproduct__size-label x-fw600">SIZE: </h2>
                                <ul class="dproduct__size-list">
                                    {
                                        listProductDetails.filter(pro => pro.color === currentProductDetail.color).map(item => (
                                            <li
                                                style={{
                                                    cursor: item.quantity > 0 ? 'pointer' : 'no-drop',
                                                }}
                                                onClick={() => { handleCurrentProductDetailChange(item.size, "size") }}
                                                className={item.quantity > 0
                                                    ? currentProductDetail.size === item.size
                                                        ? classNameSizeActive
                                                        : classNameSize
                                                    : classNameSizeNoClick}
                                            >
                                                {item.size}
                                            </li>
                                        ))
                                    }
                                </ul>
                            </div>


                            <div class="dproduct__quantity">
                                <h2 class="dproduct__quantity-label x-fw600">Buy quantity: </h2>
                                <ul class="dproduct__quantity-list">
                                    <li
                                        style={{ cursor: "pointer" }}
                                        class={(currentProductDetail.quantity > 0 && buyQuantity > 1) ? "dproduct__quantity-item" : "dproduct__quantity-item noClick"}
                                        onClick={() => setBuyQuantity(buyQuantity - 1)}
                                    >
                                        -
                                    </li>
                                    <li
                                        class={currentProductDetail.quantity > 0 ? "dproduct__quantity-item" : "dproduct__quantity-item noClick"}
                                    >
                                        {buyQuantity}
                                    </li>
                                    <li
                                        style={{ cursor: "pointer" }}
                                        class={currentProductDetail.quantity > 0 ? "dproduct__quantity-item" : "dproduct__quantity-item noClick"}
                                        onClick={() => setBuyQuantity(buyQuantity + 1)}
                                    >
                                        +
                                    </li>
                                </ul>
                            </div>

                            <button
                                class="dproduct__add-to-cart btn--primary"
                                onClick={() => addToCartDB()}
                                style={{
                                    opacity: currentProductDetail.quantity > 0 ? 1 : 0.5,
                                    outline: "none"
                                }}
                                disabled={currentProductDetail.quantity <= 0}
                            >
                                {
                                    currentProductDetail.quantity <= 0
                                        ? <>
                                            <i
                                                class="far fa-frown"
                                                style={{
                                                    fontSize: "15px"
                                                }}
                                            >
                                            </i>
                                            &nbsp;&nbsp;Be soon
                                        </>
                                        : <>
                                            <i
                                                class="fas fa-shopping-cart"
                                                style={{
                                                    fontSize: "15px"
                                                }}
                                            >
                                            </i>
                                            &nbsp;&nbsp;Add to cart
                                        </>
                                }
                            </button>

                            <div
                                style={{
                                    marginTop: "50px",
                                    alignItems: "center"
                                }}
                                class="dproduct__quantity"
                            >
                                <h2
                                    class="dproduct__quantity-label x-fw600"
                                    style={{
                                        margin: 0
                                    }}
                                >Share to social: </h2>
                                <ul class="dproduct__quantity-list">
                                    <FacebookShareButton
                                        url={window.location.href}
                                        quote={"View our product"}
                                        className="dproduct__size-item"
                                    >
                                        <FacebookIcon size={32} round />
                                    </FacebookShareButton>
                                    <TelegramShareButton
                                        url={window.location.href}
                                        quote={"View our product"}
                                        className="dproduct__size-item"
                                    >
                                        <TelegramIcon size={32} round />
                                    </TelegramShareButton>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default DetailTop;