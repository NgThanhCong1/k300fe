import React from 'react';
import ListProduct from '../../Commons/ListProduct';

function DetailRelate(props) {
    const { listRelatedProducts } = props
    return (
        <div>
            <h1 className="x-fw700">Related products</h1>
            <div class="grid-custom wide relate-wrap">
                <ListProduct direction="vertical" listProducts={listRelatedProducts} />
            </div>
        </div>
    );
}

export default DetailRelate;