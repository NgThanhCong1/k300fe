import React from 'react';
import { useSelector } from 'react-redux';
import ListProduct from '../../Commons/ListProduct';

function DetailSaw(props) {
    const listProductSaws = useSelector(state => state.productSawReducer).listProducts;
    return (
        listProductSaws && listProductSaws.length > 0 && <div class="detail-saw"
            style={{ marginBottom: "40px" }}
        >
            <h1 className="x-fw700">
                Viewed products
            </h1>
            <div class="grid-custom wide">
                <ListProduct
                    direction="horizon"
                    listProducts={listProductSaws}
                />
            </div>
        </div>
    );
}

export default DetailSaw;