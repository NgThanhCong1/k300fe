import { useState } from 'react';
import { rootPath } from 'constants/root-server';
import Pagination from "react-js-pagination";
import EditCommentConten from '../edit-content-comment';
import { RatingComment } from 'components/Commons/Rating';
import { useSelector } from 'react-redux';

export default function BoxDetailComment(props) {
    const { ratingStatic,
        addRating,
        bannedWords,
        listComments,
        pagination,
        handlePageChange,
        deleteComment,
        updateComment,
        addComment } = props
    const { _page,
        _limit,
        _totalRows } = pagination
    const [activePage, setActivePage] = useState(_page)
    function onPageChange(pageNumber) {
        if (!handlePageChange) return
        setActivePage(pageNumber)
        handlePageChange(pageNumber)
    }

    const userInfor = useSelector(state => state.userReducer);

    const [content, setContent] = useState('')
    const [isDisplayContent, setIsDisplayContent] = useState('')

    return (
        <div class="grid-custom  wide detail-top__wide">
            <div className="container" style={{ background: "#fff", padding: "40px" }}>
                <div className="row">
                    {
                        userInfor &&
                        <>
                            <div class="input-group mb-3"
                                style={{
                                    zIndex: "1"
                                }}
                            >
                                <img
                                    className="d-flex g-width-50 g-height-50 rounded-circle g-mt-3 g-mr-15"
                                    src={userInfor ? `${rootPath}${userInfor.avatar}` : `${rootPath}/Images/Users/user.png`}
                                    alt="alt Description"
                                    style={{ objectFit: "contain" }}
                                    width="50px"
                                    height="50px"
                                />
                                <input
                                    type="text" class="form-control"
                                    placeholder="your comment here..."
                                    style={{ height: "40px" }}
                                    value={content}
                                    onChange={(e) => setContent(e.target.value)}
                                />
                                <div class="input-group-append">
                                    <button
                                        class="btn--primary"
                                        type="button"
                                        onClick={() => addComment({ content: content })}
                                    >
                                        Comment
                                    </button>
                                </div>
                            </div>
                            <RatingComment
                                ratingStatic={ratingStatic}
                                addRating={addRating}
                            />
                        </>
                    }

                    {
                        listComments.map(item => (
                            <div
                                class="card card-white post"
                                style={{
                                    width: "100%",
                                    marginBottom: "20px"
                                }}
                            >
                                <div class="post-heading">
                                    <div class="float-left image">
                                        <img
                                            src={item.customerAvatar
                                                ? `${rootPath}${item.customerAvatar}`
                                                : `${rootPath}/Images/Users/user.png`
                                            }
                                            class="img-circle avatar"
                                            alt="..."
                                            style={{ objectFit: "contain" }}
                                        />
                                    </div>
                                    <div class="float-left meta">
                                        <div class="title h6">
                                            <b>
                                                {item.customerName}
                                            </b>
                                            &nbsp;made a post.
                                        </div>
                                        <span class="text-muted time">
                                            {item.createdAt.split('T')[0]}
                                        </span>
                                    </div>
                                </div>
                                <div class="post-description">
                                    {
                                        isDisplayContent === item.id
                                            ?
                                            <EditCommentConten
                                                isDisplayContent={isDisplayContent}
                                                setIsDisplayContent={setIsDisplayContent}
                                                item={item}
                                                updateComment={updateComment}
                                                style={{ fontWeight: "600" }}
                                                editContent={item.content}
                                            />
                                            :
                                            <p
                                                style={{
                                                    fontSize: "16px"
                                                }}
                                            >
                                                {item.content.split(" ").map((word => (
                                                    bannedWords.includes(word)
                                                        ? '*** '
                                                        : word + ' '
                                                )))}
                                            </p>
                                    }
                                    <ul className="list-inline d-sm-flex my-0">
                                        {
                                            userInfor && userInfor.id === item.customerId &&
                                            <>
                                                <i
                                                    class="fas fa-edit"
                                                    onClick={() => { setIsDisplayContent(item.id) }}
                                                    style={{
                                                        fontSize: "14px",
                                                        cursor: "pointer",
                                                        marginRight: "6px",
                                                        display: isDisplayContent !== item.id ? 'block' : 'none'
                                                    }}
                                                >
                                                </i>
                                                <i
                                                    class="far fa-trash-alt"
                                                    onClick={() => deleteComment(item.id)}
                                                    style={{
                                                        fontSize: "14px",
                                                        cursor: "pointer",
                                                        display: isDisplayContent !== item.id ? 'block' : 'none'
                                                    }}
                                                >
                                                </i>
                                            </>
                                        }
                                    </ul>

                                </div>
                            </div>
                        ))
                    }



                    {
                        _totalRows > _limit
                        && <div className="pagination"
                            style={{ width: "100%" }}
                        >
                            <Pagination
                                activeClass="pagination__item--active"
                                innerClass="panigation__list"
                                itemClass="panigation__item"
                                linkClass="panigation__link"
                                activePage={activePage}
                                itemsCountPerPage={_limit}
                                totalItemsCount={_totalRows}
                                pageRangeDisplayed={5}
                                onChange={onPageChange}
                            />
                        </div>
                    }
                </div>
            </div>


        </div>
    );
}