import './detail-comment.css';
import { useState, useEffect } from 'react';
import BoxDetailComment from './detail-comment';
import { connect, useSelector } from 'react-redux';
import { ConnectCommentHub } from 'actions';
import { connectionCommentHub } from 'constants/index';
import detailApi from 'api/detailApi';

function DetailComment(props) {
    const { ratingStatic, productId } = props
    const [listComments, setListComments] = useState([])
    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [bannedWords, setBannedWords] = useState([])
    const userInfor = useSelector(state => state.userReducer);

    const [filter, setFilter] = useState({
        _limit: 10,
        _page: 1,
        // productId: productId ? productId : 0
    })
    const [pagination, setPagination] = useState({
        _page: 1,
        _limit: 10,
        _totalRows: 1,
    })
    useEffect(() => {
        console.log(connectionCommentHub)
        if (connectionCommentHub) {
            connectionCommentHub.on("ReceiveMessage", newListComments => {
                setListComments(newListComments)
            });
        }
    }, [])

    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await detailApi.getCommentsByProductId({ ...filter, productId: productId })
                if (response.code === 1) {
                    setListComments(response.data.listComments)
                    setPagination(response.data.pagination)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();

        async function getBannedWordsFromApi() {
            try {
                const response = await detailApi.getBannedWords()
                if (response.code === 1) {
                    setBannedWords(response.data)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getBannedWordsFromApi();
        setIsFetchComplete(true);
    }, [isFetchComplete, filter, productId]);

    function handlePageChange(newPage) {
        if (!newPage) return
        setFilter({
            ...filter,
            _page: newPage,
        })
    }

    async function deleteComment(id) {
        if (!id) return
        try {
            const response = await detailApi.deleteComment({ id: id })
            if (response.code === 1) {
                if (connectionCommentHub)
                    connectionCommentHub.send("UpdateListComment", productId);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    async function updateComment(updateComment) {
        if (!updateComment) return;
        try {
            const response = await detailApi.updateComment({
                id: updateComment.id,
                content: updateComment.content
            })
            if (response.code === 1) {
                if (connectionCommentHub)
                    connectionCommentHub.send("UpdateListComment", productId);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }
    async function addComment(addComment) {
        try {
            const response = await detailApi.addComment({
                productId: productId,
                customerId: userInfor.id,
                content: addComment.content
            })
            if (response.code === 1) {
                if (connectionCommentHub)
                    connectionCommentHub.send("UpdateListComment", productId);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    async function addRating(rank) {
        try {
            const response = await detailApi.addRating({
                productId: productId,
                rank: rank,
                customerId: userInfor.id
            });
            if (response.code === 1) {
                alert(response.data)
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }
    return (
        <BoxDetailComment
            isFetchComplete={isFetchComplete}
            listComments={listComments}
            pagination={pagination}
            bannedWords={bannedWords}
            handlePageChange={handlePageChange}
            deleteComment={deleteComment}
            updateComment={updateComment}
            addComment={addComment}
            addRating={addRating}
            ratingStatic={ratingStatic}
        />
    );
}

const mapStateToProps = (state, ownProps) => {
    return {
        new: state.connectCommentHub
    }
}
const mapDispatchToProps = dispatch => ({
    connectCommentHub: () => dispatch(ConnectCommentHub())
})
export default connect(mapStateToProps, mapDispatchToProps)(DetailComment);