import React from 'react';
import DetailRelate from './detail-relate';
import DetailComment from './detail-comment/index';
import ReactHtmlParser from 'react-html-parser';

const DetailBot = function (props) {
    const { ratingStatic, listProductDetails, productId, listRelatedProducts } = props
    return (
        <div className="detail-bot grid-custom wide">
            <div className="row">
                <div className="col-md-9">
                    <span className="btn--primary btn-detail">Product detail</span>
                    <table className="detail-bot-table">
                        <tbody style={{ verticalAlign: 'top' }}>
                            <tr>
                                <td>
                                    <p>Product information</p>
                                    {/* Cổ Tròn<br />
                                    Tay Ngắn<br />
                                    Vạt Ngang<br />
                                    Hình In Trước<br /> */}
                                    {ReactHtmlParser(listProductDetails[0].productDescription.replace("<p>", ""))}
                                </td>
                                <td
                                    style={{
                                        overflow: "hidden",
                                        maxWidth: "500px"
                                    }}
                                >
                                    <p>Brand</p>
                                    {ReactHtmlParser(listProductDetails[0].brandDescription.replace("<p>", ""))}
                                </td>
                                <td>
                                    <p>MODEL & SIZE</p>
                                    Model's: 60kg/  175cm<br />
                                    Size: XL<br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>Material</p>
                                    {ReactHtmlParser(listProductDetails[0].material.replace("<p>", ""))}
                                </td>
                                <td>

                                </td>
                                <td>
                                    Productcode: {ReactHtmlParser(listProductDetails[0].productCode.split('-')[0])}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <style jsx>{`table {
                    border: 1px rgba(0, 0, 0, 0.1);
                }
                table tbody tr td {
                    font-size: '1.4rem',
                    padding: '14px',
                    line-height: '3rem',
                }
                table tbody tr td p {
                    font-size: 1.6rem;
                    font-weight: 700;
                    margin: 10px 0;
                }`}</style>
                    <DetailComment
                        productId={productId}
                        ratingStatic={ratingStatic}
                    />
                </div>
                <div className="col-md-3">
                    <DetailRelate
                        listRelatedProducts={listRelatedProducts}
                    />
                </div>
            </div>
        </div>
    );
}

export default DetailBot;