import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import DetailTop from './detail-top';
import DetailBot from './detail-bot';
import DetailSaw from './detail-saw';
import detailApi from 'api/detailApi';
import Loading3 from 'components/Commons/Loading3';
import { updateListProductSaws } from 'actions';

function Detail() {
    const [listProductDetails, setListProductDetails] = useState(null)
    const [listRelatedProducts, setListRelatedProducts] = useState(null)
    const [listColors, setListColors] = useState(null)
    const [ratingStatic, setRatingStatic] = useState(null)
    const [productId, setProductId] = useState(0)
    const [isFetchComplete, setIsFetchComplete] = useState(false)
    let { slug } = useParams();

    const userInfor = useSelector(state => state.userReducer);
    const dispatch = useDispatch();
    const userId = userInfor
        ? userInfor.id
        : '';

    const [alertInfor, setAlertInfor] = useState({
        type: 0,
        message: ""
    });

    useEffect(() => {
        document.title = "K300 | Detail page";
        window.scrollTo(0, 0)
    }, []);

    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await detailApi.getProductBySlug({ slug: slug, customerId: userId });
                if (response.code === 1) {
                    setListProductDetails(response.data.listProductDetails)
                    setListRelatedProducts(response.data.listRelatedProducts)
                    setListColors(response.data.listColors)
                    setRatingStatic(response.data.ratingStatic)
                    setProductId(response.data.listProductDetails[0].productId)
                    setIsFetchComplete(true)
                } else {
                    setAlertInfor({
                        type: -1,
                        message: response.message
                    })
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [slug, isFetchComplete, userId]);

    useEffect(() => {
        if (listProductDetails) {
            dispatch(updateListProductSaws(listProductDetails[0]));
        }
    }, [isFetchComplete, listProductDetails, dispatch])

    return (
        isFetchComplete
            ? <div className="main">
                <div className="wrap-detail-page">
                    <DetailTop
                        listProductDetails={listProductDetails}
                        listColors={listColors}
                        ratingStatic={ratingStatic}
                        alertInfor={alertInfor}
                    />
                    <DetailBot
                        listProductDetails={listProductDetails}
                        listRelatedProducts={listRelatedProducts}
                        productId={productId}
                        ratingStatic={ratingStatic}
                    />
                    <DetailSaw />
                </div>
            </div>
            : <Loading3
                height="60vh"
                spinnerWidth="50px"
            />
    );
}

export default Detail;