import { useState } from "react";

export default function EditCommentConten(props) {
    const { item, updateComment, setIsDisplayContent } = props
    const [editContent, setEditContent] = useState(item.content)
    return (
        <>
            <span>
                <input
                    type="text"
                    class="form-control"
                    style={{ height: "40px" }}
                    value={editContent}
                    onChange={(e) => setEditContent(e.target.value)}
                />
            </span>
            <i
                style={{
                    fontSize: "16px",
                    cursor: "pointer",
                    marginRight: "6px"
                }}
                class="far fa-save"
                onClick={() => { updateComment({ ...item, content: editContent }); setIsDisplayContent('') }}
            >
            </i>
            <i
                style={{
                    fontSize: "16px",
                    cursor: "pointer"
                }}
                class="far fa-window-close"
                onClick={() => { setIsDisplayContent('') }}
            >
            </i>
        </>
    );
}