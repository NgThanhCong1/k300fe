import { Link } from 'react-router-dom';
import Pagination from "react-js-pagination";
import { useState } from "react";
import { Button, ButtonDropdown, DropdownItem, DropdownMenu, DropdownToggle } from 'reactstrap';
import { toVNCurency } from 'helpers';

export default function BoxHistoryOrder(props) {
    const { handleBtnFilterClick, listHistoryOrderMasters, handleBtnCancleClick, pagination, handlePageChange } = props
    const { _page, _limit, _totalRows } = pagination;

    const listTrackingTransactions = [
        {
            name: 'All',
            value: -99
        },
        {
            name: 'Pending',
            value: 1
        },
        {
            name: 'Approve',
            value: 2
        },
        {
            name: 'Shipping',
            value: 3
        },
        {
            name: 'Recieved',
            value: 4
        },
        {
            name: 'Cancel',
            value: 0
        }
    ];

    const [dropdownOpen, setOpen] = useState(false);
    const toggle = () => setOpen(!dropdownOpen);
    // const totalPage = Math.ceil(_totalRows / _limit)
    function handleBtnPageChangeClick(pageNumber) {
        handlePageChange(pageNumber)
    }
    return (
        <>
            <div
                style={{
                    display: "flex",
                    marginBottom: "4px",
                    justifyContent: 'flex-end'
                }}
            >
                <ButtonDropdown isOpen={dropdownOpen} toggle={toggle}>
                    <DropdownToggle caret color="dark">
                        Find your transaction
                    </DropdownToggle>
                    <DropdownMenu
                        style={{
                            width: "100%"
                        }}
                    >
                        {
                            listTrackingTransactions.map(item => (
                                <DropdownItem
                                    onClick={() => handleBtnFilterClick(item.value)}
                                    style={{
                                        width: "100%",
                                        height: "30px"
                                    }}
                                >
                                    {item.name}
                                </DropdownItem>
                            ))
                        }

                    </DropdownMenu>
                </ButtonDropdown>
            </div>
            {
                listHistoryOrderMasters && listHistoryOrderMasters.map(item => (
                    <div>
                        <div style={{ padding: "6px", margin: "20px 0", border: "1px solid rgba(0,0,0,0.2)" }}>
                            <div style={{
                                display: "flex",
                                justifyContent: "space-between",
                            }}>
                                <h2>InvoiceId: {item.id} | Order date: {item.createdAt.split("T")[0]}</h2>
                                <span>
                                    <h2 style={{ display: "inline-block" }}>

                                        {/* Status: {
                                            item.status === 0 ? ' cancel'
                                                : item.status === 1
                                                    ? <>
                                                        <img src="https://i.imgur.com/9nnc9Et.png" alt="..." width="20px" /> pendding
                                                    </>
                                                    : item.status === 2
                                                        ? <>
                                                            <img src="https://i.imgur.com/u1AzR7w.png" alt="..." width="20px" /> approve
                                                        </>
                                                        : item.status === 3 ? ' shipping'
                                                            : item.status === 4 ? ' recieved'
                                                                : ''

                                        }
                                        &nbsp;|&nbsp; */}
                                    </h2>
                                    <Link to={`/account-setting/tracking-order/${item.id}`}>
                                        <Button
                                            style={{ minWidth: "unset", maxHeight: "30px", maxWidth: "100px" }}
                                            color="secondary"
                                        >
                                            {
                                                item.status === 0
                                                    ? <>
                                                        cancel <img src="https://cdn3.iconfinder.com/data/icons/education-3-5/48/146-512.png" alt="..." width="20px" />
                                                    </>
                                                    : item.status === 1
                                                        ? <>
                                                            pendding <img src="https://i.imgur.com/9nnc9Et.png" alt="..." width="20px" />
                                                        </>
                                                        : item.status === 2
                                                            ? <>
                                                                approve <img src="https://i.imgur.com/u1AzR7w.png" alt="..." width="20px" />
                                                            </>
                                                            : item.status === 3
                                                                ? <>
                                                                    shipping <img src="https://i.imgur.com/TkPm63y.png" alt="..." width="20px" />
                                                                </>
                                                                : item.status === 4
                                                                    ? <>
                                                                        recieved <img src="https://i.imgur.com/HdsziHP.png" alt="..." width="20px" />
                                                                    </>
                                                                    : ''

                                            }
                                        </Button>
                                    </Link>
                                    {
                                        (
                                            item.paymentMethod === 'Offline'
                                            && item.status !== 0
                                            && item.status !== 4
                                        )
                                        && <Button
                                            onClick={() => handleBtnCancleClick(item)}
                                            style={{ minWidth: "unset", maxHeight: "30px", maxWidth: "60px" }}
                                            color="danger">
                                            <i class="fas fa-window-close"></i>
                                        </Button>
                                    }

                                </span>
                            </div>
                            <table style={{ width: "100%" }}>
                                <tbody>
                                    {
                                        item.listHistoryOrders.map(subItem => (
                                            <tr className="box-cart-item">
                                                <td className="box-info">
                                                    <Link
                                                        to={`/some-route/reload/${subItem.slug}`}
                                                        style={{
                                                            color: "#000",
                                                            display: "flex"
                                                        }}
                                                    >
                                                        <img
                                                            className="box-info__img"
                                                            src={`${process.env.REACT_APP_IMAGE_URL}${subItem.listImagesLink[0]}`}
                                                            alt="alt here"
                                                        />
                                                        <div className="box-info__text">
                                                            <p>Name: {subItem.name}</p>
                                                            <p>Color: {subItem.color}</p>
                                                            <p>Size: {subItem.size}</p>
                                                            <p>Price: {toVNCurency(subItem.price)}</p>
                                                        </div>
                                                    </Link>
                                                </td>
                                                <td>Quantity: {subItem.quantity}</td>
                                                <td>Sub total: {toVNCurency(subItem.price * subItem.quantity)}</td>
                                            </tr>
                                        ))
                                    }
                                </tbody>
                            </table>
                            <div style={{ textAlign: "right" }}>
                                <h2>ShippingCharge: {toVNCurency(item.shippingCharge)}</h2>
                                <h2>Discount: {toVNCurency(item.discount)}</h2>
                                <h2>Total: {toVNCurency(item.totalMoney)}</h2>
                            </div>
                        </div>

                    </div>
                ))
            }
            {
                _totalRows > _limit
                && <div style={{ float: "right" }} className="pagination">
                    <Pagination
                        activeClass="pagination__item--active"
                        innerClass="panigation__list"
                        itemClass="panigation__item"
                        linkClass="panigation__link"
                        activePage={_page}
                        itemsCountPerPage={_limit}
                        totalItemsCount={_totalRows}
                        pageRangeDisplayed={5}
                        onChange={handleBtnPageChangeClick}
                    />
                </div>
            }
        </>
    );
}