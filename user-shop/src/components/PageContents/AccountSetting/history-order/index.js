import { useState, useEffect } from "react";
import BoxHistoryOrder from "./history-order";
import accountApi from "api/accountApi";
import { connectionNotifyHub } from "constants/index";
import Loading3 from "components/Commons/Loading3";
import { useSelector } from "react-redux";

function HistoryOrder(props) {
    const [listHistoryOrderMasters, setListHistoryOrderMasters] = useState([]);
    const [isFetchComplete, setIsFetchComplete] = useState(false);
    const [pagination, setPagination] = useState({
        _page: 1,
        _limit: 10,
        _totalRows: 1,
    });
    const userInfor = useSelector(state => state.userReducer);

    const [filter, setFilter] = useState({
        _limit: 2,
        _page: 1,
        userId: userInfor.id,
        status: -99
    })

    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await accountApi.getOutInvoiceByUser(filter);
                if (response.code === 1) {
                    setListHistoryOrderMasters(response.data.listOutinvoices)
                    setPagination(response.data.pagination)
                    setIsFetchComplete(true);
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi()
    }, [isFetchComplete, filter])

    async function addNotify(invoiceId) {
        try {
            const response = await accountApi.addNotify({
                customerId: userInfor.id,
                type: 0,
                invoiceId: invoiceId,
            });
            if (response.code === 1) {
                if (connectionNotifyHub)
                    connectionNotifyHub.send("UpdateListNotify", userInfor.id);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    async function handleBtnCancleClick(item) {
        if (item.status === 0) return
        try {
            const response = await accountApi.cancleOutInvoice(item)
            if (response.code === 1) {
                addNotify(item.id)
                setIsFetchComplete(false);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    function handlePageChange(pageNumber) {
        setFilter({
            ...filter,
            _page: pageNumber,
        })
    }

    async function handleBtnFilterClick(status) {
        console.log(status)
        await setFilter({
            ...filter,
            _page: 1,
            status: status,
        })
    }

    return (
        isFetchComplete
            ? <BoxHistoryOrder
                handleBtnFilterClick={handleBtnFilterClick}
                listHistoryOrderMasters={listHistoryOrderMasters}
                handleBtnCancleClick={handleBtnCancleClick}
                pagination={pagination}
                handlePageChange={handlePageChange}
            />
            : <Loading3
                height="60vh"
                spinnerWidth="40px"
            />
    );
}

export default HistoryOrder;