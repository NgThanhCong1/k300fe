import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import Tracking from './tracking';
import accountApi from 'api/accountApi';

function TrackingOrder() {
    let { id } = useParams();
    const [outInvoice, setOutInvoice] = useState(null);
    const [isFetchComplete, setIsFetchComplete] = useState(false);

    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await accountApi.trackingOrder(id);
                if (response.code === 1) {
                    setOutInvoice(response.data)
                    setIsFetchComplete(true);
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, id])

    return (
        isFetchComplete &&
        <Tracking
            outInvoice={outInvoice}
        />
    );
}

export default TrackingOrder;