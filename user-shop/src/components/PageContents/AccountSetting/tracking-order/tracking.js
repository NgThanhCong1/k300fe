import { Link } from 'react-router-dom';

export default function Tracking(props) {
    const { outInvoice } = props
    return (
        <div className="container px-1 px-md-4 py-5 mx-auto">
            <Link
                style={{ float: "right" }}
                to="/account-setting/history-orders"
            >
                <button className="btn--primary">Back <i class="fas fa-arrow-left"></i></button>
            </Link>
            <div className="card-tracking">
                <div className="row d-flex justify-content-between px-3 top">
                    <div className="d-flex">
                        <h5>ORDER <span className="text-primary font-weight-bold">#{outInvoice.id}</span></h5>
                    </div>
                    <div className="d-flex flex-column text-sm-right">
                        <p className="mb-0">Expected Arrival <span>{outInvoice.createdAt}</span></p>
                    </div>
                </div> {/* Add class 'active' to progress */}
                <div className="row d-flex justify-content-center">
                    <div className="col-12">
                        {
                            outInvoice.status === 1 ?
                                <ul id="progressbar" className="text-center">
                                    <li className="active step0" />
                                    <li className="step0" />
                                    <li className="step0" />
                                    <li className="step0" />
                                </ul>
                                :
                                outInvoice.status === 2 ?
                                    <ul id="progressbar" className="text-center">
                                        <li className="active step0" />
                                        <li className="active step0" />
                                        <li className="step0" />
                                        <li className="step0" />
                                    </ul>
                                    :
                                    outInvoice.status === 3 ?
                                        <ul id="progressbar" className="text-center">
                                            <li className="active step0" />
                                            <li className="active step0" />
                                            <li className="active step0" />
                                            <li className="step0" />
                                        </ul>
                                        :
                                        outInvoice.status === 4 ?
                                            <ul id="progressbar" className="text-center">
                                                <li className="active step0" />
                                                <li className="active step0" />
                                                <li className="active step0" />
                                                <li className="active step0" />
                                            </ul>
                                            :
                                            <ul id="progressbar" className="text-center">
                                                <li className="step0" />
                                                <li className="step0" />
                                                <li className="step0" />
                                                <li className="step0" />
                                            </ul>
                        }
                    </div>
                </div>
                <div className="row justify-content-between top">
                    <div className="row d-flex icon-content"> <img alt="alt here" className="icon" src="https://i.imgur.com/9nnc9Et.png" />
                        <div className="d-flex flex-column">
                            <p className="font-weight-bold">Order<br />Processed</p>
                        </div>
                    </div>
                    <div className="row d-flex icon-content"> <img alt="alt here" className="icon" src="https://i.imgur.com/u1AzR7w.png" />
                        <div className="d-flex flex-column">
                            <p className="font-weight-bold">Order<br />Shipped</p>
                        </div>
                    </div>
                    <div className="row d-flex icon-content"> <img alt="alt here" className="icon" src="https://i.imgur.com/TkPm63y.png" />
                        <div className="d-flex flex-column">
                            <p className="font-weight-bold">Order<br />En Route</p>
                        </div>
                    </div>
                    <div className="row d-flex icon-content"> <img alt="alt here" className="icon" src="https://i.imgur.com/HdsziHP.png" />
                        <div className="d-flex flex-column">
                            <p className="font-weight-bold">Order<br />Arrived</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}