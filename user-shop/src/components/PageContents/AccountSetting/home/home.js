import { useState } from 'react';
import { Route, Switch, Link } from 'react-router-dom';
import HomeAccount from '../account';
import PasswordSetting from '../password';
import HistoryOrder from '../history-order';
import TrackingOrder from '../tracking-order';
import { useSelector } from 'react-redux';
import EmailSetting from '../email';

export default function BoxHome(props) {

    const userInfor = useSelector(state => state.userReducer);
    const { alertInfor, handleUpdate } = props;
    const [listFiles, setListFiles] = useState([])
    const [listFilesObj, setListFilesObj] = useState([])
    const fileArray = []
    const fileObj = []

    const [isDisplaySaveBtn, setIsDisplaySaveBtn] = useState(false)
    const [isActive, setIsActive] = useState('Account');

    function handleUploadFiles(e) {
        fileObj.push(e.target.files)
        for (let i = 0; i < fileObj[0].length; i++) {
            fileArray.push(URL.createObjectURL(fileObj[0][i]))
        }
        setListFiles(fileArray)
        setListFilesObj(e.target.files)
        setIsDisplaySaveBtn(true)
    }

    const [listItems,] = useState([
        { enpoints: "home", name: "Account" },
        { enpoints: "history-orders", name: "History orders" },
        { enpoints: "password-setting", name: "Password" },
        { enpoints: "email-setting", name: "Email" },
    ])

    function handleBtnUpdateClick() {
        handleUpdate({
            imageFile: listFilesObj[0]
        });
        setIsDisplaySaveBtn(false)
    }

    return (
        <div className="main">
            <div
                className="grid-custom wide"
            >

                <div
                    style={{ background: "#fff", margin: "100px 0", padding: "40px" }}
                >
                    <h1 style={{ fontSize: "20px", marginBottom: "30px" }}>Account Settings</h1>
                    <div className="account-setting-warp row shadow">
                        <div className="profile-tab-nav border-right col-md-2">
                            <div className="accountSetting-avatar">
                                <span
                                    style={{ position: "relative", display: "inline-block" }}
                                >
                                    {
                                        listFiles.length > 0 ?
                                            (listFiles).map(url => (
                                                <img
                                                    className="accountSetting-avatar__img shadow"
                                                    style={{ maxWidth: "100px" }} src={url} alt="..."
                                                />
                                            ))
                                            : userInfor.avatar !== ''
                                                ? <img
                                                    className="accountSetting-avatar__img shadow"
                                                    style={{ maxWidth: "100px" }}
                                                    src={`${process.env.REACT_APP_IMAGE_URL}${userInfor.avatar}?v=${Math.random()}`}
                                                    alt="..."
                                                />
                                                : <img
                                                    className="accountSetting-avatar__img shadow"
                                                    style={{ maxWidth: "100px" }}
                                                    src={`${process.env.REACT_APP_IMAGE_URL}/Images/Users/user.png`}
                                                    alt="..."
                                                />
                                    }
                                    <label
                                        for="listFiles"
                                        style={{
                                            display: "flex",
                                            minWidth: "unset",
                                            width: "30px",
                                            height: "30px",
                                            borderRadius: "50%",
                                            position: "absolute",
                                            bottom: "0",
                                            right: "0"
                                        }}
                                        className="btn--primary"
                                    >


                                        <i style={{ margin: "auto" }} class="ti-camera"></i>

                                    </label>

                                </span>
                                {
                                    isDisplaySaveBtn &&
                                    <p>
                                        <button
                                            onClick={handleBtnUpdateClick}
                                            className="btn--primary"
                                            style={{
                                                width: "60px",
                                                height: "30px",
                                                lineHeight: "30px"
                                            }}
                                        >
                                            <i
                                                class='fa fa-save'
                                                style={{
                                                    fontSize: "12px"
                                                }}
                                            >

                                            </i>
                                        </button>
                                    </p>
                                }
                                <input
                                    type="file"
                                    className="form-control"
                                    id="listFiles"
                                    onChange={(e) => handleUploadFiles(e)}
                                    hidden
                                />

                                <h4 className="text-center">
                                    {userInfor.username}
                                </h4>

                            </div>
                            <div className="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                {
                                    listItems.map(item => (
                                        <Link
                                            style={{
                                                textDecoration: "none",
                                                margin: "5px 0"
                                            }}
                                            to={`/account-setting/${item.enpoints}`}
                                            onClick={() => setIsActive(item.name)}
                                        >
                                            <p className={isActive === item.name ? "nav-item active" : "nav-item"}
                                                data-toggle="pill"
                                                role="tab"
                                                style={{
                                                    height: '40px',
                                                    lineHeight: '40px',
                                                    padding: '0 10px',
                                                    margin: '0',
                                                    letterSpacing: '2px'
                                                }}
                                            >
                                                <i className="text-center mr-1" />
                                                {item.name}
                                            </p>
                                        </Link>
                                    ))
                                }
                            </div>
                        </div>
                        <div className="tab-content col-md-10" id="v-pills-tabContent">
                            <Switch>
                                <Route path="/account-setting/home" exact render={(props) => <HomeAccount handleUpdate={handleUpdate} alertInfor={alertInfor} {...props} />} />
                                <Route path="/account-setting/password-setting" component={PasswordSetting} />
                                <Route path="/account-setting/history-orders" component={HistoryOrder} />
                                <Route path="/account-setting/tracking-order/:id" component={TrackingOrder} />
                                <Route path="/account-setting/email-setting" component={EmailSetting} />
                            </Switch>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}