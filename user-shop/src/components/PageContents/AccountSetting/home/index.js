import BoxHome from './home';
import { useEffect, useState } from 'react';
import '../account.css';
import accountApi from 'api/accountApi';
import { useDispatch, useSelector } from 'react-redux';
import { updateUser } from 'actions';

function AccountSetting() {
    const [forceLoad, setForceLoad] = useState(false)
    const userInfor = useSelector(state => state.userReducer);
    const dispath = useDispatch();
    const [alertInfor, setAlertInfor] = useState({
        type: 0,
        message: ''
    })

    useEffect(() => {
        document.title = "K300 | Account settings page";
        window.scrollTo(0, 0)
    }, []);

    async function handleUpdate(item) {
        if (!item) return
        let formData = new FormData()
        if (item.imageFile)
            formData.append('imageFile', item.imageFile)
        formData.append('id', userInfor.id)
        // formData.append('userName', item.username)
        formData.append('address', item.address)
        formData.append('email', item.email)
        formData.append('phone', item.phone)
        formData.append('firstName', item.firstName)
        formData.append('lastName', item.lastName)

        try {
            const response = await accountApi.updateCustomer(formData)
            if (response.code === 1) {
                dispath(updateUser(response.data));
                setForceLoad(!forceLoad);
                setAlertInfor({
                    type: 1,
                    message: response.message
                })
            } else {
                setAlertInfor({
                    type: -1,
                    message: response.message
                })
            }
        } catch (error) {
            // apiErrorHandler(error);
        }
    }
    return (
        <BoxHome
            handleUpdate={handleUpdate}
            alertInfor={alertInfor}
        />
    );

}

export default AccountSetting;
