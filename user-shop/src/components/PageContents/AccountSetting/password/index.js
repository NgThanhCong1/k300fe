import authApi from "api/authApi";
import { useState } from "react";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import BoxPassword from "./password";

function PasswordSetting() {
    const [redirectToReferrer, setRedirectToReferrer] = useState(false)
    const userInfor = useSelector(state => state.userReducer);

    function redirectTo() {
        return <Redirect to="/reset-password-success" />;
    }

    async function resetPassword(resetInfor) {
        try {
            const response = await authApi.resetPassword({ ...resetInfor, id: userInfor.id })
            if (response.code === 1) {
                localStorage.removeItem('userToken')
                setRedirectToReferrer(true);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    if (redirectToReferrer === true) return redirectTo();

    return (
        <BoxPassword
            resetPassword={resetPassword}
            setRedirectToReferrer={setRedirectToReferrer}
        />
    );
}

export default PasswordSetting