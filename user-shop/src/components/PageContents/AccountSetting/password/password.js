import { useForm } from "react-hook-form";

export default function BoxPassword(props) {
    const { resetPassword } = props
    const { register, handleSubmit, formState: { errors } } = useForm({

    });

    const onSubmit = async data => {
        if (data.newPassword !== data.newConfirmPassword) {
            alert("pass do not match")
            return;
        }
        resetPassword({
            oldPassword: data.oldPassword,
            newPassword: data.newPassword
        })
    }

    return (
        <div className="tab-pane fade show active" id="password" role="tabpanel" aria-labelledby="password-tab">
            <h3 className="account-title">Password Settings</h3>
            <div className="row">
                <form style={{ width: "100%" }} onSubmit={handleSubmit(onSubmit)}>
                    <div className="col-md-6 form-group">
                        <label htmlFor="oldPassword">Old password:</label>
                        <input
                            id="oldPassword"
                            type="password"
                            className="form-control"
                            name="oldPassword" autofocus
                            {...register('oldPassword', { required: true })}
                        />
                        {errors.oldPassword && <span style={{ color: "red" }}>This field is required</span>}
                    </div>
                    <div className="col-md-6 form-group">
                        <label htmlFor="newPassword">New password: </label>
                        <input
                            id="newPassword"
                            type="password"
                            className="form-control"
                            name="newPassword" autofocus
                            {...register('newPassword', { required: true })}
                        />
                        {errors.newPassword && <span style={{ color: "red" }}>This field is required</span>}
                    </div>
                    <div className="col-md-6 form-group">
                        <label htmlFor="newConfirmPassword">Confirm new password:</label>
                        <input
                            id="newConfirmPassword"
                            type="password"
                            className="form-control"
                            name="newConfirmPassword"
                            {...register('newConfirmPassword', { required: true })}
                        />
                        {errors.newConfirmPassword && <span style={{ color: "red" }}>This field is required</span>}
                    </div>
                    <div className="col-md-6 form-group">
                        <button type="submit" className="btn--primary">Update</button>
                        <button className="btn--secondary">Cancel</button>
                    </div>
                </form>
            </div>

        </div>
    );
}