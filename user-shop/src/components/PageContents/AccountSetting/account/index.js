import { useEffect, useState } from 'react';
import Account from './account';
import accountApi from 'api/accountApi';
import Loading3 from 'components/Commons/Loading3';
import { useSelector } from 'react-redux';

function HomeAccount(props) {
    const { alertInfor, handleUpdate } = props
    const [user, setUser] = useState(null)
    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const userInfor = useSelector(state => state.userReducer);

    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await accountApi.getCustomer(userInfor.id);
                if (response.code === 1) {
                    setUser(response.data)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                // apiErrorHandler(error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, userInfor.id]);

    return (
        isFetchComplete
            ? <Account
                user={user}
                handleUpdate={handleUpdate}
                alertInfor={alertInfor}
            />
            : <Loading3
                height="60vh"
                spinnerWidth="40px"
            />
    );
}

export default HomeAccount