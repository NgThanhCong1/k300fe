import CustomAlert from "components/Commons/Alert";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { Spinner } from "reactstrap";

export default function Account(props) {
    const { alertInfor, user, handleUpdate } = props
    const [isDisabledBtnUpdate, setIsDisabledBtnUpdate] = useState(false);

    const { register, handleSubmit, formState: { errors } } = useForm();
    const onSubmit = async data => {
        setIsDisabledBtnUpdate(true);
        if (!handleUpdate) return
        await handleUpdate({
            address: data.address,
            email: data.email,
            phone: data.phone,
            firstName: data.firstname,
            lastName: data.lastname,
        })
        setIsDisabledBtnUpdate(false);
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <div className="tab-pane fade show active" id="account" role="tabpanel" aria-labelledby="account-tab">
                <h3 className="account-title">Account Settings</h3>
                <CustomAlert
                    {...alertInfor}
                />
                <div className="row">
                    <div className="col-md-6 home-accountSetting-formgroup">
                        <div className="form-group">
                            <label>First Name</label>
                            <input
                                type="text"
                                className="form-control"
                                defaultValue={user.firstName}
                                {...register('firstname', { required: true })}
                                style={{
                                    height: "35px",
                                    padding: "8px"
                                }}
                            />
                            {errors.firstname && <span style={{ color: "red" }}>This field is required</span>}
                        </div>
                    </div>
                    <div className="col-md-6 home-accountSetting-formgroup">
                        <div className="form-group">
                            <label>Last Name</label>
                            <input
                                type="text"
                                className="form-control"
                                defaultValue={user.lastName}
                                {...register('lastname', { required: true })}
                                style={{
                                    height: "35px",
                                    padding: "8px"
                                }}
                            />
                            {errors.lastname && <span style={{ color: "red" }}>This field is required</span>}
                        </div>
                    </div>
                    <div className="col-md-6 home-accountSetting-formgroup">
                        <div className="form-group">
                            <label>Phone number</label>
                            <input
                                type="text"
                                className="form-control"
                                defaultValue={user.phoneNumber}
                                {...register('phone')}
                                style={{
                                    height: "35px",
                                    padding: "8px"
                                }}
                            />
                            {errors.phone && <span style={{ color: "red" }}>This field is required</span>}
                        </div>
                    </div>
                    <div className="col-md-6 home-accountSetting-formgroup">
                        <div className="form-group">
                            <label>Address</label>
                            <input
                                type="text"
                                className="form-control"
                                defaultValue={user.address}
                                {...register('address')}
                                style={{
                                    height: "35px",
                                    padding: "8px"
                                }}
                            />
                            {errors.address && <span style={{ color: "red" }}>This field is required</span>}
                        </div>
                    </div>

                </div>
                <div>
                    <button
                        type="submit"
                        className="btn--primary"
                        disabled={isDisabledBtnUpdate}
                    >
                        {isDisabledBtnUpdate ? <Spinner></Spinner> : 'Update'}
                    </button>
                    <button className="btn--secondary">
                        Cancel
                    </button>
                </div>
            </div>
        </form>
    );
}