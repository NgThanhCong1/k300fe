import authApi from "api/authApi";
import { useState } from "react";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import BoxEmail from "./email";

function EmailSetting() {
    const [redirectToReferrer, setRedirectToReferrer] = useState(false)
    const userInfor = useSelector(state => state.userReducer);

    function redirectTo() {
        return <Redirect to="/confirm-email" />;
    }

    async function resetEmail(resetInfor) {
        try {
            const response = await authApi.changeEmail({
                oldEmail: userInfor.email,
                newEmail: resetInfor.email
            })
            if (response.code === 1) {
                setRedirectToReferrer(true);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    if (redirectToReferrer === true) return redirectTo();

    return (
        <BoxEmail
            oldEmail={userInfor.email}
            resetEmail={resetEmail}
        />
    );
}

export default EmailSetting