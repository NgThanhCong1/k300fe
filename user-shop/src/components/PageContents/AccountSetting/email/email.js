import { useForm } from "react-hook-form";

export default function BoxEmail(props) {
    const { oldEmail, resetEmail } = props
    const { register, handleSubmit, formState: { errors } } = useForm({

    });

    const onSubmit = async data => {
        resetEmail({
            email: data.email,
        })
    }

    return (
        <div className="tab-pane fade show active" id="password" role="tabpanel" aria-labelledby="password-tab">
            <h3 className="account-title">Change email</h3>
            <div className="row">
                <form style={{ width: "100%" }} onSubmit={handleSubmit(onSubmit)}>
                    <div className="col-md-6 form-group">
                        <label htmlFor="oldPassword">Old email</label>
                        <input
                            type="email"
                            className="form-control"
                            value={oldEmail}
                            disabled
                        />
                    </div>
                    <div className="col-md-6 form-group">
                        <label htmlFor="oldPassword">New email:</label>
                        <input
                            type="email"
                            className="form-control"
                            name="email" autofocus
                            {...register('email', { required: true })}
                        />
                        {errors.oldPassword && <span style={{ color: "red" }}>This field is required</span>}
                    </div>
                    <div className="col-md-6 form-group">
                        <button type="submit" className="btn--primary">Update</button>
                        <button className="btn--secondary">Cancel</button>
                    </div>
                </form>
            </div>

        </div>
    );
}