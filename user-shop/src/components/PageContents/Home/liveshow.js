import React from "react";
import SwiperCore, { Navigation, Pagination, Autoplay } from 'swiper/core';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import 'swiper/components/scrollbar/scrollbar.scss';
import { rootPathImages } from "../../../constants/root-server"
SwiperCore.use([Navigation, Pagination, Autoplay]);

class LiveShow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            arrLiveShowImg: [`${rootPathImages}SlideShows/slideshow_2.webp`, `${rootPathImages}SlideShows/slideshow_3.webp`, `${rootPathImages}SlideShows/slideshow_4.webp`],
        };
    }
    render() {
        return (
            <div className="slideshow-container">
                <Swiper
                    slidesPerView={1}
                    spaceBetween={30}
                    loop={true}
                    autoplay={{
                        delay: 2000,
                    }}
                    className="mySwiper"
                    style={{ zIndex: -100 }}
                >
                    {
                        this.state.arrLiveShowImg.map((item) => (
                            <SwiperSlide>
                                <div>
                                    <img style={{ width: "100%" }} src={item} alt="" />
                                </div>
                            </SwiperSlide>
                        ))
                    }
                </Swiper>
            </div>
        );
    }
}

export default LiveShow;