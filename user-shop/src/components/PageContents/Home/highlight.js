import React from "react";
import { rootPathImages } from "../../../constants/root-server";
import { ButtonPrimary } from '../../Commons/Button';

const HighlightStreetwear = function (props) {
    return (
        <div className="highlight-streetwear">
            <img src={`${rootPathImages}Highlights/banner_top_img.webp`} className="highlight-streetwear__img" alt="" />
            <ButtonPrimary
                text="Shop now"
                linkTo="/newin"
                className="highlight-streetwear__shopnow"
            />
        </div>
    );
}

export default HighlightStreetwear;