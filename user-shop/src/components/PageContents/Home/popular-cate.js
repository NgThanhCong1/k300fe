import React from "react";
import { Spinner } from 'reactstrap';
import { connect } from 'react-redux';
import { forceUpdate } from 'actions';
import { rootPath } from "constants/root-server";
import { ButtonPrimary } from "components/Commons/Button";

const PopularCate = function (props) {
    const { listCates, isLoading } = props
    return (
        <div className="grid-custom wide popular-cate">
            <div className="row">
                {
                    isLoading ?
                        <Spinner color="success" />
                        :
                        listCates.map((item) => (
                            <div className="col-md-6 popular-cate__item">
                                <img src={`${rootPath}${item.listImageLinks[0]}`} alt="" className="popular-cate__img" />
                                <div className="popular-cate__content">
                                    <div className="popular-cate__name">{item.name}</div>
                                    {/* <Link style={{ textDecoration: "none" }}  >
                                        <a
                                            className="menu-clothing__content-link underline_effect"
                                        >
                                            <button className="btn--primary popular-cate__btn">Shop Now</button>
                                        </a>

                                    </Link> */}
                                    <ButtonPrimary
                                        onClick={() => props.increment()}
                                        linkTo={`/collection/${item.name}`}
                                        text="Shop now"
                                    />
                                </div>
                            </div>
                        ))
                }
            </div>
        </div>
    );
}

const mapDispatchToProps = dispatch => ({
    increment: () => dispatch(forceUpdate())
})

export default connect(null, mapDispatchToProps)(PopularCate)