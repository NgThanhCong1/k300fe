import React from "react";
import { rootPath } from "../../../constants/root-server";
import { ButtonPrimary } from "../../Commons/Button";

const PopularCap = function (props) {
    return (
        <div className="popular-cap" style={{ backgroundImage: `url(${rootPath}/Images/PopularCap/banner_bottom_img.png)` }}>${ }
            <div className="popular-cap__content">
                <h1 className="popular-cap__title x-fw800">CAPS</h1>
                <span className="popular-cap__description">When it comes to true summer essentials, a baseball cap is up there with warm tins of beer, a sand-filled picnic and sunscreen. But more than just being a way of avoiding forcibly squinting at the sun, the right style can also add some personality to a bog standard shorts and T-shirt look</span>
                <ButtonPrimary
                    text="Shop now"
                    className="popular-cap__btn"
                    linkTo="collection/CAPS"
                />
            </div>
        </div>
    );
}

export default PopularCap;