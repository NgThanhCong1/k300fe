import React from "react";
import ListProduct from "components/Commons/ListProduct";
import Loading3 from "components/Commons/Loading3";
// import './home.css';

function NewIn(props) {
    const { listNewins } = props
    return (
        <div className="newin">
            <h1 className="newin__label x-fw800">NEW IN PRODUCTS</h1>
            <div className="grid-custom product__list">
                {
                    listNewins.length > 0
                        ? <ListProduct
                            direction="horizon"
                            listProducts={listNewins}
                        />
                        : <Loading3 />
                }
            </div>
        </div>
    );
}

export default NewIn;