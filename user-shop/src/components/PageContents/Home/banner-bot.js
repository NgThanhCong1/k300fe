import React from "react";
import { rootPath } from "../../../constants/root-server";
import { ButtonSecondary } from '../../Commons/Button';

function BannerBot(props) {
    return (
        <div className="banner-bot" style={{ backgroundImage: `url(${rootPath}/Images/BannerBot/slideshow_2.png)` }}>
            <div className="banner-bot__content">
                <h1 className="banner-bot__label">UPGRADE YOUR FASHION NOW</h1>
                <ButtonSecondary
                    text="Go now"
                    className="banner-bot__btn"
                />
            </div>
        </div>
    );
}

export default BannerBot;