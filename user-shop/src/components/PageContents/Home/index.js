import { React, useState, useEffect } from "react";
import LiveShow from "./liveshow";
import HighlightStreetwear from "./highlight";
import PopularCate from "./popular-cate";
import PopularShoe from "./popular-shoe";

import PopularCap from "./popular-cap";
import NewIn from "./newin";
import BannerBot from "./banner-bot";
import homeApi from "api/homeApi";

function Home() {
    const [listNewins, setListNewins] = useState([])
    const [listCates, setListCates] = useState([])

    useEffect(() => {
        document.title = "K300 | Home page";
        window.scrollTo(0, 0)
    }, []);

    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await homeApi.getNewInProducts();
                if (response.code === 1) {
                    setListNewins(response.data)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, []);

    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await homeApi.getTop4Categories();
                if (response.code === 1) {
                    setListCates(response.data)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, []);

    return (
        <div className="main">
            <LiveShow />
            <HighlightStreetwear />
            <PopularCate listCates={listCates} />
            <PopularShoe />
            <PopularCap />
            <NewIn
                listNewins={listNewins}
            />
            <BannerBot />
        </div>
    );
}

export default Home