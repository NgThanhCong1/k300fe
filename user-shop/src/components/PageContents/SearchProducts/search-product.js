import ListProduct from 'components/Commons/ListProduct';
import React from 'react';
import Pagination from "react-js-pagination";

function SearchProduct(props) {
    const { listProducts, pagination, onPageChange } = props;
    const { _page, _limit, _totalRows } = pagination;

    function handlePageChange(pageNumber) {
        if (!onPageChange) return
        onPageChange(pageNumber)
    }

    return (
        <>
            <h1 style={{ textAlign: "center", margin: "20px 0" }}>Search result</h1>
            <h3>Total results: {listProducts.length}</h3>

            <div class="collection__list">
                <div class="grid-custom product__list">
                    {
                        listProducts.length > 0
                            ? <ListProduct
                                direction="horizon"
                                listProducts={listProducts}
                            />
                            : "No product found"
                    }
                    {
                        _totalRows > _limit
                        && <div class="panigation">
                            <Pagination
                                activeClass="pagination__item--active"
                                innerClass="panigation__list"
                                itemClass="panigation__item"
                                linkClass="panigation__link"
                                activePage={_page}
                                itemsCountPerPage={_limit}
                                totalItemsCount={_totalRows}
                                pageRangeDisplayed={5}
                                onChange={handlePageChange}
                            />
                        </div>
                    }
                </div>
            </div>
        </>
    );
}

export default SearchProduct;