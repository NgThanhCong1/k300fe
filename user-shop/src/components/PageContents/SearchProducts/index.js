import React, { useState, useEffect, useCallback } from 'react';
import SearchProduct from './search-product';
import searchProductApi from 'api/searchProductApi';
import Loading3 from 'components/Commons/Loading3';
import { useSelector } from 'react-redux';

function SearchCollection(props) {
    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [listProducts, setListProducts] = useState([])
    let userInfor = useSelector(state => state.userReducer);
    if (userInfor === null) userInfor = { id: '' };
    const searchInfor = useSelector(state => state.historySearchReducer);

    const [filter, setFilter] = useState({
        _page: 1,
        _limit: 8,
    })
    const [pagination, setPagination] = useState({
        _page: 1,
        _limit: 8,
        _totalRows: 1,
    })

    useEffect(() => {
        document.title = "K300 | Search page";
        window.scrollTo(0, 0)
    }, []);

    const addKeyword = useCallback(async () => {
        debugger
        try {
            const response = await searchProductApi.addKeyword({
                customerId: userInfor.id,
                keyword: searchInfor.activeKeyword
            });
            if (response.code === 1) {

            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }, [userInfor.id, searchInfor.activeKeyword]);

    // useEffect(() => {
    // }, [searchInfor.categoryName, searchInfor.activeKeyword])

    useEffect(() => {
        setIsFetchComplete(false);
        async function getDataFromApi() {
            debugger
            try {
                const response = await searchProductApi.searchProducts({
                    ...filter,
                    categoryName: searchInfor.categoryName,
                    productName: searchInfor.activeKeyword
                });
                setIsFetchComplete(true);

                if (response.code === 1) {
                    addKeyword();
                    setListProducts(response.data.listCombineProducts)
                    setPagination(response.data.pagination)
                } else {
                    setListProducts([])
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [filter, searchInfor.categoryName, searchInfor.activeKeyword, addKeyword]);

    function handlePageChange(newPage) {
        if (!newPage) return
        setFilter({
            ...filter,
            _page: newPage,
        })
    }

    return (
        isFetchComplete
            ? <SearchProduct
                listProducts={listProducts}
                pagination={pagination}
                onPageChange={handlePageChange}
            />
            : <Loading3
                height="60vh"
                spinnerWidth="40px"
            />
    );
}

export default SearchCollection;

