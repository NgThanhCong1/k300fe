import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
export default function BoxOrderComplete() {
    return (
        <div className="main">
            <div className="order-complete-card">
                <div style={{ borderRadius: '200px', height: '200px', width: '200px', background: '#F8FAF5', margin: '0 auto' }}>
                    <i className="checkmark">✓</i>
                </div>
                <h1>Success</h1>
                <p>We received your purchase request;<br /> we'll be in touch shortly!</p>
                <Link
                    style={{ marginTop: "20px", display: "block" }}
                    to="/"
                >
                    <Button color="dark">
                        Countinute shopping
                    </Button>
                </Link>
            </div>
        </div>
    );
}