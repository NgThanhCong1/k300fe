import { useState, useRef, useEffect } from "react";
import { Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { deleteListProductInCartDB } from 'actions';
import BoxHome from './home';
import { connectionNotifyHub } from 'constants/index';
import orderApi from "api/orderApi";
import accountApi from "api/accountApi";
import Loading3 from "components/Commons/Loading3";

function OrderPage(props) {
    // let tmpListProducts = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')).listProductCart.filter(item => item.isChecked === 1) : []
    const [redirectToReferrer, setRedirectToReferrer] = useState(false)
    const typingTimeOutRef = useRef(null)
    const [discountId, setDiscountId] = useState(-1)
    const [discountPrice, setDiscountPrice] = useState(0)
    const [discountCode, setDiscountCode] = useState('')
    const [user, setUser] = useState(null)
    const [isFetchComplete, setIsFetchComplete] = useState(false);

    const dispatch = useDispatch();
    const listProductsInCart = useSelector(state => state.cartDBReducer).filter(p => p.isChecked === 1);
    const userInfor = useSelector(state => state.userReducer);

    useEffect(() => {
        document.title = "K300 | Home page";
        window.scrollTo(0, 0)
    }, []);


    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await accountApi.getCustomer(userInfor.id);
                if (response.code === 1) {
                    setUser(response.data)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete, userInfor.id]);

    function handleSearchInput(e) {
        setDiscountCode(e.target.value)
        if (typingTimeOutRef.current) clearTimeout(typingTimeOutRef.current)
        typingTimeOutRef.current = setTimeout(async () => {
            try {
                const response = await orderApi.searchDiscount(e.target.value);
                if (response.code === 1) {
                    setDiscountId(response.data.id)
                    setDiscountPrice(response.data.price)
                } else {
                    setDiscountPrice(0)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }, 300)
    }

    function redirectTo() {
        return <Redirect to="/order-complete" />;
    }

    if (redirectToReferrer === true) return redirectTo();

    async function order(item) {
        try {
            const response = await orderApi.order({
                ...item,
                discountId: discountId
            });
            if (response.code === 1) {
                await dispatch(deleteListProductInCartDB(item.listOutportProducts))
                addNotify(response.data)
                setRedirectToReferrer(true);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    async function orderWithPaymentOnline(item) {
        try {
            const response = await orderApi.orderOnline(item);
            if (response.code === 1) {
                await dispatch(deleteListProductInCartDB(item.orderInfor.listOutportProducts))
                addNotify(response.data.outInvoiceId);
                window.location.assign(response.data.paymentUrl)
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    async function addNotify(invoiceId) {
        try {
            const response = await orderApi.addNotify({
                customerId: userInfor.id,
                type: 1,
                invoiceId: invoiceId
            });
            if (response.code === 1) {
                if (connectionNotifyHub)
                    connectionNotifyHub.send("UpdateListNotify", userInfor.id);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    return (
        isFetchComplete
            ? <BoxHome
                user={user}
                order={order}
                discountCode={discountCode}
                discountPrice={discountPrice}
                listProductsInCart={listProductsInCart}
                handleSearchInput={handleSearchInput}
                orderWithPaymentOnline={orderWithPaymentOnline}
            />
            : <Loading3
                height="60vh"
                spinnerWidth="40px"
            />
    );
}

export default OrderPage;