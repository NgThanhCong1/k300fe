import { useState } from 'react';
import { Spinner, Table } from 'reactstrap';
import { useSelector } from 'react-redux';
import { toVNCurency } from 'helpers';
import { Link } from 'react-router-dom';
import firebase from 'components/PageContents/Auth/fire-base/firebase';
// import { useForm } from "react-hook-form";

function BoxHome(props) {

    const { orderWithPaymentOnline, user, discountId, order, listProductsInCart, discountCode, handleSearchInput, discountPrice } = props;
    const [fullname, setFullname] = useState(`${user.firstName} ${user.lastName}`)
    const [address, setAddress] = useState(user && user.address)
    const [phone, setPhone] = useState(user && user.phoneNumber)
    const [isValidPhone, setIsValidPhone] = useState(false);
    const [shipCost, setShipCost] = useState(30)
    const [bank, setBank] = useState('')
    const publicIp = require('public-ip');
    let total = 0;

    const [isOffSubmitBtn, setIsOffSubmitBtn] = useState(false);

    const userInfor = useSelector(state => state.userReducer);

    function checkPayDataValid(fullName, email, phone, listProducts) {
        if (fullName && email && phone && listProducts != null && listProducts.length > 0)
            return true;
        return false;
    }

    async function handlePayment() {
        if (!checkPayDataValid(fullname, phone, address, listProductsInCart) || isValidPhone === false) {
            alert("information not valid");
            return;
        }
        if (isOffSubmitBtn === true) return;
        setIsOffSubmitBtn(true);
        if (listProductsInCart.length === 0) return;
        if (shipCost === 0) {
            await orderWithPaymentOnline(
                {
                    orderInfor: {
                        listOutportProducts: listProductsInCart,
                        totalMoney: parseInt(total),
                        discountId: discountId,
                        customerId: userInfor.id,
                        shipAt: address,
                        shipPhone: phone,
                        shipName: fullname,
                        shippingCharge: 0,
                        paymentMethod: "Online"
                    },
                    paymentInfor: {
                        ip: await publicIp.v4(),
                        totalMoney: parseInt(total),
                        bankCode: bank
                    }
                }
            );
        } else {
            await order({
                listOutportProducts: listProductsInCart,
                totalMoney: parseInt(total) + parseInt(shipCost),
                discountId: discountId,
                customerId: userInfor.id,
                shipAt: address,
                shipPhone: phone,
                shipName: fullname,
                shippingCharge: shipCost,
                paymentMethod: "Offline"
            })
        }
        setIsOffSubmitBtn(false);
    }

    function handleBankChange(e) {
        setBank(e.target.value);
        setShipCost(0);
    }

    function handleShipCostChange(e) {
        setShipCost(e.target.value);
        setBank(0);
    }

    function handleClick() {
        let recapcha = new firebase.auth.RecaptchaVerifier('recaptcha-container');
        let number = phone;
        firebase.auth().signInWithPhoneNumber(number, recapcha).then(function (e) {
            let code = prompt('enter otp', '');
            if (code == null) return;
            e.confirm(code).then(result => {
                document.querySelector('#firebase').textContent = result.user.phoneNumber + " Number verified";
                setIsValidPhone(true);
            })
                .catch(error => console.log(error))
        })
    }

    return (
        <div className="main">
            <div className="grid-custom wide">
                <div className="wrap-cart-page row">
                    <div className="col-md-8 box-cart-left">
                        <h2
                            style={{ fontWeight: "700" }}
                        >
                            Payment
                        </h2>
                        {
                            user && <div style={{
                                borderTop: "1px solid rgba(0, 0, 0, 0.2)",
                                borderBottom: "1px solid rgba(0, 0, 0, 0.2)",
                                marginTop: "30px",
                            }}>
                                <h3 style={{ fontWeight: "700", fontSize: "16px", margin: "10px 0 20px 0" }}>
                                    Delivery information
                                </h3>
                                <div>
                                    <label style={{ display: "block", fontWeight: "700" }}>
                                        * Fullname:
                                    </label>
                                    <input
                                        style={{ padding: "4px", outline: "none", minWidth: "250px" }}
                                        type="text"
                                        value={fullname}
                                        // {...register('fullName', { required: true })}
                                        onChange={(e) => setFullname(e.target.value)}
                                    />
                                    {/* {errors.fullName && <span style={{ color: "red" }}>This field is required</span>} */}
                                    <div style={{ display: "flex", margin: "20px 0" }}>
                                        <span>
                                            <label style={{ display: "block", fontWeight: "700" }}>
                                                * Address:
                                            </label>
                                            <input
                                                style={{ padding: "4px", outline: "none", marginRight: "20px", minWidth: "250px" }}
                                                type="text"
                                                value={address}
                                                onChange={(e) => setAddress(e.target.value)}
                                            />
                                        </span>
                                        <span>
                                            <label style={{ display: "block", fontWeight: "700" }}>
                                                * Phone number:
                                            </label>
                                            <input
                                                style={{ padding: "4px", outline: "none", minWidth: "250px" }}
                                                type="text"
                                                value={phone}
                                                onChange={(e) => setPhone(e.target.value)}
                                            />
                                        </span>
                                    </div>
                                </div>
                                <div id="recaptcha-container"></div>
                                <h1 id="firebase"></h1>
                                <button
                                    className="btn btn-secondary"
                                    onClick={() => handleClick()}
                                >
                                    Validate phone number
                                </button>
                            </div>
                        }
                        <h3
                            style={{ fontSize: "16px", margin: "10px 0 20px 0" }}
                            className="x-fw700"
                        >
                            Products
                        </h3>
                        <Table
                            className="cart-table"
                            hover
                        >
                            <thead>
                                <tr>
                                    <th>PRODUCT DETAIL</th>
                                    <th>QUANTITY</th>
                                    <th>TOTAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    listProductsInCart.map((item) => (
                                        <>
                                            <tr>
                                                <td>
                                                    <Link
                                                        to={`/some-route/reload/${item.slug}`}
                                                        style={{
                                                            color: "#000",
                                                            display: "flex"
                                                        }}
                                                    >
                                                        <img
                                                            className="box-info__img"
                                                            src={`${process.env.REACT_APP_IMAGE_URL}${item.listImagesLink[0]}`}
                                                            alt="alt here"
                                                        />
                                                        <div className="box-info__text">
                                                            <h3>Name: {item.name}</h3>
                                                            <h4>Color: {item.color}</h4>
                                                            <h4>Size: {item.size}</h4>
                                                            <h4>Price: {toVNCurency(item.price)}</h4>
                                                        </div>
                                                    </Link>
                                                </td>
                                                <td
                                                    style={{
                                                        verticalAlign: "middle"
                                                    }}
                                                >
                                                    <h4>
                                                        {item.cartQuantity}
                                                    </h4>
                                                </td>
                                                <td
                                                    style={{
                                                        verticalAlign: "middle"
                                                    }}
                                                >
                                                    <h4>
                                                        {toVNCurency((parseInt(item.price) * parseInt(item.cartQuantity)))}
                                                    </h4>
                                                </td>
                                            </tr>
                                            <span
                                                hidden
                                            >
                                                {total += item.price * item.cartQuantity}
                                            </span>
                                        </>
                                    ))
                                }

                            </tbody>
                        </Table>
                    </div>
                    <div className="col-md-4 box-cart-right">
                        <div
                            className="box-cart-right-warp"
                            style={{ padding: "20px" }}
                        >
                            <h3
                                style={{ margin: "10px 0", fontWeight: "700" }}
                            >
                                ORDER SUMMARY
                            </h3>
                            <p style={{ margin: "10px 0", display: "flex", justifyContent: "space-between" }}>
                                <h4
                                    style={{ fontWeight: "700" }}
                                >
                                    ITEMS: {listProductsInCart.length}
                                </h4>
                                <h4> {toVNCurency(total)}</h4>
                            </p>
                            <p
                                style={{
                                    display: "flex",
                                    justifyContent: "space-between",
                                    alignItems: "baseline"
                                }}
                            >
                                <h4
                                    style={{ fontWeight: "700" }}
                                >
                                    SHIPPING:
                                </h4>
                                <select
                                    value={shipCost}
                                    onChange={(e) => handleShipCostChange(e)}
                                    style={{ margin: "10px 0" }}
                                >
                                    <option value="30">Standar 30.000 VND</option>
                                    <option value="45">Premium 45.000 VND</option>
                                    <option disabled value="0">Payment online</option>
                                </select>
                            </p>
                            <p
                                style={{
                                    textAlign: "right",
                                    margin: "0"
                                }}
                            >
                                {toVNCurency(discountPrice)}
                            </p>
                            <p
                                style={{
                                    display: "flex",
                                    justifyContent: "space-between",
                                    alignItems: "baseline"
                                }}
                            >
                                <h4
                                    style={{ fontWeight: "700" }}
                                >
                                    PROMONATION CODE:
                                </h4>
                                <input
                                    style={{ maxHeight: "0", outline: "none", width: "50%", margin: "unset", padding: "14px" }}
                                    type="text"
                                    value={discountCode}
                                    onChange={(e) => handleSearchInput(e)}
                                />
                            </p>

                            <p style={{ margin: "10px 0", display: "flex", justifyContent: "space-between" }}>
                                <h4
                                    style={{ fontWeight: "700" }}
                                >
                                    TOTAL COST:
                                </h4>
                                <h4>{toVNCurency(parseInt(total) + parseInt(shipCost) - discountPrice)}</h4>
                            </p>

                            <p
                                style={{
                                    margin: "10px 0",
                                    display: "flex",
                                    justifyContent: "space-between",
                                    alignItems: "baseline"
                                }}
                            >
                                <h4
                                    style={{
                                        margin: "10px 0 0 0",
                                        fontWeight: "700"
                                    }}
                                >
                                    SELECT BANK:
                                </h4>
                                <select
                                    value={bank}
                                    onChange={(e) => handleBankChange(e)}
                                    style={{ margin: "10px 0" }}
                                >
                                    <option value="0" text="Không chọn" >No select</option>
                                    <option value="NCB" >Ngan hang NCB</option>
                                    <option value="SACOMBANK" >Ngan hang SacomBank</option>
                                    <option value="EXIMBANK" >Ngan hang EximBank</option>
                                    <option value="MSBANK" >Ngan hang MSBANK</option>
                                    <option value="NAMABANK" >Ngan hang NamABank </option>
                                    <option value="VISA" >The quoc te VISA/MASTER</option>
                                    <option value="VNMART" >Vi dien tu VnMart</option>
                                    <option value="VIETINBANK" >Ngan hang Vietinbank</option>
                                    <option value="VIETCOMBANK" >Ngan hang VCB</option>
                                    <option value="HDBANK" >Ngan hang HDBank</option>
                                    <option value="DONGABANK" >Ngan hang Dong A</option>
                                    <option value="TPBANK" >Ngân hàng TPBank</option>
                                    <option value="OJB" >Ngân hàng OceanBank</option>
                                    <option value="BIDV" >Ngân hàng BIDV</option>
                                    <option value="TECHCOMBANK" >Ngân hàng Techcombank</option>
                                    <option value="VPBANK" >Ngan hang VPBank</option>
                                    <option value="AGRIBANK" >Ngan hang Agribank</option>
                                    <option value="ACB" >Ngan hang ACB</option>
                                    <option value="OCB" >Ngan hang Phuong Dong</option>
                                    <option value="SCB" >Ngan hang SCB</option>
                                </select>
                            </p>




                            <p
                                className="btn--primary"
                                onClick={() => handlePayment()}
                                style={{ width: "100%" }}
                            >
                                {isOffSubmitBtn ? <Spinner></Spinner> : 'BUY'}
                            </p>


                        </div>
                        {/* <Nav tabs>
                            <NavItem>
                                <NavLink
                                    style={{ cursor: "pointer", fontWeight: "700" }}
                                    className={classnames({ active: activeTab === '1' })}
                                    onClick={() => { toggle('1'); }}
                                >
                                    Delivery
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    style={{ cursor: "pointer", fontWeight: "700" }}
                                    className={classnames({ active: activeTab === '2' })}
                                    onClick={() => { toggle('2'); }}
                                >
                                    VNPay
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent activeTab={activeTab}>
                            <TabPane tabId="1">
                                
                            </TabPane>
                            <TabPane tabId="2">
                                <div
                                    className="box-cart-right-warp"
                                    style={{ padding: "20px" }}
                                >
                                    <h3
                                        style={{ margin: "10px 0" }}
                                    >
                                        ORDER SUMMARY
                                    </h3>
                                    <p style={{ margin: "10px 0", display: "flex", justifyContent: "space-between" }}>
                                        <h4>ITEMS: {listProductsInCart.length}</h4>
                                        <h4> {total}.000 vnd</h4>
                                    </p>
                                    <p style={{ margin: "10px 0", display: "flex", justifyContent: "space-between" }}>
                                        <h4>PROMONATIONAL CODE: </h4>
                                        <h4> {discountPrice}.000 vnd</h4>
                                    </p>
                                    <input
                                        style={{ maxHeight: "0", outline: "none", width: "50%", margin: "unset", padding: "14px" }}
                                        type="text"
                                        value={discountCode}
                                        onChange={(e) => handleSearchInput(e)}
                                    />
                                    <p style={{ margin: "10px 0", display: "flex", justifyContent: "space-between" }}>
                                        <h4>TOTAL COST: </h4>
                                        <h4>{parseInt(total) - discountPrice}.000 vnd</h4>
                                    </p>

                                </div>
                            </TabPane>
                        </TabContent> */}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default BoxHome;