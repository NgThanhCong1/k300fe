export default function BoxVNPay(props) {
    const { btnPay_Click } = props
    return (
        <div className="container">
            <div className="header clearfix">
                <h3 className="text-muted">VNPAY DEMO</h3>
            </div>
            <div className="table-responsive">
                <form id="form1" runat="server">
                    <h3>Tạo yêu cầu thanh toán </h3>
                    <div className="form-group">
                        <label>Loại hàng hóa (*) </label>
                        <select id="orderCategory" runat="server" cssclass="form-control">
                            {/* <option value="topup" text="Nạp tiền điện thoại" ></option>
                            <option value="billpayment" text="Thanh toán hóa đơn" ></option>
                            <option value="fashion" text="Thời trang" ></option> */}
                            <option value="other" text="Thanh toán trực tuyến" >Thanh toán trực tuyến</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label>Số tiền (*)</label>
                        <input type="text" id="Amount" runat="server" cssclass="form-control" />
                    </div>
                    <div className="form-group">
                        <label>Nội dung thanh toán (*)</label>
                        <input type="text" id="OrderDescription" runat="server" cssclass="form-control" />
                    </div>
                    <div className="form-group">
                        <label>Ngân hàng</label>
                        <select id="bank" runat="server" cssclass="form-control">
                            <option value text="Không chọn" >Không chọn</option>
                            <option value="NCB" >Ngan hang NCB</option>
                            <option value="SACOMBANK" >Ngan hang SacomBank</option>
                            <option value="EXIMBANK" >Ngan hang EximBank</option>
                            <option value="MSBANK" >Ngan hang MSBANK</option>
                            <option value="NAMABANK" >Ngan hang NamABank </option>
                            <option value="VISA" >The quoc te VISA/MASTER</option>
                            <option value="VNMART" >Vi dien tu VnMart</option>
                            <option value="VIETINBANK" >Ngan hang Vietinbank</option>
                            <option value="VIETCOMBANK" >Ngan hang VCB</option>
                            <option value="HDBANK" >Ngan hang HDBank</option>
                            <option value="DONGABANK" >Ngan hang Dong A</option>
                            <option value="TPBANK" >Ngân hàng TPBank</option>
                            <option value="OJB" >Ngân hàng OceanBank</option>
                            <option value="BIDV" >Ngân hàng BIDV</option>
                            <option value="TECHCOMBANK" >Ngân hàng Techcombank</option>
                            <option value="VPBANK" >Ngan hang VPBank</option>
                            <option value="AGRIBANK" >Ngan hang Agribank</option>
                            <option value="ACB" >Ngan hang ACB</option>
                            <option value="OCB" >Ngan hang Phuong Dong</option>
                            <option value="SCB" >Ngan hang SCB</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label>Ngôn ngữ  (*)</label>
                        <select id="language" cssclass="form-control" runat="server">
                            <option value="vn" >Tiếng Việt</option>
                            <option value="en" >Engoptionsh</option>
                        </select>
                    </div>
                    <button id="btnPay" runat="server" cssclass="btn btn-default" onclick={() => btnPay_Click()}>
                        Thanh toán
                    </button>
                </form>
            </div>
        </div>
    );
}