import { Card, CardBody, CardText, CardImg } from 'reactstrap';
import { rootPath } from '../../../constants/root-server';
export default function BoxStoreSystem(props) {
    const { listStores } = props
    return (
        <div>
            <div className="grid-custom wide"
                style={{ background: "#fff", marginTop: "50px", minHeight: "100vh", padding: "20px" }}
            >
                <h1
                    style={{ margin: "20px 0" }}
                >
                    Store system
                </h1>
                <div className="row">
                    {
                        listStores.map(item => (
                            <div
                                key={item.id}
                                className="col-md-4"
                            >
                                <Card >
                                    <CardImg
                                        top width="100%"
                                        src={`${rootPath}${item.image}`}
                                        alt="Card image cap"
                                    />
                                    <CardBody>
                                        {/* <CardTitle tag="h5">Card Title</CardTitle> */}
                                        <CardText>
                                            Address: {item.address}
                                        </CardText>
                                        <CardText>
                                            Phone: {item.phone}
                                        </CardText>
                                    </CardBody>
                                </Card>
                            </div>
                        ))
                    }
                </div>
            </div>
        </div>
    );
}