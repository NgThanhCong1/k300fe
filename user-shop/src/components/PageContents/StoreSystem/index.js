import storeSystemApi from "api/storeSystemApi";
import { useEffect, useState } from "react";
import Loading2 from "../../Commons/Loading2";
import BoxStoreSystem from "./store-system";

export default function Store() {

    const [isFetchComplete, setIsFetchComplete] = useState(false)
    const [listStores, setListStores] = useState(null)

    useEffect(() => {
        document.title = "K300 | Store system page";
        window.scrollTo(0, 0)
    }, []);

    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await storeSystemApi.getAllStores();
                if (response.code === 1) {
                    setListStores(response.data.listStores)
                    setIsFetchComplete(true)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }
        }
        getDataFromApi();
    }, [isFetchComplete]);

    return (
        isFetchComplete
            ? <div className="main">
                <BoxStoreSystem
                    listStores={listStores}
                />
            </div>
            : <Loading2 />
    );
}