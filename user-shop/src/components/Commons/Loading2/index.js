import './loading2.css';
export default function Loading2() {
    return (
        <div class="loader-cotainer loaded">
            <div class="loaders">
                <div class="loader">
                    <div class="loader-inner ball-pulse-sync">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
    );
}