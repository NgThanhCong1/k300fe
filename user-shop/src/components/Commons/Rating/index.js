import './rating.css';
export function RatingComment(props) {
    const { ratingStatic, addRating } = props
    return (
        <div
            style={{
                display: "flex",
                alignItems: "center",
                margin: "20px 0"
            }}
        >
            <span
                class="dproduct__color-label x-fw600"
                style={{ marginRight: "4px" }}
            >
                Rating:
            </span>
            {
                ratingStatic.isCustomerRated
                    ? <div
                        style={{ fontSize: "14px", fontWeight: "600" }}
                    >
                        You rated {ratingStatic.isCustomerRated} <i className="ti-star"></i> for this product, thanks you
                    </div>
                    : <div className="rate"
                        style={{ marginLeft: "20px" }}
                    >
                        <input type="radio" id="star5" name="rate" defaultValue={5} />
                        <label
                            htmlFor="star5" title="text"
                            onClick={() => addRating(5)}
                        >
                            5 stars
                        </label>
                        <input type="radio" id="star4" name="rate" defaultValue={4} />
                        <label
                            htmlFor="star4"
                            title="text"
                            onClick={() => addRating(4)}
                        >
                            4 stars
                        </label>
                        <input type="radio" id="star3" name="rate" defaultValue={3} />
                        <label
                            htmlFor="star3"
                            title="text"
                            onClick={() => addRating(3)}
                        >
                            3 stars
                        </label>
                        <input type="radio" id="star2" name="rate" defaultValue={2} />
                        <label
                            htmlFor="star2"
                            title="text"
                            onClick={() => addRating(2)}
                        >
                            2 stars
                        </label>
                        <input type="radio" id="star1" name="rate" defaultValue={1} />
                        <label
                            htmlFor="star1"
                            title="text"
                            onClick={() => addRating(1)}
                        >
                            1 star
                        </label>
                    </div>
            }
        </div>
    );
}