import { Link } from 'react-router-dom';
export function ButtonPrimary(props) {
    const { text, linkTo, className, width } = props;
    return (
        width
            ? <Link
                to={`${linkTo}`}
                className={`btn--primary ${className}`}
                style={{ width: `${width}` }}
            >
                {text}
            </Link>
            : <Link
                to={`${linkTo}`}
                className={`btn--primary ${className}`}
            >
                {text}
            </Link>
    );
}
export function ButtonSecondary(props) {
    const { text, linkTo, className } = props;
    return (
        <Link
            to={`${linkTo}`}
            className={`btn--secondary ${className}`}
        // {width && } 
        >
            {text}
        </Link>
    );
}