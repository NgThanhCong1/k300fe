import { React, useState } from 'react'
import { useForm } from "react-hook-form";

function Auth(props) {
    const [usename, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const { setIsOpenAuth, typeAuth, onBtnLoginClick, onBtnRegisterClick, handleLogin, handleRegister } = props
    const { register, handleSubmit, formState: { errors } } = useForm({
        mode: 'onBlur',
    });

    const onLoginSubmit = data => {
        if (!handleLogin) return
        handleLogin({
            UserName: usename,
            Password: password,
        })
    }
    const onRegisterSubmit = data => {
        if (!handleRegister) return
        handleRegister({
            UserName: usename,
            Password: password,
            FirstName: firstName,
            LastName: lastName,
            Email: email
        })
    }

    function handleForgotPasswordClick() {
        setIsOpenAuth(false)
        window.location.assign("http://localhost:3000/#/forgot-password")
    }
    function onOverlayClick() {
        setIsOpenAuth(false)
    }
    function handleBtnLoginClick() {
        onBtnLoginClick()
    }
    function handleBtnRegisterClick() {
        onBtnRegisterClick()
    }
    return (
        <>
            <div style={{ position: "fixed", top: "0", left: "0", bottom: "0", right: "0", background: "rgba(0, 0, 0, 0.7)" }}>
                <div className="formwrap" style={{ zIndex: "2" }}>
                    <div className="close-btn" onClick={() => onOverlayClick()}>

                    </div>

                    <div className="form-left">
                        <div className="form-left__content">
                            <h3>The K300</h3>
                            <p>Upgrade your fashion now</p>
                        </div>
                        <img src="https://i.pinimg.com/564x/c1/d9/8c/c1d98c9e44490e4979ed3bb5b4e50e66.jpg" alt="" />
                    </div>
                    <div className="form-right">
                        {
                            typeAuth == 'login' ?
                                <form action="" className="login-form" id="login-form" onSubmit={handleSubmit(onLoginSubmit)}>
                                    <div class="form-group">
                                        <label for="Username">*Username</label>
                                        <input
                                            type="text"
                                            name="Username"
                                            id="Username"
                                            {...register('username', { required: true })}
                                            onChange={(e) => setUsername(e.target.value)}
                                        />
                                        {errors.username && <span style={{ color: "red" }}>This field is required</span>}
                                    </div>
                                    <div class="form-group">
                                        <label for="Password">*Password</label>
                                        <input
                                            type="password"
                                            name="Password"
                                            id="Password"
                                            {...register('password', { minLength: 10 })}
                                            onChange={(e) => setPassword(e.target.value)}
                                        />
                                        {errors.password && <span style={{ color: "red" }}>10 character atleast</span>}
                                    </div>
                                    <div class="form-group">
                                        <p>Forgot password ?</p>
                                        <p onClick={() => handleForgotPasswordClick()}>Click here</p>
                                        {/* <a href="localhost:3000/forgot-password">Click here</a> */}
                                    </div>
                                    <div className="form-submit">
                                        <button type="submit" className="form-btn">LOGIN</button>
                                        <p onClick={() => handleBtnRegisterClick()}>SIGNUP NOW</p>
                                    </div>
                                </form>
                                :
                                <form action="" className="signup-form" id="signup-form" onSubmit={handleSubmit(onRegisterSubmit)}>
                                    <div className="register-form-flex-input">
                                        <div class="form-group register-form-flex-input__first">
                                            <label for="firstName">*Firstname</label>
                                            <input
                                                type="text"
                                                id="firstName"
                                                {...register('firstName', { required: true })}
                                                onChange={(e) => setFirstName(e.target.value)}
                                            />
                                            {
                                                errors.firstName &&
                                                <span
                                                    style={{ color: "red" }}
                                                >
                                                    This field is required
                                                </span>
                                            }

                                        </div>
                                        <div class="form-group register-form-flex-input__last">
                                            <label for="lastName">*Lastname</label>
                                            <input
                                                type="text"
                                                id="lastName"
                                                {...register('lastName', { required: true })}
                                                onChange={(e) => setLastName(e.target.value)}
                                            />
                                            {errors.lastName && <span style={{ color: "red" }}>This field is required</span>}

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="username">*Username</label>
                                        <input
                                            type="text"
                                            id="username"
                                            {...register('username', { required: true })}
                                            onChange={(e) => setUsername(e.target.value)}
                                        />
                                        {errors.username && <span style={{ color: "red" }}>This field is required</span>}

                                    </div>
                                    <div class="form-group">
                                        <label for="Password">*Password</label>
                                        <input
                                            type="password"
                                            id="password"
                                            {...register('password', { minLength: 10 })}
                                            onChange={(e) => setPassword(e.target.value)}
                                        />
                                        {errors.password && <span style={{ color: "red" }}>10 character atleast</span>}
                                    </div>
                                    <div class="form-group">
                                        <label for="email">*Email</label>
                                        <input
                                            type="text"
                                            name="email"
                                            id="email"
                                            {...register('email', { required: true })}
                                            onChange={(e) => setEmail(e.target.value)}
                                        />
                                        {errors.email && <span style={{ color: "red" }}>Do not match</span>}
                                    </div>
                                    <div className="form-submit">
                                        <button type="submit" className="form-btn">SUGNUP</button>
                                        <p onClick={() => handleBtnLoginClick()}>LOGIN NOW</p>
                                    </div>
                                </form>

                        }

                    </div>
                </div>
            </div>
        </>
    )
}

export default Auth;