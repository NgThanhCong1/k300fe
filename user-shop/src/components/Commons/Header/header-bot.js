import React from 'react';
import { Link } from 'react-router-dom';
function HeaderBot(props) {
    const { listCates } = props
    return (
        <div className="header-bot nonedisplay-both">
            <ul className="menu">
                {
                    listCates && listCates.map(item => (
                        <li className="menu__item menu-clothing show-hiden-content">
                            <a
                                href="/#"
                                className="menu__link underline_effect"
                            >
                                {item.name}
                                <i className="ti-angle-down menu__link-icon"></i>
                            </a>
                            <ul className="menu-clothing__content hiden-content">
                                {
                                    item.listCategories.map((item) => (
                                        <li key={item.id} className="menu-clothing__content-item">
                                            <Link
                                                style={{ textDecoration: "none", color: "#000" }}
                                                to={`/some-route-2/reload/${item.name}`}
                                            >
                                                <span
                                                    className="menu-clothing__content-link underline_effect"
                                                >
                                                    {item.name.toUpperCase()}
                                                </span>
                                            </Link>
                                        </li>
                                    ))
                                }
                            </ul>
                        </li>
                    ))
                }
                <li className="menu__item">
                    <Link
                        to="/collection/NEWINS"
                        className="menu__link"
                    >
                        NEW IN
                    </Link>
                </li>
                <li className="menu__item">
                    <Link
                        to="/about-us"
                        className="menu__link"
                    >
                        ABOUT US
                    </Link>
                </li>
                <li className="menu__item">
                    <Link
                        to="/store"
                        className="menu__link"
                    >
                        STORE SYSTEM
                    </Link>
                </li>
                <li className="menu__item">
                    <Link
                        to="/newin"
                        className="menu__link"
                    >
                        SHOP ON INTAGRAM
                    </Link>
                </li>
                <li className="menu__item">
                    <Link
                        to="/contact"
                        className="menu__link"
                    >
                        CONTACT
                    </Link>
                </li>
            </ul>
        </div>
    );
}


export default HeaderBot;
