
import { rootPath } from "../../../../../constants/root-server";
export default function BoxNotify(props) {
    const { listNotifications, handleDeleteNotify, handleUpdateNotify } = props
    return (
        <li className="personal-list__item show-hiden-content header__navbar-item-has-notify">
            <a href className="header__navbar-item-link"
                style={{ position: "relative" }}
            >
                Notification
                <i className="header__navbar-icon ti-bell" />
                <span
                    className="notify-count"
                    style={{ position: "absolute", top: "0", right: "0" }}
                >
                    {/* 2 */}
                </span>
            </a>
            <div className="header__notify">
                <header className="header__notify-header">
                    <h3>New notifications</h3>
                </header>
                <ul
                    className="header__notify-list"
                    style={{
                        maxHeight: "60vh",
                        overflowY: "scroll"
                    }}
                >
                    {
                        listNotifications && listNotifications.map((item, key) => (
                            <li key={key}
                                className={item.isWatched === 1
                                    ? 'header__notify-item header__notify-item--viewed'
                                    : 'header__notify-item header__notify-item'}
                                onClick={() => handleUpdateNotify(item)}
                                style={{ border: "1px solid rgba(0, 0, 0, 0.02)" }}
                            >
                                <a href className="header__notify-link">
                                    <img
                                        src={`${rootPath}${item.image}`}
                                        alt=""
                                        className="header__notify-img"
                                    />
                                    <div className="header__notify-info">
                                        <span className="header__notify-name">
                                            {item.title}
                                        </span>
                                        <span className="header__notify-description">
                                            {item.content}
                                        </span>
                                    </div>

                                </a>
                                <i
                                    class="far fa-window-close"
                                    style={{
                                        margin: "auto 10px auto auto",
                                        height: "20px",
                                        width: "20px",
                                        zIndex: "100",
                                        fontWeight: "800",
                                        fontSize: "14px"
                                    }}
                                    onClick={() => handleDeleteNotify(item)}
                                >

                                </i>
                            </li>
                        ))
                    }
                </ul>
                <footer className="header__notify-footer">
                    <a href className="header__notify-footer-btn">
                        Xem tất cả
                    </a>
                </footer>
            </div>
        </li>
    );
}