import { useEffect, useState } from 'react';
import BoxNotify from './notify';
import { connectionNotifyHub } from 'constants/index';
import headerApi from 'api/headerApi';
import { useSelector } from 'react-redux';

function Notify() {
    const [listNotifications, setListNotifications] = useState(null);
    const userInfor = useSelector(state => state.userReducer);
    useEffect(() => {
        if (connectionNotifyHub) {
            connectionNotifyHub.on("ReceiveMessage", newListNotifications => {
                setListNotifications(newListNotifications)
            });
        }
    }, []);

    async function handleDeleteNotify(item) {
        try {
            const response = await headerApi.deleteNotify(item);
            if (response.code === 1) {
                if (connectionNotifyHub)
                    connectionNotifyHub.send("UpdateListNotify", userInfor.id);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    async function handleUpdateNotify(item) {
        try {
            const response = await headerApi.updateNotify(item);
            if (response.code === 1) {
                if (connectionNotifyHub)
                    connectionNotifyHub.send("UpdateListNotify", userInfor.id);
            } else {
                console.log(response.message)
            }
        } catch (error) {
            console.log("fail to fetch api", error);
        }
    }

    return (
        <BoxNotify
            listNotifications={listNotifications}
            handleDeleteNotify={handleDeleteNotify}
            handleUpdateNotify={handleUpdateNotify}
        />
    );
}
export default Notify;