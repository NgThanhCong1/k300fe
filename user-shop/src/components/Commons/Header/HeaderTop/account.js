import { Link, useHistory } from 'react-router-dom';
import React from "react";
import { useSelector, useDispatch } from 'react-redux';
import { getProductsInCartDB, logoutAction } from 'actions';
import { rootPath } from 'constants/root-server';

//Account
const Account = function () {
    const userInfor = useSelector(state => state.userReducer);
    console.log("userInfor", userInfor);
    const dispath = useDispatch();
    const history = useHistory();

    function handleLogoutClick() {
        dispath(logoutAction());
        dispath(getProductsInCartDB());
        history.push("/");
    }
    return (
        userInfor
            ? <UserLogined userInfor={userInfor} handleLogoutClick={handleLogoutClick} />
            : <UnUserLogined />
    )
}

function UserLogined(props) {
    return (
        <li className="personal-list__item personal-list__item-account show-hiden-content">
            <span
                className="personal-list__link">
                {
                    props.userInfor.avatar !== ''
                        ? <div className="account-avatar" style={{ backgroundImage: `url(${`${rootPath}${props.userInfor.avatar}?v=${Math.random()}`})` }}></div>
                        : <div className="account-avatar" style={{ backgroundImage: `url(${`${rootPath}/Images/Users/user.png`})` }}></div>
                }
                {props.userInfor.fullname}
            </span>
            <ul className="account-content hiden-content">
                <li className="account-logined__item">
                    <Link style={{ textDecoration: "none", color: "#000" }} to={`/account-setting/home`} >
                        <span className="account-logined__link underline_effect">ACCOUNT SETTING</span>
                    </Link>
                </li>
                <li className="account-logined__item"
                    onClick={() => props.handleLogoutClick()}
                >
                    <span className="account-logined__link underline_effect">LOGOUT</span>
                </li>
            </ul>
        </li >
    );
}
function UnUserLogined() {
    return (
        <li className="personal-list__item personal-list__item-account show-hiden-content">
            <span onClick={e => e.preventDefault()} className="personal-list__link underline_effect">
                Account <i className="personal-list__icon personal-list__icon--account-unlogin ti-user"></i>
            </span>
            <ul className="account-content hiden-content">
                <li className="account-unlogin__item">
                    <Link
                        to="/register"
                        className="account-unlogin__link"
                    >
                        REGISTER
                    </Link>
                </li>
                <li className="account-unlogin__item">
                    <Link
                        to="/login"
                        className="account-unlogin__link"
                    >
                        LOGIN
                    </Link>
                </li>
            </ul>
        </li>
    );
}
//End Account

export default Account;
