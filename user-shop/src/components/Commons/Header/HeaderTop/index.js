import React from 'react';
import Account from './account';
import Cart from './cart';
import { Link } from 'react-router-dom';
import { rootPath } from "constants/root-server"
import Notity from './notify/index';

function HeaderTop() {
    return (
        <>
            <div className="header-top nonedisplay-both">
                <div className="logo">
                    <Link to="/" >
                        <img src={`${rootPath}/Images/Logo/logo.webp`} alt="" className="logo__img" />
                    </Link>
                </div>
                <ul className="personal-list">
                    <Notity />
                    <Account />
                    <Cart />
                </ul>
            </div>
        </>
    );
}

export default HeaderTop;