import { useState } from 'react';
import {
    UncontrolledDropdown,
    InputGroupButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    InputGroup,
    InputGroupAddon,
    Button,
    Input
} from 'reactstrap';
import {
    Link,
} from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';
import { updateListHistoriesSearch } from 'actions';

function HeaderMid(props) {
    const { isLoading, listCates } = props
    const [splitButtonOpen, setSplitButtonOpen] = useState(false);
    const toggleSplit = () => setSplitButtonOpen(!splitButtonOpen);

    const dispatch = useDispatch();
    const listHistoriesSearch = useSelector(state => state.historySearchReducer).listKeywords;

    const [searchTerm, setSearchTerm] = useState({
        categoryName: 'ALL',
        productName: '',
    });

    function handleHistoriesItemClick(item) {
        dispatch(updateListHistoriesSearch({ ...searchTerm, productName: item, categoryName: 'ALL' }));
        setSearchTerm({ ...searchTerm, productName: item, categoryName: 'ALL' })
    }

    return (
        <form>
            <div style={{ position: "relative", top: "-6px" }}>
                <InputGroup style={{ width: "800px", marginBottom: "2px" }} className="search-group">
                    <Input
                        placeholder="type to search your product"
                        className="input-search"
                        style={{ height: "100%", position: "relative" }}
                        value={searchTerm.productName}
                        onChange={(e) => setSearchTerm({ ...searchTerm, productName: e.target.value })}
                    />
                    <InputGroupButtonDropdown
                        addonType="prepend"
                        isOpen={splitButtonOpen}
                        toggle={toggleSplit}
                        style={{ height: "100%", position: "relative" }}
                    >
                        <UncontrolledDropdown>
                            <DropdownToggle
                                caret
                                style={{ height: "32px" }}
                                color="secondary"
                            >
                                {searchTerm.categoryName}
                            </DropdownToggle>
                            <DropdownMenu
                                style={{ width: "100%" }}
                            >
                                <DropdownItem
                                    onClick={() => setSearchTerm({ ...searchTerm, categoryName: 'ALL' })}
                                    style={{
                                        margin: "0 6px",
                                        display: "inline-block",
                                        height: "30px"
                                    }}
                                >
                                    ALL
                                </DropdownItem>
                                {
                                    !isLoading && listCates.map(item => (
                                        item.listCategories.map(subCate => (
                                            <DropdownItem
                                                key={subCate.id}
                                                onClick={() => setSearchTerm({ ...searchTerm, categoryName: subCate.name })}
                                                style={{
                                                    margin: "0 6px",
                                                    display: "inline-block",
                                                    height: "30px"
                                                }}
                                            >
                                                {subCate.name}
                                            </DropdownItem>
                                        ))
                                    ))
                                }
                            </DropdownMenu>
                        </UncontrolledDropdown>

                    </InputGroupButtonDropdown>
                    <InputGroupAddon addonType="append">
                        <Link
                            onClick={() => dispatch(updateListHistoriesSearch(searchTerm))}
                            to={{ pathname: '/search-collection' }} //, search: queryString.stringify(searchTerm) 
                        >
                            <Button
                                type="submit"
                                style={{ height: "32px" }}
                                color="dark"
                            >
                                Find your gourt
                            </Button>
                        </Link>
                    </InputGroupAddon>
                </InputGroup>
                <ul className="search-group" style={{ width: "800px", height: "unset" }}>
                    {
                        listHistoriesSearch.map(item => (
                            <Link
                                onClick={() => handleHistoriesItemClick(item)}
                                to={{ pathname: '/search-collection' }} //, search: queryString.stringify({ categoryName: 'ALL', productName: item }) 
                            >
                                <span
                                    class="badge badge-light"
                                    style={{ margin: "0 2px", fontSize: "10px", cursor: "pointer" }}
                                >
                                    {item}
                                </span>
                            </Link>
                        ))
                    }
                </ul>
            </div >
        </form>
    );
}

export default HeaderMid;