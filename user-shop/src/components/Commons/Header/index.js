import React, { useState, useEffect } from "react";
import HeaderTop from './HeaderTop';
import HeaderBot from './header-bot';
import MHeader from './mheader';
import HeaderMid from "./header-mid";
import headerApi from "api/headerApi";

function Header(props) {
    const [listCates, setListCates] = useState([])
    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        async function getDataFromApi() {
            try {
                const response = await headerApi.getAllCategories();
                if (response.code === 1) {
                    setListCates(response.data)
                    setIsLoading(false)
                } else {
                    console.log(response.message)
                }
            } catch (error) {
                console.log("fail to fetch api", error);
            }

            // let url = `http://localhost:62284/api/Category/get-categories-by-parentCategory`
            // await fetch(url,
            //     {
            //         method: 'GET',
            //         headers: {
            //             'Content-Type': 'application/json',
            //             'Accept': 'application/json',
            //             'Authorization': `Bearer ${localStorage.userToken}`
            //         },
            //     }
            // )
            //     .then(response => response.json())
            //     .then(result => {
            //         if (result.code == 1) {
            //             setListCates(result.data)
            //             setIsLoading(false)
            //         } else {
            //             alert(result.message)
            //         }
            //     })
            //     .catch(error => alert(error.message))
        }
        getDataFromApi();
    }, []);
    return (
        <div className="header">
            <HeaderTop />
            <HeaderMid listCates={listCates} isLoading={isLoading} />
            <HeaderBot listCates={listCates} isLoading={isLoading} />
            <MHeader />
        </div>
    )
}

export default Header;