import { Spinner } from "reactstrap";

export default function WaitButton(props) {
    const { type, className, isOff, btnText } = props;
    return (
        <button
            type={type}
            className={className}
            disabled={isOff}
        >
            {isOff ? <Spinner></Spinner> : btnText}
        </button>
    );
}