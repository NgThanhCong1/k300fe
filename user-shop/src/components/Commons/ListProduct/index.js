import React from 'react';
import { Card, CardBody, Button, CardTitle, CardText, CardImg } from 'reactstrap';
import { rootPath } from "constants/root-server";
import { Link } from "react-router-dom";
import { useDispatch } from 'react-redux';
import { addProductToCartDB } from 'actions';
import { toVNCurency } from 'helpers';

function ListProduct(props) {
    const { direction, listProducts } = props;
    const dispath = useDispatch();

    function addToCart(product) {
        dispath(addProductToCartDB({
            ...product,
            cartQuantity: 1,
            isCheck: 0
        }))
        // console.log("product: ", product);
    }

    return (
        direction === 'horizon'
            ? <div className="row">
                {
                    (listProducts && listProducts.length > 0) && listProducts.map((item) => (
                        <div
                            key={item.productId}
                            className="col-md-3"
                            style={{
                                marginTop: "20px"
                            }}
                        >
                            <Card style={{ position: "relative" }}>
                                <CardImg
                                    top
                                    src={`${rootPath}${item.listImagesLink[0]}`}
                                    alt="Card image cap"
                                    className="product-card__img"
                                />
                                <CardBody
                                    className="product-cart__body"
                                >
                                    <Link
                                        style={{
                                            color: "#fff",
                                            display: "block",
                                            marginBottom: "10px"
                                        }}
                                        to={`/some-route/reload/${item.slug}`}
                                    >
                                        <CardTitle
                                            tag="h5"
                                            className="x-fw600"
                                        >
                                            {item.name}
                                        </CardTitle>
                                    </Link>
                                    <CardText>
                                        <span
                                            className="x-fw600"
                                            style={{ marginRight: "4px" }}
                                        >
                                            Brand:
                                        </span>
                                        {item.brandName}
                                    </CardText>
                                    <CardText >
                                        <span
                                            className="x-fw600"
                                            style={{ marginRight: "4px" }}
                                        >
                                            Price:
                                        </span>
                                        {toVNCurency(item.price)}
                                    </CardText>
                                    <Button
                                        style={{
                                            fontSize: "12px",
                                            width: "100%"
                                        }}
                                        disabled={item.quantity <= 0}
                                        color="dark"
                                        onClick={() => addToCart(item)}
                                    >
                                        {
                                            item.quantity <= 0
                                                ? <>
                                                    <i
                                                        class="far fa-frown"
                                                        style={{
                                                            fontSize: "15px"
                                                        }}
                                                    >
                                                    </i>
                                                    &nbsp;&nbsp;Be soon
                                                </>
                                                : <>
                                                    <i
                                                        class="fas fa-shopping-cart"
                                                        style={{
                                                            fontSize: "15px"
                                                        }}
                                                    >
                                                    </i>
                                                    &nbsp;&nbsp;Add to cart
                                                </>
                                        }
                                    </Button>
                                </CardBody>
                            </Card>
                        </div>
                    ))
                }
            </div>
            : <div>
                {
                    (listProducts && listProducts.length > 0) && listProducts.map((item) => (
                        <Card
                            key={item.productId}
                            style={{ position: "relative" }}
                        >
                            <CardImg
                                top
                                src={`${rootPath}${item.listImagesLink[0]}`}
                                alt="Card image cap"
                                className="product-card__img"
                            />
                            <CardBody
                                className="product-cart__body"
                            >
                                <Link
                                    style={{
                                        color: "#fff",
                                        display: "block",
                                        marginBottom: "10px"
                                    }}
                                    to={`/some-route/reload/${item.slug}`}

                                >
                                    <CardTitle
                                        tag="h5"
                                        className="x-fw600"
                                    >
                                        {item.name}
                                    </CardTitle>
                                </Link>
                                <CardText>
                                    <span
                                        className="x-fw600"
                                        style={{ marginRight: "4px" }}
                                    >
                                        Brand:
                                    </span>
                                    {item.brandName}
                                </CardText>
                                <CardText >
                                    <span
                                        className="x-fw600"
                                        style={{ marginRight: "4px" }}
                                    >
                                        Price:
                                    </span>
                                    {toVNCurency(item.price)}
                                </CardText>
                                <Button
                                    style={{
                                        fontSize: "12px",
                                        width: "100%"
                                    }}
                                    disabled={item.quantity <= 0}
                                    color="dark"
                                    onClick={() => addToCart(item)}
                                >
                                    {
                                        item.quantity <= 0
                                            ? <>
                                                <i
                                                    class="far fa-frown"
                                                    style={{
                                                        fontSize: "15px"
                                                    }}
                                                >
                                                </i>
                                                &nbsp;&nbsp;Be soon
                                            </>
                                            : <>
                                                <i
                                                    class="fas fa-shopping-cart"
                                                    style={{
                                                        fontSize: "15px"
                                                    }}
                                                >
                                                </i>
                                                &nbsp;&nbsp;Add to cart
                                            </>
                                    }
                                </Button>
                            </CardBody>
                        </Card>
                    ))
                }
            </div>

    );
}

export default ListProduct;