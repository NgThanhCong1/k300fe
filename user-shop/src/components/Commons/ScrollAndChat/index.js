import React from "react";
import MessengerCustomerChat from 'react-messenger-customer-chat';

function ScrollAndChat() {
    return (
        <div className="scroll-and-chat">
            <p
                style={{
                    cursor: "pointer",
                    zIndex: "1001"
                }}
                onClick={() => window.scrollTo({
                    top: 0,
                    behavior: 'smooth',
                })}
                class="floatscroll"
            >
                <i class="ti-angle-double-up"></i>
            </p>

            <MessengerCustomerChat
                pageId="335074066942304"
                appId="341659394277904"
            // htmlRef="https://www.facebook.com/zedLH"
            />
        </div>
    );
}
export default ScrollAndChat;