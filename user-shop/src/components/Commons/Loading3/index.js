import { Spinner } from "reactstrap";

export default function Loading3(props) {
    const { height, spinnerWidth } = props
    return (
        <div
            style={{
                textAlign: "center",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                height: height
            }}
        >
            <Spinner
                style={{
                    width: spinnerWidth,
                    height: spinnerWidth
                }}
            />
        </div>
    );
}