import React from "react";

function ScrollTop() {
    return (
        <div id="scrolltop"></div>
    );
}

export default ScrollTop;
