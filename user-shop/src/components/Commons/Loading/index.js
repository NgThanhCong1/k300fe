import './loading.css'
function Loading() {
    return (
        <div class="overlay-loading">
            <div class="loading">
                <div class="loading-item"></div>
                <div class="loading-item"></div>
                <div class="loading-item"></div>
                <div class="loading-item"></div>
                <div class="loading-item"></div>
                <div class="loading-item"></div>
                <div class="loading-item"></div>
                <div class="loading-item"></div>
            </div>
        </div>
    )
}
export default Loading;