import { combineReducers } from 'redux';

const forceUpdateReducers = (state = false, action) => {
    switch (action.type) {
        case 'FORCE_UPDATE':
            state = !state;
            return state;
        default:
            return state;
    }
};



let initUser = null;
let initSearchProducts = {};
let initProductSaws = {
    listProducts: []
};
let initCart = {
    listProducts: []
};
let initHistorySearch = {
    activeKeyword: 'null',
    categoryName: 'ALL',
    listKeywords: []
};

const historySearchReducer = (state = initHistorySearch, action) => {
    switch (action.type) {
        case 'UPDATE_LIST_HISTORY_SEARCH':
            let newCloneListHistoriesSearch = state.listKeywords;
            if (newCloneListHistoriesSearch.length >= 5)
                newCloneListHistoriesSearch.length = 4;
            newCloneListHistoriesSearch = newCloneListHistoriesSearch.filter(p => p !== action.data.productName)
            newCloneListHistoriesSearch.unshift(action.data.productName);

            return {
                ...state,
                activeKeyword: action.data.productName,
                categoryName: action.data.categoryName,
                listKeywords: newCloneListHistoriesSearch
            };
        default:
            return state;
    }
};

const searchProductReducer = (state = initSearchProducts, action) => {
    switch (action.type) {
        case 'SEARCH_PRODUCTS_BY_NAME_OR_CATEGORY':
            return action.data;
        default:
            return state;
    }
};

const productSawReducer = (state = initProductSaws, action) => {
    switch (action.type) {
        case 'UPDATE_LIST_PRODUCT_SAW':
            let newCloneListProductSaws = state.listProducts;
            if (newCloneListProductSaws.length >= 5)
                newCloneListProductSaws.length = 4;

            newCloneListProductSaws = newCloneListProductSaws.filter(p => p.productCode !== action.data.productCode)
            newCloneListProductSaws.unshift(action.data);
            return {
                ...state,
                listProducts: newCloneListProductSaws
            };
        default:
            return state;
    }
};

const userReducer = (state = initUser, action) => {
    switch (action.type) {
        case 'LOGIN':
            return {
                ...state,
                fullname: `${action.data.firstName} ${action.data.lastName}`,
                id: action.data.id,
                avatar: action.data.avatar,
                email: action.data.email
            };
        case 'LOGOUT':
            return null;
        case 'UPDATE_USER':
            return {
                ...state,
                fullname: `${action.data.firstName} ${action.data.lastName}`,
                id: action.data.id,
                avatar: action.data.avatar,
                email: action.data.email
            };

        default:
            return state;
    }
};

const cartReducer = (state = initCart, action) => {
    console.log("state in reducer: ", state)
    switch (action.type) {
        case 'ADD_PRODUCT_TO_CART':
            let newListProducts = state.listProducts;
            let checkExistProductInCart = newListProducts.filter(pc => pc.productCode === action.data.product.productCode);

            if (checkExistProductInCart.length > 0) {
                if (checkExistProductInCart[0].cartQuantity + action.data.buyQuantity > action.data.product.quantity) {
                    alert("out of quantity")
                } else {
                    checkExistProductInCart[0].cartQuantity += action.data.buyQuantity;
                }
            } else {
                if (action.data.buyQuantity > action.data.product.quantity) {
                    alert("out of quantity")
                } else {
                    newListProducts.push({ ...action.data.product, cartQuantity: action.data.buyQuantity, isChecked: 0 });
                }
            }

            return {
                ...state,
                listProducts: newListProducts
            };
        case 'DELETE_PRODUCT_IN_CART':
            let newCloneLists = state.listProducts;
            newCloneLists = newCloneLists.filter(item => (item.productCode !== action.data))

            return {
                ...state,
                listProducts: newCloneLists
            };

        case 'DELETE_LIST_PRODUCT_IN_CART':
            let newCloneListDeleteLists = state.listProducts;

            newCloneListDeleteLists = newCloneListDeleteLists.filter(function (x) {
                return action.data.indexOf(x) < 0;
            });

            return {
                ...state,
                listProducts: newCloneListDeleteLists
            };

        case 'INCRE_PRODUCT_IN_CART':
            console.log("action: ", action)
            let newCloneListIncres = state.listProducts.map(p =>
                p.productCode === action.data.productCode
                    ? action.data.isAdd === true
                        ? { ...p, cartQuantity: p.cartQuantity + 1 }
                        : { ...p, cartQuantity: p.cartQuantity - 1 }
                    : p
            );

            return {
                ...state,
                listProducts: newCloneListIncres
            };

        case 'CHECK_PRODUCT_IN_CART':
            const newCloneListChecks = state.listProducts.map(p =>
                p.productCode === action.data
                    ? { ...p, isChecked: p.isChecked === 0 ? 1 : 0 }
                    : p
            );

            return {
                ...state,
                listProducts: newCloneListChecks
            };
        default:
            return state;
    }
};

const initCartDB = []

const cartDBReducer = (state = initCartDB, action) => {
    switch (action.type) {
        case 'ADD_PRODUCT_TO_CART_DB':
            return action.data;
        case 'REMOVE_PRODUCT_IN_CART_DB':
            return action.data;
        case 'GET_PRODUCTS_IN_CART_DB':
            return action.data;
        case 'SET_CHECK_PRODUCT_IN_CART_DB':
            return action.data;
        case 'MERGE_PRODUCTS_IN_CART_DB':
            return action.data;
        case 'DELETE_LIST_PRODUCTS_IN_CART_DB':
            return action.data;
        default:
            return state;
    }
};

export default combineReducers({
    forceUpdateReducers: forceUpdateReducers,
    userReducer: userReducer,
    cartReducer: cartReducer,
    cartDBReducer: cartDBReducer,
    searchProductReducer: searchProductReducer,
    historySearchReducer: historySearchReducer,
    productSawReducer: productSawReducer
})
