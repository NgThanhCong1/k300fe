export function toVNCurency(inputNumber) {
    return (inputNumber * 1000).toLocaleString('it-IT', { style: 'currency', currency: 'VND' })
}