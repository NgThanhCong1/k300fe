import * as signalR from "@microsoft/signalr";
import jwt from 'jwt-decode';
const token = localStorage.userToken ? jwt(localStorage.userToken) : '';

export const connectionNotifyHub = new signalR.HubConnectionBuilder()
    .withUrl("http://localhost:62284/notifyHub", {
        skipNegotiation: true,
        transport: signalR.HttpTransportType.WebSockets
    })
    .withAutomaticReconnect()
    .build();
connectionNotifyHub.start()
    .then(() => {
        connectionNotifyHub.send("UpdateListNotify", token['http://schemas.microsoft.com/ws/2008/06/identity/claims/primarysid'])
    })
    .catch((error) => console.log(error));

export const connectionCommentHub = new signalR.HubConnectionBuilder()
    .withUrl("http://localhost:62284/commentHub", {
        skipNegotiation: true,
        transport: signalR.HttpTransportType.WebSockets
    })
    .withAutomaticReconnect()
    .build();
connectionCommentHub.start()
    .then(() => {
        connectionCommentHub.send("UpdateListComment", 0)
    })
    .catch((error) => console.log(error));