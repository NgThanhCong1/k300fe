import * as signalR from "@microsoft/signalr";
import cartApi from "api/cartApi";
// import cartApi from "api/cartApi";
import searchProductApi from "api/searchProductApi";
import { checkCookie, getCookie, setCookie } from "cookies store";
// import { checkCookie, getCookie, setCookie } from "cookies store";

export const forceUpdate = () => {
    return {
        type: 'FORCE_UPDATE'
    };
};

export const updateListHistoriesSearch = (searchTerm) => {
    return {
        type: 'UPDATE_LIST_HISTORY_SEARCH',
        data: searchTerm
    };
};

export const updateListProductSaws = (product) => {
    console.log("product in action: ", product)
    return {
        type: 'UPDATE_LIST_PRODUCT_SAW',
        data: product
    };
};

export const loginAction = (userInfor) => {
    localStorage.setItem("userToken", userInfor.token);
    return {
        type: 'LOGIN',
        data: userInfor
    };
};

export const updateUser = (userInfor) => {
    return {
        type: 'UPDATE_USER',
        data: userInfor
    };
};

export const logoutAction = () => {
    localStorage.removeItem("userToken");
    return {
        type: 'LOGOUT'
    };
};

export function addProductToCartDB(product) {
    const type = localStorage.userToken ? 1 : 0;

    return async dispatch => {
        const response = await cartApi.addProductToCart({
            listProductsInCart: [product],
            type: type,
            key: getCartKeyCookie()
        });
        dispatch({
            type: 'ADD_PRODUCT_TO_CART_DB',
            data: response.data,
        })
    };
};

export function removeProductInCartDB(product) {
    const type = localStorage.userToken ? 1 : 0;

    return async dispatch => {
        const response = await cartApi.removeProductInCart({
            listProductsInCart: [product],
            type: type,
            key: getCartKeyCookie()
        });
        dispatch({
            type: 'REMOVE_PRODUCT_IN_CART_DB',
            data: response.data,
        })
    };
};

export function getProductsInCartDB() {
    const type = localStorage.userToken ? 1 : 0;

    return async dispatch => {
        const response = await cartApi.getProductsInCart({
            type: type,
            key: getCartKeyCookie()
        });
        dispatch({
            type: 'GET_PRODUCTS_IN_CART_DB',
            data: response.data,
        })
    };
};

export function mergeProductsInCartDB(userId) {
    return async dispatch => {
        const response = await cartApi.mergeProductsInCart({
            key: getCartKeyCookie(),
            userIdKey: userId
        });
        dispatch({
            type: 'MERGE_PRODUCTS_IN_CART_DB',
            data: response.data,
        })
    };
};

export function deleteListProductInCartDB(listProducts) {
    return async dispatch => {
        const response = await cartApi.deleteListProductsInCart({
            listProductsInCart: listProducts,
        });
        dispatch({
            type: 'DELETE_LIST_PRODUCTS_IN_CART_DB',
            data: response.data,
        })
    };
};

export function setCheckProductInCartDB(product) {
    const type = localStorage.userToken ? 1 : 0;

    return async dispatch => {
        const response = await cartApi.setCheckProductInCart({
            listProductsInCart: [product],
            type: type,
            key: getCartKeyCookie()
        });
        dispatch({
            type: 'SET_CHECK_PRODUCT_IN_CART_DB',
            data: response.data,
        })
    };
};

export const addProductToCart = (product, buyQuantity) => {
    return {
        type: 'ADD_PRODUCT_TO_CART',
        data: { product: product, buyQuantity: buyQuantity }
    };
};


function getCartKeyCookie() {
    if (checkCookie('unloginCartKey')) {
        return getCookie('unloginCartKey');
    } else {
        setCookie('unloginCartKey', 10);
    }
    return getCookie('unloginCartKey');
}

export const addProductsToCart = (product, buyQuantity) => {
    if (localStorage.userToken !== null || localStorage.userToken !== '') {

    }

    return {
        type: 'ADD_PRODUCT_TO_CART',
        data: { product: product, buyQuantity: buyQuantity }
    };
};

export const deleteProductInCart = (productCode) => {
    return {
        type: 'DELETE_PRODUCT_IN_CART',
        data: productCode
    };
};

export const deleteListProductsInCart = (listProducts) => {
    return {
        type: 'DELETE_LIST_PRODUCT_IN_CART',
        data: listProducts
    };
};

export const searchProductsByNameOrCategory = async (searchTerm) => {
    var returnData = null;
    try {
        const response = await searchProductApi.searchProducts(searchTerm);
        if (response.code === 1) {
            returnData = response.data
        }
    } catch (error) {
        // apiErrorHandler(error);
    }
    return {
        type: 'SEARCH_PRODUCTS_BY_NAME_OR_CATEGORY',
        data: returnData
    };
};

export const increBuyQuantity = (productCode, isAdd) => {
    return {
        type: 'INCRE_PRODUCT_IN_CART',
        data: { productCode, isAdd }
    };
};

export const setCheckProductInCart = (productCode) => {
    return {
        type: 'CHECK_PRODUCT_IN_CART',
        data: productCode
    };
};

export const updateBuyQuantity = (productCode) => {
    return {
        type: 'UPDATE_PRODUCT_IN_CART',
        data: productCode
    };
};

export const updateNotify = () => {
    return {
        type: 'UPDATE_NOTIFY'
    };
};
export function ConnectHub() {
    const connectionHub = new signalR.HubConnectionBuilder()
        .withUrl("http://localhost:62284/quantityHub", {
            skipNegotiation: true,
            transport: signalR.HttpTransportType.WebSockets
        })
        .withAutomaticReconnect()
        .build();
    return {
        type: 'START_CONNECT',
        connection: connectionHub
    }
};
export function ConnectCommentHub() {
    const connectionHub = new signalR.HubConnectionBuilder()
        .withUrl("http://localhost:62284/commentHub", {
            skipNegotiation: true,
            transport: signalR.HttpTransportType.WebSockets
        })
        .withAutomaticReconnect()
        .build();
    return {
        type: 'CONNECT_COMMENTHUB',
        connection: connectionHub
    }
};
export function ConnectNotifyHub() {
    const connectionHub = new signalR.HubConnectionBuilder()
        .withUrl("http://localhost:62284/notifyHub", {
            skipNegotiation: true,
            transport: signalR.HttpTransportType.WebSockets
        })
        .withAutomaticReconnect()
        .build();

    return {
        type: 'CONNECT_NOTIFYHUB',
        connection: connectionHub
    }
};