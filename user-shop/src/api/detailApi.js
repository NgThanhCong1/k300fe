import axiosClient from "./axiosClient";

const detailApi = {
    getCommentsByProductId: params => {
        const url = "Comment/get-comments-by-product-id";
        return axiosClient.get(url, { params });
    },
    getBannedWords: () => {
        const url = "Product/get-banned-words";
        return axiosClient.get(url);
    },
    deleteComment: comment => {
        const url = "Comment/delete-comment";
        return axiosClient.post(url, comment);
    },
    updateComment: comment => {
        const url = "Comment/update-comment";
        return axiosClient.post(url, comment);
    },
    addComment: comment => {
        const url = "Comment/add-comment";
        return axiosClient.post(url, comment);
    },
    addRating: rating => {
        const url = "Product/add-rating";
        return axiosClient.post(url, rating);
    },
    getProductBySlug: params => {
        const url = "Product/get-products-by-slug";
        return axiosClient.get(url, { params });
    },
}

export default detailApi;