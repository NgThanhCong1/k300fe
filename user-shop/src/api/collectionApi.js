import axiosClient from "./axiosClient";

const collectionApi = {
    getProductsByCategory: params => {
        const url = "Product/get-products-by-cate";
        return axiosClient.get(url, { params });
    },
}

export default collectionApi;