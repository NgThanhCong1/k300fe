import axiosClient from "./axiosClient";

const authApi = {
    login: userInfor => {
        const url = "/User/user-login";
        return axiosClient.post(url, userInfor);
    },
    confirmMail: confirmInformation => {
        const url = "User/confirm-email";
        return axiosClient.post(url, confirmInformation);
    },
    confirmChangeMail: confirmInformation => {
        const url = "User/confirm-change-email";
        return axiosClient.post(url, confirmInformation);
    },
    forgotPassword: forgotPasswordInfor => {
        const url = "User/forgot-password";
        return axiosClient.post(url, forgotPasswordInfor);
    },
    resetPassword: resetPasswordInfor => {
        const url = "User/reset-password";
        return axiosClient.post(url, resetPasswordInfor);
    },
    changeEmail: changeEmail => {
        const url = "User/change-email";
        return axiosClient.post(url, changeEmail);
    },
    createCustommer: createCustommerInfor => {
        const url = "User/create-user?role=Customer";
        return axiosClient.post(url, createCustommerInfor);
    },
    getRefreshToken: token => {
        const url = `User/get-refresh-token`;
        return axiosClient.post(url, { token });
    },
}

export default authApi;