import axiosClient from "./axiosClient";

const storeSystemApi = {
    getAllStores: () => {
        const url = "Store/get-all-store?page=0";
        return axiosClient.get(url);
    },
}

export default storeSystemApi;