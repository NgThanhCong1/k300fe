import axiosClient from "./axiosClient";

const accountApi = {
    getCustomer: id => {
        const url = `User/get-user-by-id?id=${id}`;
        return axiosClient.get(url);
    },
    getOutInvoiceByUser: params => {
        const url = "Outport/get-outinvoice-by-user";
        return axiosClient.get(url, { params });
    },
    addNotify: notify => {
        const url = "Notify/add-notify";
        return axiosClient.post(url, notify);
    },
    cancleOutInvoice: outinvoice => {
        const url = "Outport/update-outinvoice?type=cacel";
        return axiosClient.post(url, outinvoice);
    },
    updateCustomer: userInfor => {
        const url = "User/update-user";
        return axiosClient.post(url, userInfor);
    },
    trackingOrder: id => {
        const url = `Outport/get-data-for-update-outInvoice?id=${id}`;
        return axiosClient.get(url);
    },
}

export default accountApi;