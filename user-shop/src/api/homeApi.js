import axiosClient from "./axiosClient";

const homeApi = {
    getNewInProducts: () => {
        const url = "Product/get-newin-combine-products";
        return axiosClient.get(url);
    },
    getTop4Categories: () => {
        const url = "Category/get-top-4-cates";
        return axiosClient.get(url);
    },
}

export default homeApi;