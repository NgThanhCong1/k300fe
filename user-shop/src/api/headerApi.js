import axiosClient from "./axiosClient";

const headerApi = {
    getAllCategories: () => {
        const url = "Category/get-categories-by-parentCategory";
        return axiosClient.get(url);
    },
    deleteNotify: notify => {
        const url = "Notify/delete-notify";
        return axiosClient.post(url, notify);
    },
    updateNotify: notify => {
        const url = "Notify/update-notify";
        return axiosClient.post(url, notify);
    },
}

export default headerApi;