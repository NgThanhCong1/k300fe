import axiosClient from "./axiosClient";

const searchProductApi = {
    searchProducts: params => {
        const url = "Product/search-products-by-customer";
        return axiosClient.get(url, { params });
    },
    addKeyword: keywordInfor => {
        const url = "Outport/add-keyword";
        return axiosClient.post(url, keywordInfor);
    },
}

export default searchProductApi;