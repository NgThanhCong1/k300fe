import axiosClient from "./axiosClient";

const cartApi = {
    addListProductsToCart: cart => {
        const url = "product/add-list-product-to-cart";
        return axiosClient.post(url, cart);
    },
    deleteListProductsInCart: cart => {
        const url = "product/delete-lists-products-in-cart";
        return axiosClient.post(url, cart);
    },
    addProductToCart: cart => {
        const url = "product/add-product-to-cart";
        return axiosClient.post(url, cart);
    },
    removeProductInCart: cart => {
        const url = "product/remove-product-in-cart";
        return axiosClient.post(url, cart);
    },
    getProductsInCart: cart => {
        const url = "product/get-products-in-cart";
        return axiosClient.post(url, cart);
    },
    setCheckProductInCart: cart => {
        const url = "product/set-check-product-in-cart";
        return axiosClient.post(url, cart);
    },
    mergeProductsInCart: cart => {
        const url = "product/merge-products-in-cart";
        return axiosClient.post(url, cart);
    }
    //set-check-product-in-cart
}

export default cartApi;