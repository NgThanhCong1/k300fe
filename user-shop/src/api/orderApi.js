import axiosClient from "./axiosClient";

const orderApi = {
    searchDiscount: discountCode => {
        const url = `Discount/search-discount?discountCode=${discountCode}`;
        return axiosClient.get(url);
    },
    order: orderInformation => {
        const url = "Outport/order";
        return axiosClient.post(url, orderInformation);
    },
    orderOnline: orderInformation => {
        const url = "Outport/order-with-payment-online";
        return axiosClient.post(url, orderInformation);
    },
    addNotify: notify => {
        const url = "Notify/add-notify";
        return axiosClient.post(url, notify);
    },
}

export default orderApi;