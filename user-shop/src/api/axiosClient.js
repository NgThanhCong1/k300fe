import axios from 'axios';
import queryString from 'query-string';
import authApi from './authApi';

const axiosClient = axios.create({
    baseURL: process.env.REACT_APP_API_URL,
    paramsSerializer: params => queryString.stringify(params),
});


axiosClient.interceptors.request.use(async (config) => {
    config.headers = {
        'Authorization': `Bearer ${localStorage.userToken}`,
        'Accept': 'application/json',
    }
    return config;
})

axiosClient.interceptors.response.use(
    (response) => {
        if (response && response.data) {
            return response.data;
        }
        return response;
    }, async error => {
        const originalRequest = error.config;

        if (error.response.status === 401 && !originalRequest._retry) {
            alert("get rf token")
            originalRequest._retry = true;
            const response = await authApi.getRefreshToken(localStorage.userToken);

            const newToken = response.data.jwtInfor.accessToken;
            localStorage.setItem('userToken', newToken);
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + newToken;
            return axiosClient(originalRequest);
        }
        // apiErrorHandler(error);
        return Promise.reject(error);
    });
export default axiosClient;

// user/get-alll
